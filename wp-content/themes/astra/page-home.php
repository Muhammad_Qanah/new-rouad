<?php
get_header(); ?>

<?php

/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */
?>

<div id="home-page">
    <section class="hero-section container">

        <!-- 👉 Right Side  -->

        <article class="right-side">
            <h2 class="hero-title">
                أنشــــــئ متجــــــــــرك الإلكترونـــــــــــي
                <br>
                <span class="hero-title-colored">
                    %100 ملكية
                    <span class="hero-title-colored inside">
                        0% عمولات
                    </span>
                </span>
            </h2>
            <p class="hero-description">
                تملّك متجرك اإللكتروني بضغطة زر وق ّسط قيمته على دفعات شهري
            </p>
            <div>

                <?php if (!is_user_logged_in()) {
                    ?>
                    <div class="hero-btns">
                        <a class="primary-button" href="<?= site_url() ?>/my-account/?register=true">
                            <button class="btn btn-primary ">
                                اشترك معنا
                            </button>
                        </a>
                        <a class="outline-button mr-2" href="<?= site_url() ?>/my-account/?register=true">
                            <button class="btn  btn-outline-secondary">
                                اعرف عنا أكثر
                            </button>
                        </a>
                    </div>

                    <?php
                } else {
                    $user = wp_get_current_user();
                    if ($user->TrialDomain) {

                        ?>
                        <form method="post" class=" " action="https://<?= $user->TrialDomain ?>">
                            <input type="hidden" name="username" value=<?= $user->user_login ?>>
                            <input type="hidden" name="admin" value=<?= $user->user_email ?>>
                            <input type="hidden" name="password" value=<?= $user->user_pass ?>>
                            <input type="hidden" name="id" value=<?= $user->ID ?>>

                            <input type="hidden" name="have_sub" value=" <?= $user->serv ?>">
                            <button class="btn btn-primary " type="submit">
                                <?= __('اشترك معنا', 'sso') ?>
                            </button>
                        </form>

                        <?php
                    } else {
                        ?>

                        <div class="hero-btns">
                            <a class="primary-button" href="<?= site_url() ?>/my-account">
                                <button class="btn btn-primary ">
                                    اشترك معنا
                                </button>
                            </a>
                            <a class="mr-2 outline-button" href="<?= site_url() ?>/my-account">
                                <button class="btn  btn-outline-secondary">
                                    اعرف عنا أكثر
                                </button>
                            </a>
                        </div>


                        <?php
                    }
                } ?>

            </div>
        </article>

        <div class="left-side">
            <div id="animation-container"></div>
        </div>
    </section>

    <!-- 👉 feature Section  -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500" class="container margin-top-section">

        <h3 class="section-header">
            مميزات نراهن عليها
        </h3>
        <p class="section-description">
            استفد من تخفيضات باقات الاستضافة في السنوات التالية بدون أي أقساط إضاية
        </p>
        <article class="feature-container" x-data="{section:[
                    {discription:'ملكية 100 %واشتراك ُيدفع مرة وحدة او يمكنك تقسيطه لمدة عام كامل، فيما عدا اشتراكات االستضافة والدومين أو التحديثات.',img:'/wp-content/uploads/2023/landing-page/image 46.svg',btnIcon: '/wp-content/uploads/2023/landing-page/image1.svg' , btnText : 'ملكية 100 %واشتراك ُيدفع مرة وحدة او يمكنك تقسيطه لمدة عام كامل'},
                    {discription:'نوفر حلول لوجستية ومالية لمتجرك من خلال ربطه مع أفضل شركات الشحن وبوابات الدفع الإلكترونية.',img:'/wp-content/uploads/2023/landing-page/image 47.svg' ,btnIcon: '/wp-content/uploads/2023/landing-page/image2.svg' , btnText : 'الربط مع بوابات الدفع و الشحن'},
                    {discription:'فريق عربي محترف ومتعاون يعمل معك عن قرب لضمان حصولك على أفضل تجربة وتقديم الدعم الفني والتقنياللازم لك..',img:'/wp-content/uploads/2023/landing-page/image 48.svg' ,btnIcon: '/wp-content/uploads/2023/landing-page/image3.svg' , btnText : 'الدعم الفّني باللغة العربية'},
                    {discription:'نقدم لك ضمان لاسترداد اموالك خلال ثلاثين يوم من اشتراك ونضمن لك استرداد فوري من دون آي سؤال. في حال رغبت بايقاف التعاقد خلال اول 30 يوم يتم رد كامل المبلغ لك..',img:'/wp-content/uploads/2023/landing-page/image 49.svg' ,btnIcon: '/wp-content/uploads/2023/landing-page/image4.svg' , btnText : 'ضمان ذهبي'}
                    ],active:0}">
            <ul class="feature-cards ">
                <template x-for=" (item , index) in section" :key="index">
                    <li class="feature-card" x-bind:class="active == index ? 'active-feature-card ' : '' "
                        @click="active=index">
                        <div class="icon-for-list-group">
                            <img :src="'<?= site_url() ?>'+item.btnIcon" alt="shape" loading="lazy">
                        </div>
                        <h5 x-text="item.btnText"
                            x-bind:class="active == index ? 'active-feature-card-paragraph' : '' ">
                        </h5>
                    </li>
                </template>
            </ul>
            <div class="feature-second-card">
                <img class=" img-in-second-section-left" :src="'<?= site_url() ?>' + section[active].img" loading="lazy"
                     alt="shape">
                <p class="  p-in-second-section-left" x-text="section[active].discription"></p>
            </div>
        </article>
    </section>

    <!-- 👉 third Section  -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500"
             class="container important-card-section ">
        <div class="">
            <h3 class="section-header">
                دعمنا لك يستمر
            </h3>
            <p class="section-description">هناك العديد من الميزات التي نتفوق بها دائماً</p>
            <div class="important-cards" x-data="
            {
                card:[
                    {img:'/wp-content/uploads/2023/landing-page/language1.svg',header:'متعدد اللغات',paragraph:'ندعم حتى 5 لغات لعرض منتجاتك على جمهورك.',style:'background: rgba(255, 138, 0, 0.08);border-right: 7px solid #FF8A00;'},
                {img:'/wp-content/uploads/2023/landing-page/wallet1.svg',header:'بدون عمولة على المبيعات',paragraph:'من بوابة دفع إلى حسابك مباشرة.. بلا اقتطاع.',style:'background: rgba(16, 82, 239, 0.08);border-right: 7px solid #1152F2;'},
                {img:'/wp-content/uploads/2023/landing-page/update1.svg',header:'تحديثات دورية',paragraph:'نواكب أحدث التقنيات العالمية ونوظفها مباشرة في متجرك.',style:'background: rgba(58, 197, 255, 0.08);border-right: 7px solid #38C8FC;'}
              ]}">
                <template x-for="item in card">
                    <div class="important-card">
                        <div class="image-background" :style="item.style">
                            <img :src="'<?= site_url() ?>'+item.img" alt="" class="img-card-in-fo-section"
                                 loading="lazy">

                        </div>
                        <h4 x-text="item.header"></h4>
                        <p x-text="item.paragraph"></p>
                    </div>
                </template>

            </div>
            <a href="<?= site_url() ?>/my-account?register=true">
                <button class="btn btn-primary ">
                    إبدأ تجربتك المجانية <img
                            src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/arrow-right(1).svg"
                            class="arrow-img-btn" loading="lazy">
                </button>
            </a>
        </div>
    </section>
    <!-- 👉 fourth Section  -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500" class="container margin-top-section">
        <h3 class="section-header">
            ميزات غير محدودة
        </h3>
        <p class="section-description">ستحصل على العديد من الخدمات التي تفيدك في متجرك الإلكتروني</p>
        <div class="fourth-section-cards" x-data="
                {cards:
                    [
                   {header:'المزايا الإدارية',img:'/wp-content/uploads/2023/landing-page/bag.svg',items: ['إدارة منتجاتك بسهولة مع التحكم بالصور والأسعار.', 'إدارة الطلبات المرسلة من العملاء.', 'إدارة العملاء وتنظيمهم.','إدارة الموظفين والصلاحيات.']}
                  ,{header:'الدفع والشحن',img:'/wp-content/uploads/2023/landing-page/card.svg',items: ['تفعيل الدفع عند الاستلام', 'تفعيل الدفع بالتحويل البنكي', 'تفعيل الدفع الإلكتروني','الربط مع شركات التوصيل']},
                   {header:'عمليات الدفع وتحصيل الأموال',img:'/wp-content/uploads/2023/landing-page/airplane.svg',items: ['إدارة الفواتير والضرائب','تحليل المبيعات','تقارير مخصصة']},
                   {header:'التسويق',img:'/wp-content/uploads/2023/landing-page/happy-face.svg',items: ['تفعيل قسائم الخصم (نسبة مئوية أو خصم ثابت)', 'تفعيل الخصم على الأسعار (نسبة مئوية أو خصم ثابت)','الربط مع وسائل التواصل الإجتماعي المختلفة']}
                ]
                }">
            <template x-for="card in cards">
                <div class="fourth-section-card">
                    <img :src="'<?= site_url() ?>'+card.img" class="" alt="shape" loading="lazy">
                    <div class="">
                        <h5 x-text="card.header"></h5>
                        <ul class=" ">
                            <template x-for="(item, index) in card.items">
                                <li x-text="item">
                                </li>
                            </template>
                        </ul>
                    </div>
                </div>
            </template>
        </div>
    </section>
    <!-- 👉 fifth Section  -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500"
             class="container margin-top-section man-section ">
        <div class="man-container">

            <picture>
                <source srcset="<?= site_url() ?>/wp-content/uploads/2023/landing-page/laptop-man.svg"
                        media="(min-width: 1100px)">
                <source srcset="<?= site_url() ?>/wp-content/uploads/2023/landing-page/mobile-man.svg">
                <img class="man-img" src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/mobile-man.svg"
                     loading="lazy" alt="shape"/>
            </picture>
            <div class="background-man"></div>

            <div class="man-text">
                <h3>انشئ متجرك بملكية كاملة وخدمات كثيرة</h3>
                <p class="man-paragraph">
                    مع منصّة روّاد ابدأ تجربتك المجانية وامتلك متجرك الإلكتروني اليوم وقسّط قيمته على دفعات شهرية لمدة
                    عام واحد فقط
                </p>
                <div class="man-btn">

                    <a href="<?= site_url() ?>/my-account/?register=true">
                        <button class="btn btn-white  " style="color : #1152ee;padding: 10px;">
                            أنشئ متجرك المجاني الآن
                        </button>
                    </a>
                    <a class="mr-2" href="<?= site_url() ?>/my-account/?register=true">
                        <button class="btn btn-outline-white " style="padding: 10px;color:white;border-color:white">
                            تعرف على المميزات الخاصة بنا
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- 👉 Tech Section  -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500" class="container margin-top-section">
        <div class="">
            <h3 class="section-header">
                التقنيات التي ندعمها
            </h3>
            <p class="section-description">هناك العديد من التقنيات التي ندعمها لأصحاب
                المتاجر وبتطور مستمر</p>
            <div class="technique-images" x-data="{imgs:['/wp-content/uploads/2023/landing-page/wpml.svg',
                        '/wp-content/uploads/2023/landing-page/elementor.svg',
                        '/wp-content/uploads/2023/landing-page/woocommerce.svg',
                        '/wp-content/uploads/2023/landing-page/wordpress.svg']}">
                <template x-for="img in imgs">
                    <img :src="'<?= site_url() ?>'+img" alt="shape" style="height:2.5rem" loading="lazy">

                </template>
            </div>
        </div>
    </section>

    <!-- 👉 Stores Created By Rouad -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500" class=" margin-top-section">
        <div class="slider-section">
            <h3 class=" section-header">أكثر من +99 متجر تم إنشائهم على منصّة روّاد </h3>
            <p class="  section-description">تصفح المشاريع
                التي قمنا بتقديمها لزبائننا ولا تتردد بالحديث عن متجرك</p>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <a href="https://greenoption.ae">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Green.png" loading="lazy"
                                 alt="store">
                            <div class="text-slider">
                                <h4>غرين أوبشن</h4>
                                <p></p>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="http://almicoajm.com/">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Almico.png" loading="lazy"
                                 alt="store">
                            <div class="text-slider">
                                <h4>الميكو</h4>
                                <p></p>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="http://laylak-fashion.com/">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/laylak.png" loading="lazy"
                                 alt="store">
                            <div class="text-slider">
                                <h4>ليلك</h4>
                                <p></p>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="http://talaatom.com/">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/oman.png" loading="lazy"
                                 alt="store">
                            <div class="text-slider">
                                <h4>طلة عٌماني</h4>
                                <p></p>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="http://almali20.com/">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/malle.png" loading="lazy"
                                 alt="store">
                            <div class="text-slider">
                                <h4>المعالي</h4>
                                <p></p>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="http://themonoshop.com/">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/mono.png" loading="lazy"
                                 alt="store">
                            <div class="text-slider">
                                <h4>مونو فاشن</h4>
                                <p></p>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="http://noriastore.com/">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/noria.png" loading="lazy"
                                 alt="store">
                            <div class="text-slider">
                                <h4>نوريا</h4>
                                <p></p>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="swiper-button-next mobile-hide"></div>
                <div class="swiper-button-prev mobile-hide"></div>
            </div>
        </div>
    </section>

    <!-- 👉 Bundles  -->

    <!-- 👉 Bundles  -->

    <section id="backeg" data-aos="fade-down" data-aos-delay="200" data-aos-duration="500"
             class=".offer-cardcontainer margin-top-section ">
        <div class="tab-card">
            <h3 class=" section-header">
                باقات المتاجر من روّاد
            </h3>
            <p class="section-description">احصل على أفضل العروض من الحزم التالية</p>
            <div class="tab-card-countainer " x-data="{active: 0,
            tabs: ['شهري', 'ربع سنوي', 'سنوي'],
            tabsContent: [
                [
                {
                    header: 'تاجر',
                    img_icon: '/wp-content/uploads/2023/landing-page/man.svg',
                    price: '299 درهم',
                    date: ' شهرياً لمدة عام',
                    list: [
                    'دومين مجاني',
'تدعم عملة واحدة',
                    'تدعم اللغة العربية',
                    'استضافة 10 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'أكثر من 30 الف زيارة شهرياً',
                    'تخصيص المتجر للفئة المرادة',
                    '3 إيميلات',
                    'إضافة حتى 4 موظفين',
                    ],
                    id:'1161'
                },
                {
                    isMostPopular: true,
                    header: 'النمو',
                    img_icon: '/wp-content/uploads/2023/landing-page/tree.svg',
                    price: '1000 درهم ',
                    date: 'شهرياً لمدة عام',
                    list: [
                    'دومين مجاني',
                    'تصميم ثيم مخصص شامل البانارات الإعلانية',
'متعدد العملات',
'متعدد اللغات إلى لغتين',
                    'استضافة 50 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'تخصيص المتجر للفئة المرادة',
                    '5 إيميلات',
                    'إضافة حتى 12 موظفين',
                    ],
                                        id:'960'

                },
                {
                    tab_header: 'سنوي',
                    header: 'ريادة',
                    img_icon: '/wp-content/uploads/2023/landing-page/cup.svg',
                    price: '2500 درهم ',
                    date: 'شهرياً لمدة عام',
                    list: [
                    'دومين مجاني',
                    'تصميم ثيم مخصص شامل البانارات الإعلانية',
                    'متعدد العملات',
                    'متعدد اللغات إلى خمس لغات',
                    'استضافة 100 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',

                    'تخصيص المتجر للفئة المرادة',
                    '10 إيميلات',
                    'إضافة حتى 22 موظفين',
                    'تطبيق إلكتروني',
                    ],
                    id:'958'

                },
                ],
                [
                {
                    header: 'تاجر',
                    img_icon: '/wp-content/uploads/2023/landing-page/man.svg',
                    price: '810 درهم',
                    date: ' 3 شهور',
                    list: [
                    'دومين مجاني',
                    'تدعم عملة واحدة',
'تدعم اللغة العربية',
                    'استضافة 10 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'أكثر من 30 الف زيارة شهرياً',
                    'تخصيص المتجر للفئة المرادة',
                    '3 إيميلات',
                    'إضافة حتى 4 موظفين',
                    ],
                    id:'1368'

                },
                {
                    isMostPopular: true,
                    header: 'النمو',
                    img_icon: '/wp-content/uploads/2023/landing-page/tree.svg',
                    price: '2750 درهم ',
                    date: ' 3 شهور',
                    list: [
                    'دومين مجاني',
'تصميم ثيم مخصص شامل البانارات الإعلانية',
                    'متعدد العملات',
'متعدد اللغات إلى لغتين',
                    'استضافة 50 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'تخصيص المتجر للفئة المرادة',
                    '5 إيميلات',
                    'إضافة حتى 12 موظفين',
                    ],
                    id:'1371'

                },
                {
                    tab_header: 'سنوي',
                    header: 'ريادة',
                    img_icon: '/wp-content/uploads/2023/landing-page/cup.svg',
                    price: '7000 درهم ',
                    date: ' 3 شهور',
                    list: [
                    'دومين مجاني',
                    'تصميم ثيم مخصص شامل البانارات الإعلانية',
'متعدد العملات',
'متعدد اللغات إلى خمس لغات',
                    'استضافة 100 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'تخصيص المتجر للفئة المرادة',
                    '10 إيميلات',
                    'إضافة حتى 22 موظفين',
                    'تطبيق إلكتروني',
                    ],
                    id:'1374'

                },
                ],
                [
                {
                    header: 'تاجر',
                    img_icon: '/wp-content/uploads/2023/landing-page/man.svg',
                    price: '3000 درهم',
                    date: 'سنوياًَ',
                    list: [
                    'دومين مجاني',
                    'تدعم عملة واحدة',
                    'تدعم اللغة العربية',
                    'استضافة 10 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'أكثر من 30 الف زيارة شهرياً',
                    'تخصيص المتجر للفئة المرادة',
                    '3 إيميلات',
                    'إضافة حتى 4 موظفين',
                    ],
                    id:'1391'

                },
                {
                    isMostPopular: true,
                    header: 'النمو',
                    img_icon: '/wp-content/uploads/2023/landing-page/tree.svg',
                    price: '10000 درهم ',
                    date: 'سنوياًَ',
                    list: [
                    'دومين مجاني',
'تصميم ثيم مخصص شامل البانارات الإعلانية',
'متعدد العملات',
'متعدد اللغات إلى لغتين',
                    'استضافة 50 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'تخصيص المتجر للفئة المرادة',
                    '5 إيميلات',
                    'إضافة حتى 12 موظفين',
                    ],
                    id:'1393'

                },
                {
                    tab_header: 'سنوي',
                    header: 'ريادة',
                    img_icon: '/wp-content/uploads/2023/landing-page/cup.svg',
                    price: '25000 درهم ',
                    date: 'سنوياًَ',
                    list: [
                    'دومين مجاني',
'تصميم ثيم مخصص شامل البانارات الإعلانية',
'متعدد العملات',
 'متعدد اللغات إلى خمس لغات',
                    'استضافة 100 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'تخصيص المتجر للفئة المرادة',
                    '10 إيميلات',
                    'إضافة حتى 22 موظفين',
                    'تطبيق إلكتروني',
                    ],
                    id:'1396'

                },
                ],
            ]}">
                <div class="offer-tabs">
                    <template x-for="(tab,index) in tabs" :key="index">
                        <div class="offer-tab " x-bind:class="active == index ? 'offer-tab-active' : '' "
                             @click="active=index">
                            <h5 class="offer-tab-header" x-text="tab"></h5>
                        </div>
                    </template>

                </div>
                <div class="offer-cards">
                    <template x-for="(card,index) in tabsContent[active]" :key="index">
                        <div :class="index==1?'offer-index-card':'offer-card'">
                            <div>
                                <div class="offer-card-header">
                                    <div class="offer-card-header-img">
                                        <img :src="'<?= site_url() ?>'+card.img_icon" alt="shape" loading="lazy">
                                        <h4 x-text="card.header"
                                            :class="index==1?'offer-index-card-text':'offer-card-header'"></h4>
                                    </div>
                                    <div class="tradition" x-show="card.isMostPopular">
                                        الأكثر شعبية
                                    </div>
                                </div>
                            </div>
                            <div class="offer-card-price">
                                <h4 x-text="card.price" :class="index==1?'offer-index-card-text':''"></h4>
                                <h5 x-text="' / '+card.date" :class="index==1?'offer-index-card-text':''"></h5>
                            </div>
                            <hr>

                            <div>
                                <ul class="offer-lists">
                                    <template x-for="list in card.list">
                                        <li class="offer-list">
                                            <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/true.svg"
                                                 loading="lazy" alt="shape">
                                            <p x-text="list" :class="index==1?'offer-index-card-text':''"></p>
                                        </li>
                                    </template>
                                </ul>
                            </div>
                            <a x-bind:href="'checkout/?add-to-cart='+card.id">
                                <button class="btn offer-card-btn"
                                        :class="index==1?'btn-secondary margin-top-btn': index==0?'btn-outline-primary margin-top-btn':'btn-outline-primary'">
                                    اشترك الان
                                    <template x-if="index==1">
                                        <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/arrow-right(1).svg"
                                             loading="lazy" alt="shape">
                                    </template>
                                </button>
                            </a>
                        </div>
                    </template>

                </div>
            </div>
        </div>

    </section>

    <!-- 👉 FAQs -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500" class=" container margin-top-section">

        <h3 class="section-header">
            الأسئلة الشائعة
        </h3>
        <article class="quistion-article" x-data="{section:[
        {quistion:'مالفرق بين نظام األقساط في ّ رواد ونظام االشتراك المتعارف عليه؟',answer:'في نظام الاشتراك يتوجب على المشترك دفع مبلغ شهري بشكل دائم وطوال سنوات وجوده على المنصة، أما في نظام التقسيط الذي نقدمه لعملائنا فالدفع يتم فقط لمدة سنة واحدة وبعدها يتمتع العميل بملكية كاملة للبيانات والمتجر' },
        {quistion:'هل يمكنني الحصول على بيانات المتجر بعد انتهاء السنة الأولى؟',answer:'نعم، يمكنك امتلاك متجرك بشكل كامل والحصول على بياناته بعد انقضاء السنة الأولى وتملك الحرية في إبقاء متجرك في لدينا للحصول على خدمات الاستضافة والتحديثات أو نقله بكل بساطة.' },
        {quistion:'ما هي الالتزامات المادية إذا اخترت الاستمرار في العمل معكم بعد انقضاء السنة الأولى؟',answer:'يمكنك الاستمرار معنا، وتكون قد انتهيت من التزاماتك المادية اتجاه المنصة من حيث أقساط المتجر، ويمكنك الاختيار بين باقة الاستضافة و تجديد الدومين او استمرار اشتراكك كما هو والذي يشمل استضافة وتجديد دومين مجانا بالإضافة الى تحديثات ودعم فني '},
        {quistion:'ماهي تكلفة الاستضافة على سيرفرات منصّة روّاد؟',list:['1 .باقة التجار: (موقع واحد، عدد زوار 50 ألف زائر، مساحة االستضافة 10 غيغابايت)السعر (39 درهم شهرياً) تدفع سنوياً','2 .باقة نمو: (موقع واحد، عدد زوار 200 ألف زائر، مساحة االستضافة 50 غيغابايت)السعر (59 درهم شهرياً) تدفع سنوياً','3 .باقة ريادة: (موقع واحد، عدد زوار 500 ألف زائر، مساحة االستضافة 100 غيغابايت)السعر (99 درهم شهرياً) تدفع سنويا']},
        {quistion:'أين يقع مقر شركتكم؟',answer:'الإمارات العربية المتحدة، دبي، الخليج التجاري، برج تماني آرت.'},
        {quistion:'هل يمكنني بناء متجري الخاص بدون الحاجة إلى معرفة برمجية؟',answer:'الهدف الرئيسي من رواد هو الوصول إلى عالم التجارة الإلكترونية من قبل التجار الذين لا يملكون المعرفة البرمجية وينتظرون الفرصة لتطوير مبيعاتهم بطرق إبداعية وبسيطة وبتكلفة معقولة ، فمن خلال لوحة التحكم الخاصة بك يمكنك إدارة متجرك والتعديل عليه بما يتناسب مع طبيعة النشاط التجاري.'},
        {quistion:'كيف أكون واثقاً بأن بياناتي محمية مع ّ رواد؟',answer:'من خلال فريقنا المتخصص في أمن المعلومات يمكنك الوثوق بأن بياناتك محمية بشكل كامل، حيث يتم إنشاء شهادة أمان SSL للمتاجر المجانية أو المدفوعة بشكل آلي. يعمل فريقنا التقني على تحديث المتاجر المشتركة لدينا بشكل دوري وسد أي ثغرات امنية'},
        {quistion:'هل يمكنني حجز دومين جديد أو نقل بيانات متجر رواد إلى دوميني الخاص؟',answer:'نعم هذه الخيارات متاحة بشكل آلي، حيث يمكنك حجز الدومين الذي ترغب فيه لمتجرك بشكل مجاني، وإذا كنت تمتلك دومين قديم يمكنك نقل بيانات متجر رواد إليه بطريقة بسيطة وسهلة'},
        {quistion:'ما هو الضمان الذهبي من رواد؟',list:['لثقتنا الكبرى بجودة خدماتنا المقدمة نقدم لجميع مشتركينا ضمان استرداد الأموال 100% في حال عدم رضاهم خلال الشهر الأول من الاشتراك وذلك بشكل فوري ومن دون أي سؤال ','ملاحظة : لا يشمل مبلغ حجز النطاقات ويتم تحميل مبلغ 50 درهم للدومين المجاني الذي يتم حجزه من خلال الباقات من حال الرغبة في استراد المبلغ']}
        ],active:0}">
            <ul class="list-quistion">
                <template x-for=" (item , index) in section" :key="index">
                    <li class="btn-quistion " x-bind:class="active == index ? 'active-quistion' : '' "
                        @click="active=index" style="opacity: 0.5;color: #5C5C5C">
                        <div class="btn-quistion-img">
                            <img class="cercle-list" loading="lazy"
                                 src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/Ellipse.svg"
                                 :class="active==index?'cercle-list-active':''">
                            <p class="paragraph-quistion " x-text="item.quistion"></p>
                            <img class="img-list arrow" loading="lazy"
                                 src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="shape"
                                 :class="active==index?'rotate-arrow':''">
                            <img class="img-list plus" loading="lazy"
                                 :src="active!=index?'<?= site_url() ?>/wp-content/uploads/2023/landing-page/open question.svg':'<?= site_url() ?>/wp-content/uploads/2023/landing-page/close question.svg'">
                        </div>
                        <div class="bottom-quistion">
                            <template x-if="item.answer && index === active">
                                <p class=" answer" x-show="index === active" x-text="item.answer"></p>
                            </template>
                            <template x-if="item.list && index === active">
                                <ul>
                                    <template x-for="item in section[active].list">
                                        <li>
                                            <p x-text="item"></p>
                                        </li>
                                    </template>
                                </ul>
                            </template>

                        </div>
                    </li>
                </template>
            </ul>
            <div class="left-quistion">
                <h3 class="" x-text="section[active].quistion"></h3>
                <p class=" " x-text="section[active].answer"></p>
                <ul>
                    <template x-for="item in section[active].list">
                        <li>
                            <p x-text="item"></p>
                        </li>
                    </template>
                </ul>

            </div>
        </article>
    </section>

    <!-- 👉 Contact US -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500" class="container margin-top-section">
        <div class="">
            <h3 class="section-header">
                تواصل معنا
            </h3>
            <div class="contact-us">
                <div class=""><label for="email" class="contact-us-email-lebel">ايميلك الشخصي</label>
                    <br>
                    <input class="width-100 contact-us-email" id="email" type="text" placeholder="your email">
                </div>
                <div class=""><label for="text" class="contact-us-text-lebel">نص الرسالة</label>
                    <br>
                    <textarea class="width-100 contact-us-text" rows="4" id="text"
                              placeholder="مثال : ارغب بشراء متجر الكتروني لديكم."></textarea>
                </div>
                <div class=" submit-group">
                    <p class="">
                        سوف نجيب على رسالتك وقت استلامها
                        <br>
                    </p>
                    <button class="btn btn-primary width-100">ارسال الرسالة</button>
                </div>

            </div>
        </div>
    </section>
</div>


<?php
get_footer();
?>

<style>
    #page {
        background: #fff !important;
    }
</style>


<script>
    var content = $("#home-page")
    $('#content').remove()
    $('#page').append(content)

    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 2.6,
        spaceBetween: 70,

        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        }
    });
    AOS.init();
    var animation = bodymovin.loadAnimation({

        container: document.getElementById('animation-container'),

        path: '<?= site_url() ?>/wp-content/uploads/2023/landing-page/Hero Section.json',

        renderer: 'svg',

        loop: true,

        autoplay: true,

        name: "Rouad Animation",

    });
</script>