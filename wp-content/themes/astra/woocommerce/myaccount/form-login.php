<?php

/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
if ($_GET["register"]) {

    $login_form = "display:none;";
} else {
    $register_form = "display:none;";
}
do_action('woocommerce_before_customer_login_form'); ?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/common.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-744px.css" rel="stylesheet"
      media="(min-width:744px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1440px.css" rel="stylesheet"
      media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1920px.css" rel="stylesheet"
      media="(min-width:1920px)">
<div class="auth-container">
    <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>
        <!-- TODO: Login Form -->

        <div class="login-form-container" style="<?= $login_form ?>" id="customer_login">


            <a class="back-link" href="<?= site_url() ?>">
                <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="">
                <span class="back-title">الرجوع إلى الصفحة الرئيسية</span>
            </a>
            <h2 class="welcome-message"><?php esc_html_e('هلا بيك من جديد! 👋', 'woocommerce'); ?></h2>
            <h5 class="welcome-subtitle">المنصة الأقوى والأجدر في مجال التجارة الإلكترونية</h5>
            <form class="rouad-form login-form" method="post">

                <?php do_action('woocommerce_login_form_start'); ?>

                <p class="auth-form-input">
                    <label for="username"><?php esc_html_e('اسم المستخدم أو البريد الإلكتروني', 'woocommerce'); ?>
                        &nbsp;<span class="required">*</span></label>
                    <input type="text" name="username" id="username" autocomplete="username"
                           value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine
                    ?>
                </p>
                <p x-data="{showPassword : false}" class="auth-form-input">
                    <label for="password"><?php esc_html_e('كلمة السّر', 'woocommerce'); ?>&nbsp;<span
                                class="required">*</span></label>
                    <input x-bind:type="showPassword ? 'text' : 'password'" name="password" id="password"
                           autocomplete="current-password"/>
                    <img @click="showPassword = !showPassword"
                         src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/eye.svg" width="24" height="24">
                </p>

                <?php do_action('woocommerce_login_form'); ?>

                <div class="remember-password-row">
                    <label class="remember-password-checkbox">
                        <input name="rememberme" type="checkbox" id="rememberme" value="forever"/>
                        <span><?php esc_html_e(' تذكرني', 'woocommerce'); ?></span>
                    </label>

                    <div class="lost-password-link">
                        <a
                                href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('هل نسيت كلمة السّر ؟', 'woocommerce'); ?></a>
                    </div>
                </div>
                <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
                <button type="submit" class="btn btn-primary auth-btn"
                <?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>
                "
                name="login"
                value="<?php esc_attr_e('تسجيل الدخول', 'woocommerce'); ?>
                "><?php esc_html_e('تسجيل الدخول', 'woocommerce'); ?></button>
                <?php do_action('woocommerce_login_form_end'); ?>
            </form>
            <div class="register-account">
                <p class="register-account-question">
                    ليس لديك حساب في روّاد؟ &nbsp;<a
                            class="register-account-link show-login-form"><?php esc_html_e('أنشئ حساب ', 'woocommerce'); ?></a>
                </p>
            </div>


        </div>
    <?php endif; ?>

    <!-- TODO: Register Form -->

    <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

        <div class="register-form-container" style="<?= $register_form ?>">


            <a class="back-link" href="<?= site_url() ?>">
                <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="">
                <span class="back-title">الرجوع إلى الصفحة الرئيسية</span>
            </a>

            <h2 class="welcome-message"><?php esc_html_e('أهلاً بك 👋، أنشئ متجرك الآن !', 'woocommerce'); ?></h2>
            <h5 class="welcome-subtitle">المنصة الأقوى والأجدر في مجال التجارة الإلكترونية</h5>


            <form method="post" class="rouad-form register-form" <?php do_action('woocommerce_register_form_tag'); ?>>

                <?php do_action('woocommerce_register_form_start'); ?>

                <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

                    <!-- <p class="auth-form-input">
                <label for="reg_username"><?php esc_html_e('اسم المستخدم', 'woocommerce'); ?>&nbsp;<span
                        class="required">*</span></label>
                <input type="text" name="username" id="reg_username" autocomplete="username"
                    value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine 
                    ?>
            </p> -->

                <?php endif; ?>

                <p class="auth-form-input">
                    <label for="reg_email"><?php esc_html_e('البريد الإلكتروني', 'woocommerce'); ?>&nbsp;<span
                                class="required">*</span></label>
                    <input type="email" name="email" id="reg_email" autocomplete="email"
                           value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine
                    ?>
                </p>

                <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>


                    <p x-data="{showPassword : false}" class="auth-form-input">
                        <label for="reg_password"><?php esc_html_e('كلمة السّر', 'woocommerce'); ?>&nbsp;<span
                                    class="required">*</span></label>
                        <input x-bind:type="showPassword ? 'text' : 'password'" name="password" id="reg_password"
                               autocomplete="current-password"/>
                        <img @click="showPassword = !showPassword"
                             src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/eye.svg" width="24" height="24">
                    </p>

                <?php else : ?>

                    <p><?php esc_html_e('سيرسل اليك الرابط إلى ايميلك المدخل لإختيار كلمة السّر', 'woocommerce'); ?></p>

                <?php endif; ?>


                <p x-data="{showPassword : false}" class="auth-form-input">
                    <label for="reg_password"><?php esc_html_e('رقم الهاتف', 'woocommerce'); ?></label>
                    <input name="reg_phone" id="reg_password"
                           />
                </p>
                <p class="woocommerce-form-row form-row">
                    <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                    <button type="submit" class="btn btn-primary auth-btn
                <?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ?
                        ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>
                    woocommerce-form-register__submit" name="register" value="<?php esc_attr_e(
                        'إنشاء حساب',
                        'woocommerce'
                    ); ?>"><?php esc_html_e(
                            'إنشاء حساب',
                            'woocommerce'
                        ); ?></button>
                </p>


                <?php do_action('woocommerce_register_form_end'); ?>

            </form>

            <div class="register-account">
                <p class="register-account-question">
                    لديك حساب في روّاد؟ <a
                            class="register-account-link show-register-form"><?php esc_html_e('تسجيل الدخول ', 'woocommerce'); ?></a>
                </p>
            </div>

        </div>
    <?php endif; ?>

    <!-- TODO: Side Image -->

    <div class="auth-side-img">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/Phone.svg" class="laptop-login">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/login-tab.svg" alt="" class="tab-login">
    </div>
</div>

<script>
    jQuery(document).ready(() => {

        if (window.location.pathname.includes('my-account')) {
            $('footer').remove()
            $('.road-header').remove()
        }
    })
    jQuery(".show-login-form").on("click", function () {
        window.location.replace("?register=true");


    });
    jQuery(".show-register-form").on("click", function () {
        window.location.replace("?login=true");

    });
</script>