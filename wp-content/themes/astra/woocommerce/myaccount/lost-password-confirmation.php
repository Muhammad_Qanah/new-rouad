<?php
/**
 * Lost password confirmation text.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/lost-password-confirmation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.9.0
 */

defined( 'ABSPATH' ) || exit;

//wc_print_notice( esc_html__( 'تم إرسال ايميل لاعادة تعيين كلمة المرور.', 'woocommerce' ) );
?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1440px.css" rel="stylesheet"
      media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1920px.css" rel="stylesheet"
      media="screen and (min-width:1920px)">
<?php do_action( 'woocommerce_before_lost_password_confirmation_message' ); ?>
<div class="auth-container">


    <!-- TODO: Register Form -->

    <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

        <div class="register-form-container" style="<?=$register_form ?>">


            <a class="back-link" href="<?= site_url() ?>">
                <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="">
                <span class="back-title">الرجوع إلى الصفحة الرئيسية</span>
            </a>

            <h2 class="welcome-message"><?php esc_html_e('إعادة تعيين كلمة المرور', 'woocommerce'); ?></h2>
            <h5 class="welcome-subtitle">تم إرسال ايميل لإعادة تعيين كلمة المرور.</h5>


            <p><?php echo esc_html( apply_filters( 'woocommerce_lost_password_confirmation_message', esc_html__( '
تم إرسال بريد إلكتروني لإعادة تعيين كلمة المرور إلى عنوان البريد الإلكتروني المسجل لحسابك ، ولكن قد يستغرق عدة دقائق لتظهر في صندوق الوارد الخاص بك. يرجى الانتظار لمدة 10 دقائق على الأقل قبل محاولة إعادة تعيين أخرى.', 'woocommerce' ) ) ); ?></p>

            <?php do_action( 'woocommerce_after_lost_password_confirmation_message' ); ?>




        </div>
    <?php endif; ?>

    <!-- TODO: Side Image -->

    <div class="auth-side-img">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/Message.svg">
    </div>
</div>
