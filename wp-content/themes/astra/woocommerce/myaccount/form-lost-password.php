<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>

<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1440px.css" rel="stylesheet"
    media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1920px.css" rel="stylesheet"
    media="screen and (min-width:1920px)">
<div class="auth-container">


    <!-- TODO: Register Form -->

    <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

    <div class="register-form-container" style="<?=$register_form ?>">


        <a class="back-link" href="<?= site_url() ?>">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="">
            <span class="back-title">الرجوع إلى الصفحة الرئيسية</span>
        </a>

        <h2 class="welcome-message"><?php esc_html_e('نسيت كلمة السّر؟', 'woocommerce'); ?></h2>
        <h5 class="welcome-subtitle">سوف نرسل لك رمز التحقق ، يرجى كتابة بريدك الإلكتروني</h5>


        <form method="post" class="rouad-form register-form woocommerce-ResetPassword lost_reset_password" <?php do_action('woocommerce_register_form_tag'); ?>>

            <?php do_action('woocommerce_register_form_start'); ?>

            <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

            <!-- <p class="auth-form-input">
                <label for="reg_username"><?php esc_html_e('اسم المستخدم', 'woocommerce'); ?>&nbsp;<span
                        class="required">*</span></label>
                <input type="text" name="username" id="reg_username" autocomplete="username"
                    value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
            </p> -->

            <?php endif; ?>

            <p class="auth-form-input">
                <label
                    for="user_login"><?php esc_html_e( 'اسم المستخدم أو البريد الإلكتروني', 'woocommerce' ); ?></label>
                <input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login"
                    id="user_login" autocomplete="username" />
            </p>
            <div class="resend-password">
                <p class="resend-question">
                    ألم تحصل على رابط إعادة التعيين؟ <a
                        class="resend-link"><?php esc_html_e('إعادة إرسال ', 'woocommerce'); ?></a>
                </p>
            </div>
            <?php do_action( 'woocommerce_lostpassword_form' ); ?>

            <p class="woocommerce-form-row form-row">

            </p>


            <p class="woocommerce-form-row form-row">
                <input type="hidden" name="wc_reset_password" value="true" />
                <button type="submit" class="btn btn-primary auth-btn" value="<?php esc_attr_e( 'استعادة كلمة السّر', 'woocommerce' ); ?>  "><?php esc_html_e( 'استعادة كلمة السّر', 'woocommerce' ); ?></button>
            </p>

            <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>



        </form>



    </div>
    <?php endif; ?>

    <!-- TODO: Side Image -->

    <div class="auth-side-img">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/Message.svg">
    </div>
</div>
<script>
$(document).ready(() => {
    if (window.location.pathname.includes('my-account')) {
        $('footer').remove()
        $('.road-header').remove()
    }
})
</script>
<?php
do_action( 'woocommerce_after_lost_password_form' );
