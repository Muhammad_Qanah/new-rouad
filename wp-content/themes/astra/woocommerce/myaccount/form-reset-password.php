<?php
/**
 * Lost password reset form.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_reset_password_form');
?>

    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles.css" rel="stylesheet">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1440px.css" rel="stylesheet"
          media="(min-width:1440px)">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1920px.css" rel="stylesheet"
          media="screen and (min-width:1920px)">
    <div class="auth-container">


        <!-- TODO: Register Form -->

        <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

        <div class="register-form-container" style="<?= $register_form ?>">


            <a class="back-link" href="<?= site_url() ?>">
                <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="">
                <span class="back-title">الرجوع إلى الصفحة الرئيسية</span>
            </a>

            <h2 class="welcome-message"><?php esc_html_e('إعادة تعيين كلمة المرور', 'woocommerce'); ?></h2>
            <h5 class="welcome-subtitle"></h5>


            <?php endif; ?>

            <!-- TODO: Side Image -->


            <form method="post" class="woocommerce-ResetPassword lost_reset_password">


                <p class="auth-form-input">
                    <label for="password_2 "><?php esc_html_e('أدخل كلمة السر الجديدة', 'woocommerce'); ?>&nbsp;<span
                                class="required">*</span></label>

                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password"
                           name="password_1" id="password_1" autocomplete="new-password"/>
                </p>
                <p class="auth-form-input">
                    <label for="password_2 "><?php esc_html_e('أعد كتابة كلمة السر ', 'woocommerce'); ?>&nbsp;<span
                                class="required">*</span></label>

                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password"
                           name="password_2" id="password_2" autocomplete="new-password"/>
                </p>

                <input type="hidden" name="reset_key" value="<?php echo esc_attr($args['key']); ?>"/>
                <input type="hidden" name="reset_login" value="<?php echo esc_attr($args['login']); ?>"/>

                <div class="clear"></div>

                <?php do_action('woocommerce_resetpassword_form'); ?>

                <p class="woocommerce-form-row form-row">
                    <input type="hidden" name="wc_reset_password" value="true"/>
                    <button type="submit" class="btn btn-primary auth-btn"
                            value="<?php esc_attr_e('حفظ', 'woocommerce'); ?>  "><?php esc_html_e('استعادة كلمة السّر', 'woocommerce'); ?></button>
                </p>
                <?php wp_nonce_field('reset_password', 'woocommerce-reset-password-nonce'); ?>

            </form>
        </div>

        <div class="auth-side-img">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/Message.svg">
        </div>
    </div>
    <style>
        input {
            width: 100%;
        }

        .auth-container {
            margin-top: 75px;
        }
    </style>
<?php
do_action('woocommerce_after_reset_password_form');

