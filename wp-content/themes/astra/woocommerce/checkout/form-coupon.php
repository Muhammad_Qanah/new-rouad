<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined('ABSPATH') || exit;

if (!wc_coupons_enabled()) { // @codingStandardsIgnoreLine.
    return;
}

?>

<section class="pay-way-page container" style="margin: 0px" x-data="{overlay:false,pay:false,pill:false}">
    <div class="woocommerce-form-coupon-toggle pay-way-price">
        <a class="btn btn-outline-secondary" x-on:click="overlay=!overlay;"><img
                    src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/code-laptop.svg" alt="">
            إدخال كود حسم
        </a>
    </div>
    <form class="checkout_coupon woocommerce-form-coupon" style="display: block;" method="post">
        <div class="bottom-sheet-wrapper laptop" >

            <div class="backdrop " :class="overlay==true?'backdrop-open':''">
                <div class="laptop-backdrop">
                    <h4><span @click.self="overlay=!overlay">×</span>إدخال كود الحسم</h4>
                    <p style="font-size: 15px;"><?php esc_html_e('إذا كان لديك كود خصم يرجى إدخاله هنا ', 'woocommerce'); ?></p>
                    <label for="code" class="">كود الحسم</label>


                    <p class="form-row form-row-first">
                        <label for="coupon_code"
                               class="screen-reader-text"><?php esc_html_e('الكود:', 'woocommerce'); ?></label>
                        <input type="text" name="coupon_code" class="input-text"
                               placeholder="<?php esc_attr_e('رمز الكوبون', 'woocommerce'); ?>" id="coupon_code"
                               value=""/>
                    </p>

                    <p class="form-row form-row-last">
                    <div class="code-btns">
                        <button type="submit"
                                class="copoun-button    button<?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>"
                                name="apply_coupon"
                                value="<?php esc_attr_e('إضافة', 'woocommerce'); ?>"><?php esc_html_e('إضافة', 'woocommerce'); ?></button>

                        <div class="btn exit-code" @click.self="overlay=!overlay">إغلاق</div>
                    </div>
                    </p>

                    <div class="clear"></div>

                </div>
            </div>
        </div>
    </form>
</section>

<style>
    .copoun-button, .woocommerce form.checkout_coupon .button[name=apply_coupon] {
        background-color: #1152F2;
        border-radius: 10px;

    }

    #coupon_code {
        background-color: #EEF7FF;
        border-radius: 5px;
        height: 50px;
        width: 100%;

    }

    .woocommerce-form-coupon {
        display: block !important;
    }

    .woocommerce form .form-row-first, .woocommerce form .form-row-last, .woocommerce-page form .form-row-first, .woocommerce-page form .form-row-last {
        width: 100%;
    }
    .code-btns button{
        width: 75%;
    }
</style>