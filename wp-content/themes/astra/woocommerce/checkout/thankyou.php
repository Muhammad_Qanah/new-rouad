<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined('ABSPATH') || exit;
?>

    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/payment-completed.css" rel="stylesheet">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/payment-completed-min-1440px.css" rel="stylesheet"
          media="(min-width:1440px)">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/payment-completed-min-1920px.css" rel="stylesheet"
          media="screen and (min-width:1920px)">

    <div class="woocommerce-order">

        <?php
        if ($order) :
            do_action('woocommerce_before_thankyou', $order->get_id());
            ?>

            <?php if ($order->has_status('failed')) : ?>

            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce'); ?></p>

            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
                <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>"
                   class="button pay"><?php esc_html_e('Pay', 'woocommerce'); ?></a>
                <?php if (is_user_logged_in()) : ?>
                    <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>"
                       class="button pay"><?php esc_html_e('My account', 'woocommerce'); ?></a>
                <?php endif; ?>
            </p>
            <section class="payment-completed-page container">
                <article>
                    <div class="right-side">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/payment-completed/completed.svg" alt="">
                    </div>
                    <div class="left-side">
                        <div class="title">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/payment-completed/tick-circle.svg"
                                 alt="">
                            <h4>شكراً لك، تمت عملية الدفع بنجاح.</h4>
                        </div>
                        <div class="text">
                            <div class="text-row">
                                <h5>رقم الطّلب</h5>
                                <p><?= $order->get_order_number(); ?></p>
                            </div>
                            <div class="text-row">
                                <h5>المنتج</h5>
                                <p>متجر إلكترومي متكامل اشتراك شهري لمدة عام * 1</p>
                            </div>
                            <div class="text-row">
                                <h5>التّاريخ</h5>
                                <p><?= wc_format_datetime($order->get_date_created()) ?></p>
                            </div>
                            <div class="text-row">
                                <h5>إجمالي المبلغ المستحق</h5>
                                <p> <?= $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped     ?></p>
                            </div>
                            <div class="text-row">
                                <h5>طريقة الدفع</h5>
                                <p><?= wp_kses_post($order->get_payment_method_title()); ?></p>
                            </div>
                            <hr>
                            <div class="state-row">
                                <h5> حالة الطلب</h5>
                                <p> <?= $order->get_status() ?></p>
                            </div>
                        </div>
                    </div>

                </article>
            </section>
        <?php else : ?>

            <section class="payment-completed-page container">
                <article>
                    <div class="right-side">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/payment-completed/completed.svg" alt="">
                    </div>
                    <div class="left-side">
                        <div class="title">
                            <img src="<?= site_url() ?>/wp-content/uploads/2023/payment-completed/tick-circle.svg"
                                 alt="">
                            <h4>شكراً لك، تمت عملية الدفع بنجاح.</h4>
                        </div>
                        <div class="text">
                            <div class="text-row">
                                <h5>رقم الطّلب</h5>
                                <p><?= $order->get_order_number(); ?></p>
                            </div>
                            <div class="text-row">
                                <?php
                                $order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
                                //   $product = $order_items[0]->get_product();
                                foreach ($order_items as $item_id => $item) {
                                    $product = $item->get_product();
                                }
                                ?>
                                <h5>المنتج</h5>
                                <p><?= $product->name . "*" . count($order->get_items()); ?> </p>
                            </div>
                            <div class="text-row">
                                <h5>التّاريخ</h5>
                                <p><?= wc_format_datetime($order->get_date_created()) ?></p>
                            </div>
                            <div class="text-row">
                                <h5>إجمالي المبلغ المستحق</h5>
                                <p> <?= $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped     ?></p>
                            </div>
                            <div class="text-row">
                                <h5>طريقة الدفع</h5>
                                <p><?= wp_kses_post($order->get_payment_method_title()); ?></p>
                            </div>
                            <hr>
                            <div class="state-row">
                                <h5> حالة الطلب</h5>
                                <p> <?= $order->get_status() ?></p>
                            </div>
                        </div>
                    </div>

                </article>
            </section>


        <?php endif; ?>

            <!--            --><?php //do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id());
            ?>
<!--            --><?php //do_action('woocommerce_thankyou', $order->get_id()); ?>

        <?php else : ?>

            <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('شكراً لك تم إيصال طلبك.', 'woocommerce'), null); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

        <?php endif; ?>

    </div>
<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */
