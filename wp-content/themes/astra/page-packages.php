<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<div style="width:100%">
    <section id="backeg" class="container margin-top-section ">
        <div class="tab-card">
            <h3 class=" section-header">
                باقات المتاجر من روّاد
            </h3>
            <p class="section-description">احصل على أفضل العروض من الحزم التالية</p>
            <div class="tab-card-countainer " x-data="{active: 0,
            tabs: ['شهري', 'ربع سنوي', 'سنوي'],
            tabsContent: [
                [
                {
                    header: 'تاجر',
                    img_icon: '/wp-content/uploads/2023/landing-page/man.svg',
                    price: '299 درهم',
                    date: ' شهرياً لمدة عام',
                    list: [
                    'دومين مجاني',
                    'استضافة 10 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'أكثر من 30 الف زيارة شهرياً',
                    'تخصيص المتجر للفئة المرادة',
                    '3 إيميلات',
                    'إضافة حتى 4 موظفين',
                    ],
                    id:'1161'
                },
                {
                    isMostPopular: true,
                    header: 'النمو',
                    img_icon: '/wp-content/uploads/2023/landing-page/tree.svg',
                    price: '1000 درهم ',
                    date: 'شهرياً لمدة عام',
                    list: [
                    'دومين مجاني',
                    'استضافة 50 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'عدد زيارات غير محدود',
                    'تخصيص المتجر للفئة المرادة',
                    '5 إيميلات',
                    'إضافة حتى 12 موظفين',
                    ],
                                        id:'960'

                },
                {
                    tab_header: 'سنوي',
                    header: 'ريادة',
                    img_icon: '/wp-content/uploads/2023/landing-page/cup.svg',
                    price: '2500 درهم ',
                    date: 'شهرياً لمدة عام',
                    list: [
                    'دومين مجاني',
                    'استضافة 100 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'عدد زيارات غير محدود',
                    'تخصيص المتجر للفئة المرادة',
                    '10 إيميلات',
                    'إضافة حتى 22 موظفين',
                    'تطبيق إلكتروني',
                    ],
                    id:'958'

                },
                ],
                [
                {
                    header: 'تاجر',
                    img_icon: '/wp-content/uploads/2023/landing-page/man.svg',
                    price: '810 درهم',
                    date: ' 3 شهور',
                    list: [
                    'دومين مجاني',
                    'استضافة 10 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'أكثر من 30 الف زيارة شهرياً',
                    'تخصيص المتجر للفئة المرادة',
                    '3 إيميلات',
                    'إضافة حتى 4 موظفين',
                    ],
                    id:'1368'

                },
                {
                    isMostPopular: true,
                    header: 'النمو',
                    img_icon: '/wp-content/uploads/2023/landing-page/tree.svg',
                    price: '2750 درهم ',
                    date: ' 3 شهور',
                    list: [
                    'دومين مجاني',
                    'استضافة 50 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'عدد زيارات غير محدود',
                    'تخصيص المتجر للفئة المرادة',
                    '5 إيميلات',
                    'إضافة حتى 12 موظفين',
                    ],
                    id:'1371'

                },
                {
                    tab_header: 'سنوي',
                    header: 'ريادة',
                    img_icon: '/wp-content/uploads/2023/landing-page/cup.svg',
                    price: '7000 درهم ',
                    date: ' 3 شهور',
                    list: [
                    'دومين مجاني',
                    'استضافة 100 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'عدد زيارات غير محدود',
                    'تخصيص المتجر للفئة المرادة',
                    '10 إيميلات',
                    'إضافة حتى 22 موظفين',
                    'تطبيق إلكتروني',
                    ],
                    id:'1374'

                },
                ],
                [
                {
                    header: 'تاجر',
                    img_icon: '/wp-content/uploads/2023/landing-page/man.svg',
                    price: '3000 درهم',
                    date: 'سنوياًَ',
                    list: [
                    'دومين مجاني',
                    'استضافة 10 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'أكثر من 30 الف زيارة شهرياً',
                    'تخصيص المتجر للفئة المرادة',
                    '3 إيميلات',
                    'إضافة حتى 4 موظفين',
                    ],
                    id:'1391'

                },
                {
                    isMostPopular: true,
                    header: 'النمو',
                    img_icon: '/wp-content/uploads/2023/landing-page/tree.svg',
                    price: '10000 درهم ',
                    date: 'سنوياًَ',
                    list: [
                    'دومين مجاني',
                    'استضافة 50 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'عدد زيارات غير محدود',
                    'تخصيص المتجر للفئة المرادة',
                    '5 إيميلات',
                    'إضافة حتى 12 موظفين',
                    ],
                    id:'1393'

                },
                {
                    tab_header: 'سنوي',
                    header: 'ريادة',
                    img_icon: '/wp-content/uploads/2023/landing-page/cup.svg',
                    price: '25000 درهم ',
                    date: 'سنوياًَ',
                    list: [
                    'دومين مجاني',
                    'استضافة 100 غيغا مساحة تخزين',
                    'عدد منتجات غير محدود',
                    'عدد غير محدود من الطلبات',
                    'عدد زيارات غير محدود',
                    'تخصيص المتجر للفئة المرادة',
                    '10 إيميلات',
                    'إضافة حتى 22 موظفين',
                    'تطبيق إلكتروني',
                    ],
                    id:'1396'

                },
                ],
            ]}">
                <div class="offer-tabs">
                    <template x-for="(tab,index) in tabs" :key="index">
                        <div class="offer-tab " x-bind:class="active == index ? 'offer-tab-active' : '' "
                            @click="active=index">
                            <h5 class="offer-tab-header" x-text="tab"></h5>
                        </div>
                    </template>

                </div>
                <div class="offer-cards">
                    <template x-for="(card,index) in tabsContent[active]" :key="index">
                        <div :class="index==1?'offer-index-card':'offer-card'">
                            <div>
                                <div class="offer-card-header">
                                    <div class="offer-card-header-img">
                                        <img :src="'<?= site_url() ?>'+card.img_icon" alt="">
                                        <h4 x-text="card.header"
                                            :class="index==1?'offer-index-card-text':'offer-card-header'"></h4>
                                    </div>
                                    <div class="tradition" x-show="card.isMostPopular">
                                        الأكثر شعبية
                                    </div>
                                </div>
                            </div>
                            <div class="offer-card-price">
                                <h4 x-text="card.price" :class="index==1?'offer-index-card-text':''"></h4>
                                <h5 x-text="' / '+card.date" :class="index==1?'offer-index-card-text':''"></h5>
                            </div>
                            <hr>

                            <div>
                                <ul class="offer-lists">
                                    <template x-for="list in card.list">
                                        <li class="offer-list">
                                            <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/true.svg"
                                                alt="">
                                            <p x-text="list" :class="index==1?'offer-index-card-text':''"></p>
                                        </li>
                                    </template>
                                </ul>
                            </div>
                            <a x-bind:href="'checkout/?add-to-cart='+card.id">
                                <button class="btn offer-card-btn"
                                    :class="index==1?'btn-secondary margin-top-btn': index==0?'btn-outline-primary margin-top-btn':'btn-outline-primary'">
                                    اشترك الان <template x-if="index==1">
                                        <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/arrow-right(1).svg"
                                            alt="">
                                    </template></button></a>
                        </div>
                    </template>

                </div>
            </div>
        </div>


    </section>
    <!-- 👉 FAQs -->

    <section data-aos="fade-down" data-aos-delay="200" data-aos-duration="500" class=" container margin-top-section">

        <h3 class="section-header">
            الأسئلة الشائعة
        </h3>
        <article class="quistion-article" x-data="{section:[
{quistion:'مالفرق بين نظام األقساط في ّ رواد ونظام االشتراك المتعارف عليه؟',answer:'في نظام الاشتراك يتوجب على المشترك دفع مبلغ شهري بشكل دائم وطوال سنوات وجوده على المنصة، أما في نظام التقسيط الذي نقدمه لعملائنا فالدفع يتم فقط لمدة سنة واحدة وبعدها يتمتع العميل بملكية كاملة للبيانات والمتجر' },
{quistion:'هل يمكنني الحصول على بيانات المتجر بعد انتهاء السنة الأولى؟',answer:'نعم، يمكنك امتلاك متجرك بشكل كامل والحصول على بياناته بعد انقضاء السنة الأولى وتملك الحرية في إبقاء متجرك في لدينا للحصول على خدمات الاستضافة والتحديثات أو نقله بكل بساطة.' },
{quistion:'ما هي الالتزامات المادية إذا اخترت الاستمرار في العمل معكم بعد انقضاء السنة الأولى؟',answer:'يمكنك الاستمرار معنا، وتكون قد انتهيت من التزاماتك المادية اتجاه المنصة من حيث أقساط المتجر، ويمكنك الاختيار بين باقة الاستضافة و تجديد الدومين او استمرار اشتراكك كما هو والذي يشمل استضافة وتجديد دومين مجانا بالإضافة الى تحديثات ودعم فني '},
{quistion:'ماهي تكلفة الاستضافة على سيرفرات منصّة روّاد؟',list:['1 .باقة التجار: (موقع واحد، عدد زوار 50 ألف زائر، مساحة االستضافة 10 غيغابايت)السعر (39 درهم شهرياً) تدفع سنوياً','2 .باقة نمو: (موقع واحد، عدد زوار 200 ألف زائر، مساحة االستضافة 50 غيغابايت)السعر (59 درهم شهرياً) تدفع سنوياً','3 .باقة ريادة: (موقع واحد، عدد زوار 500 ألف زائر، مساحة االستضافة 100 غيغابايت)السعر (99 درهم شهرياً) تدفع سنويا']},
{quistion:'أين يقع مقر شركتكم؟',answer:'الإمارات العربية المتحدة، دبي، الخليج التجاري، برج تماني آرت.'},
{quistion:'هل يمكنني بناء متجري الخاص بدون الحاجة إلى معرفة برمجية؟',answer:'الهدف الرئيسي من رواد هو الوصول إلى عالم التجارة الإلكترونية من قبل التجار الذين لا يملكون المعرفة البرمجية وينتظرون الفرصة لتطوير مبيعاتهم بطرق إبداعية وبسيطة وبتكلفة معقولة ، فمن خلال لوحة التحكم الخاصة بك يمكنك إدارة متجرك والتعديل عليه بما يتناسب مع طبيعة النشاط التجاري.'},
{quistion:'كيف أكون واثقاً بأن بياناتي محمية مع ّ رواد؟',answer:'من خلال فريقنا المتخصص في أمن المعلومات يمكنك الوثوق بأن بياناتك محمية بشكل كامل، حيث يتم إنشاء شهادة أمان SSL للمتاجر المجانية أو المدفوعة بشكل آلي. يعمل فريقنا التقني على تحديث المتاجر المشتركة لدينا بشكل دوري وسد أي ثغرات امنية'},
{quistion:'هل يمكنني حجز دومين جديد أو نقل بيانات متجر رواد إلى دوميني الخاص؟',answer:'نعم هذه الخيارات متاحة بشكل آلي، حيث يمكنك حجز الدومين الذي ترغب فيه لمتجرك بشكل مجاني، وإذا كنت تمتلك دومين قديم يمكنك نقل بيانات متجر رواد إليه بطريقة بسيطة وسهلة'},
{quistion:'ما هو الضمان الذهبي من رواد؟',list:['لثقتنا الكبرى بجودة خدماتنا المقدمة نقدم لجميع مشتركينا ضمان استرداد الأموال 100% في حال عدم رضاهم خلال الشهر الأول من الاشتراك وذلك بشكل فوري ومن دون أي سؤال ','ملاحظة : لا يشمل مبلغ حجز النطاقات ويتم تحميل مبلغ 50 درهم للدومين المجاني الذي يتم حجزه من خلال الباقات من حال الرغبة في استراد المبلغ']}
],active:0}">
            <ul class="list-quistion">
                <template x-for=" (item , index) in section" :key="index">
                    <li class="btn-quistion " x-bind:class="active == index ? 'active-quistion' : '' "
                        @click="active=index" style="opacity: 0.5;color: #5C5C5C">
                        <div class="btn-quistion-img">
                            <img class="cercle-list" loading="lazy"
                                src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/Ellipse.svg"
                                :class="active==index?'cercle-list-active':''">
                            <p class="paragraph-quistion " x-text="item.quistion"></p>
                            <img class="img-list arrow" loading="lazy"
                                src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="shape"
                                :class="active==index?'rotate-arrow':''">
                            <img class="img-list plus" loading="lazy"
                                :src="active!=index?'<?= site_url() ?>/wp-content/uploads/2023/landing-page/open question.svg':'<?= site_url() ?>/wp-content/uploads/2023/landing-page/close question.svg'">
                        </div>
                        <div class="bottom-quistion">
                            <template x-if="item.answer && index === active">
                                <p class=" answer" x-show="index === active" x-text="item.answer"></p>
                            </template>
                            <template x-if="item.list && index === active">
                                <ul>
                                    <template x-for="item in section[active].list">
                                        <li>
                                            <p x-text="item"></p>
                                        </li>
                                    </template>
                                </ul>
                            </template>

                        </div>
                    </li>
                </template>
            </ul>
            <div class="left-quistion">
                <h3 class="" x-text="section[active].quistion"></h3>
                <p class=" " x-text="section[active].answer"></p>
                <ul>
                    <template x-for="item in section[active].list">
                        <li>
                            <p x-text="item"></p>
                        </li>
                    </template>
                </ul>

            </div>
        </article>
    </section>
    <section class="container margin-top-section">
        <div class="">
            <h3 class="section-header">
                تواصل معنا
            </h3>
            <div class="contact-us">
                <div class=""><label for="email" class="contact-us-email-lebel">ايميلك الشخصي</label>
                    <br>
                    <input class="width-100 contact-us-email" id="email" type="text" placeholder="your email">
                </div>
                <div class=""> <label for="text" class="contact-us-text-lebel">نص الرسالة</label>
                    <br>
                    <textarea class="width-100 contact-us-text" rows="4" id="text"
                        placeholder="مثال : ارغب بشراء متجر الكتروني لديكم."></textarea>
                </div>
                <div class=" submit-group">
                    <p class="">
                        سوف نجيب على رسالتك وقت استلامها
                        <br>
                    </p>
                    <button class="btn btn-primary width-100">ارسال الرسالة</button>
                </div>

            </div>
        </div>
    </section>

</div>
<style>

</style>
<?php get_footer(); ?>