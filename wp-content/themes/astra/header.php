<?php

/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
<!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.10.2/lottie.min.js"
            integrity="sha512-fTTVSuY9tLP+l/6c6vWz7uAQqd1rq3Q/GyKBN2jOZvJSLC5RjggSdboIFL1ox09/Ezx/AKwcv/xnDeYN9+iDDA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="keywords" content="متجر,تسويق,ادارة متجر ,تجارةالكترونية ,تجارة ," />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1 ,shrink-to-fit=no">
    <meta property="og:title" content="Rouad" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://rouad.com/" />
    <meta property="og:image" content="<?= site_url() ?>/wp-content/uploads/2023/landing-page/hero-pic.svg" />
    <meta property="og:description" content="تملّك متجرك اإللكتروني بضغطة زر وق ّسط قيمته على دفعات شهري" />

    <link rel="stylesheet" href="<?=site_url()?>/wp-content/themes/astra/library/css/swiper-bundle.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script defer src="<?= site_url()?>/wp-content/themes/astra/library/js/cdn.min.js"></script>
    <link href="<?= site_url() ?>/wp-content/uploads/2023/font/style.css" rel="stylesheet">
    <script src="<?= site_url()?>/wp-content/themes/astra/library/js/swiper-bundle.min.js"></script>
    <link href="<?=site_url()?>/wp-content/themes/astra/library/css/aos.css" rel="stylesheet">
    <script src="<?=site_url()?>/wp-content/themes/astra/library/js/aos.js"></script>
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/common.css" rel="stylesheet">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/landing-styles.css" rel="stylesheet">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/landing-styles-min-744px.css" rel="stylesheet"
          media="screen and (min-width:744px)">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/landing-styles-min-1100px.css" rel="stylesheet"
          media="screen and (min-width:1100px)">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/landing-styles-min-1440px.css" rel="stylesheet"
          media="(min-width:1440px)">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/landing-styles-min-1920px.css" rel="stylesheet"
          media="screen and (min-width:1920px)">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="icon" href="<?= site_url() ?>/wp-content/uploads/2022/12/cropped-1-1-192x192.png">
    <link rel="stylesheet" href="<?= site_url() ?>/wp-content/themes/astra/styles/header-styles.css">
    <link rel="stylesheet" href="<?= site_url() ?>/wp-content/themes/astra/styles/header-styles-min-1100px.css"
          media="(min-width: 1100px)">
    <link rel="stylesheet" href="<?= site_url() ?>/wp-content/themes/astra/styles/header-styles-min-1440px.css"
          media="(min-width: 1440px)">
    <?php wp_head(); ?>

</head>

<body>
<div <?php
echo astra_attr(
    'site',
    array(
        'id' => 'page',
        'class' => 'hfeed site',
    )
);
?>>
    <!-- ,{url:'/من-نحن',title:'من نحن' , slug:'%D9%85%D9%86-%D9%86%D8%AD%D9%86'},{url:'/contact-us',title:'تواصل معنا'},{url:'/privacy-policy',title:'سياسة الخصوصية'} -->
    <a class="skip-link screen-reader-text"
       href="#content"><?php echo esc_html(astra_default_strings('string-header-skip-link', false)); ?></a>
    <div class="road-header"
         x-data="{open: false,links:[{url:'',title:'الرئيسية'},{url:'/feature',title:'لماذا رواد ؟'},{url:'/packages',title:'الباقات'},{url:'/articles',title:'المقالات'}]}">

        <!-- mobile sidebar -->

        <nav class="sidebar" :class="{'sidebar-opened': open === true, 'sidebar-closed': open === false}">
            <div @click="open = false" class="sidebar-toggle">
                <img class="menu-open-icon" src="<?= site_url() ?>/wp-content/uploads/2023/header/close.svg"
                     alt="close-icon">
            </div>

            <div class="nav-links-container">
                <template x-for="(link,index) in links" :key="index">
                    <a class="nav-link"
                       :class="window.location.pathname.includes(link.slug)  ? 'nav-link-active' : ''"
                       :href="'<?= site_url() ?>'+link.url">
                        <div class="" x-text="link.title">
                        </div>
                    </a>
                </template>
            </div>

            <div class="visit-site">
                <?php if (!is_user_logged_in()) {
                    ?>
                    <a href="<?= site_url() ?>/my-account/?login=true">
                        <div class="header-button">
                            الدخول إلى متجرك
                        </div>
                    </a>
                    <?php
                } else {
                    $user = wp_get_current_user();
                    if ($user->TrialDomain) {

                        ?>
                        <form method="post" class=" " action="https://<?= $user->TrialDomain ?>">
                            <input type="hidden" name="username" value=<?= $user->user_login ?>>
                            <input type="hidden" name="admin" value=<?= $user->user_email ?>>
                            <input type="hidden" name="password" value=<?= $user->user_pass ?>>
                            <input type="hidden" name="id" value=<?= $user->ID ?>>

                            <input type="hidden" name="have_sub" value=" <?= $user->serv ?>">
                            <button class="header-button " type="submit">
                                <?= __('الدخول إلى متجرك', 'sso') ?>
                            </button>
                        </form>
                        <?php
                    } else {
                        ?>

                        <a href="<?= site_url() ?>/my-account">
                            <div class="header-button">
                                <?= __('الدخول إلى متجرك', 'sso') ?>
                            </div>
                        </a>

                        <?php

                    }
                }
                ?>
            </div>

        </nav>
        <section class="main-header container">
            <!-- <div class="menu-icon menu-open">
                <img class="menu-open-icon" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/open-menu.svg"
                    alt="logo">
                <img class="menu-open-icon-w" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/open-menu-w.svg"
                    alt="logo">
            </div>
            <div class="menu-icon menu-close">
                <img class="menu-close-icon" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/close-menu.svg"
                    alt="logo">
                <img class="menu-close-icon-w" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/close-menu-w.svg"
                    alt="logo">
            </div> -->
            <div class="header-logo">
                <a href="<?= site_url() ?> ">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/1-300x97.png " alt="logo">
                </a>
            </div>
            <!-- <div class="header-logo">
                    <a href="<?= site_url() ?> ">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/01/logo/logo-w.png" alt="logo">
                    </a>
                </div> -->
            <nav class="links-container">
                <template x-for="(link,index) in links" :key="index">
                    <a class="header-link"
                       :class="window.location.pathname.includes(link.slug)  ? 'header-link-active' : ''"
                       :href="'<?= site_url() ?>'+link.url" x-text="link.title"></a>
                </template>

            </nav>
            <div class="left-container">
                <div class="visit-site">
                    <?php if (!is_user_logged_in()) {
                        ?>
                        <a href="<?= site_url() ?>/my-account/?login=true">
                            <div class="header-button">
                                الدخول إلى متجرك
                            </div>
                        </a>
                        <?php
                    } else {
                        $user = wp_get_current_user();
                        if ($user->TrialDomain) {

                            ?>
                            <form method="post" class=" " action="https://<?= $user->TrialDomain ?>">
                                <input type="hidden" name="username" value=<?= $user->user_login ?>>
                                <input type="hidden" name="admin" value=<?= $user->user_email ?>>
                                <input type="hidden" name="password" value=<?= $user->user_pass ?>>
                                <input type="hidden" name="id" value=<?= $user->ID ?>>

                                <input type="hidden" name="have_sub" value=" <?= $user->serv ?>">
                                <button class="header-button " type="submit">
                                    <?= __('الدخول إلى متجرك', 'sso') ?>
                                </button>
                            </form>
                            <?php
                        } else {
                            ?>

                            <a href="<?= site_url() ?>/my-account">
                                <div class="header-button">
                                    <?= __('الدخول إلى متجرك', 'sso') ?>
                                </div>
                            </a>

                            <?php
                        }
                    }
                    ?>
                </div>
                <div @click="open = true" class="sidebar-toggle">
                    <img class="menu-open-icon" src="<?= site_url() ?>/wp-content/uploads/2023/header/menu.svg"
                         alt="logo">
                </div>
            </div>
        </section>
    </div>
    <style>
        .menu-close {
            display: none;
        }

        .road-header {
            background-color: white;
            position: fixed;
            z-index: 2;
            top: 0;
            right: 0;
            width: 100%;
            padding: 3px;
            display: flex;
            align-items: center;
            justify-content: space-between;

        }

        .main-header {
            width: 100%;
            padding: 11px 0px;
            display: flex;
            align-items: center;
        }

        .rouad-logo img {
            max-width: 89px;

        }

        div.<?=$activ_class ?> {
            border-bottom: 4px solid #4bc8f5;

        }

        .main-menu {
            width: 60%;
            display: flex;
            align-items: center;
            justify-content: flex-start;
            color: #1152f2;
            font-size: 16px;
            font-weight: bold;
        }

        a {
            color: #1152f2;
        }


        .main-menu div {
            color: #1152f2;
            margin-right: 10px;
            margin-left: 10px;
        }

        .visit-site-button {
            font-size: 16px;
            width: 160px;
            height: 40px;
            text-align: center;
            display: flex;
            align-content: center;
            justify-content: center;
            align-items: center;
            font-weight: 500;
            color: var(--r-secondary);
            border-width: 1.5px;
            background-color: white;
            border-color: var(--r-secondary);
            border-style: solid;
            border-radius: 10px;
            padding: 0px;
            transition: all 1s;
        }

        .visit-site-button:hover {
            background-color: var(--r-secondary) !important;
            color: white;
        }

        .main-icon {
            display: flex;
            flex-direction: column;
            align-content: center;
            justify-content: center;
            align-items: center;
        }

        .main-line {
            border-bottom: 4px solid transparent;
            width: 100%;
            margin-top: 5px;
            border-radius: 10px;

        }

        button:focus,
        .menu-toggle:hover,
        button:hover,
        .ast-button:hover,
        .ast-custom-button:hover .button:hover,
        .ast-custom-button:hover,
        input[type=reset]:hover,
        input[type=reset]:focus,
        input#submit:hover,
        input#submit:focus,
        input[type="button"]:hover,
        input[type="button"]:focus,
        input[type="submit"]:hover,
        input[type="submit"]:focus,
        form[CLASS*="wp-block-search__"].wp-block-search .wp-block-search__inside-wrapper .wp-block-search__button:hover,
        form[CLASS*="wp-block-search__"].wp-block-search .wp-block-search__inside-wrapper .wp-block-search__button:focus {
            color: #ffffff;
            background-color: #1152f2;
            border-color: #1152f2;
        }

        .who-icon {

            display: flex;
            flex-direction: column;
            align-content: center;
            justify-content: center;
            align-items: center;
        }

        .who-line {
            border-bottom: 4px solid transparent;
            width: 100%;
            margin-top: 5px;
            border-radius: 10px;

        }

        .packing-icon {
            display: flex;
            flex-direction: column;
            align-content: center;
            justify-content: center;
            align-items: center;
        }

        .packing-line {
            border-bottom: 4px solid transparent;
            width: 100%;
            margin-top: 5px;
            border-radius: 10px;

        }

        .woocommerce-LostPassword .woocommerce-LostPassword1 .lost_password {
            font-size: 14px;
        }


        .menu-icon {
            cursor: pointer;
        }

        .rouad-logo-w {
            display: none;
        }

        .menu-close-w {
            display: none;

        }

        .menu-open-icon-w {
            display: none;

        }

        .menu-close-icon-w {
            display: none;

        }

        .menu-close {
            display: none;

        }

        .elementor-kit-1336 h4,
        .elementor-kit-1336 h5,
        .elementor-kit-1336 h3,
        .elementor-kit-1336 h6 {
            color: unset;
        }
    </style>
    <script>
        let type = window.location.href;
        type1 = type.split("#");
        if (type1[1] == "package") {
            console.log("dfdfdf")

        }
        jQuery(".mobile-open-menu").hide();

        // jQuery(document).scroll(function() {
        //     if (jQuery(this).scrollTop() > 1) {
        //         jQuery('.site-header').css({
        //             "position": "fixed"
        //         });
        //         jQuery('.main-menu div').css({
        //             "color": "#ffffff"
        //         })
        //         jQuery('.site-header').css({
        //             "background-color": "#1152F2"
        //         })
        //         jQuery('.rouad-logo-b').hide()
        //         jQuery('.rouad-logo-w').show()
        //         jQuery('.menu-open-icon-w').show()
        //         jQuery('.menu-close-icon-w').show()
        //         jQuery('.menu-open-icon').hide()
        //         jQuery('.menu-close-icon').hide();
        //         jQuery(".visit-site-button").hover(function() {
        //             jQuery(this).css("background-color", "#ffffff");
        //             jQuery(this).css("color", "#1152F2");
        //         }, function() {
        //             jQuery(this).css("background-color", "#4bc8f5");
        //             jQuery(this).css("color", "#ffffff");
        //         });
        //     } else {

        //         jQuery(".visit-site-button").hover(function() {

        //             jQuery(this).css("background-color", "#1152F2");
        //             jQuery(this).css("color", "#ffffff");
        //         }, function() {

        //             jQuery(this).css("background-color", "#4bc8f5");
        //             jQuery(this).css("color", "#ffffff");
        //         });
        //         jQuery('.site-header').css({
        //             "position": "inherit"
        //         });
        //         jQuery('.main-menu div').css({
        //             "color": "#1152F2"
        //         })
        //         jQuery('.site-header').css({
        //             "background-color": "#ffffff"
        //         })
        //         jQuery('.rouad-logo-w').hide()
        //         jQuery('.rouad-logo-b').show()

        //         jQuery('.menu-open-icon-w').hide()
        //         jQuery('.menu-close-icon-w').hide()
        //         jQuery('.menu-open-icon').show()
        //         jQuery('.menu-close-icon').show()
        //     }
        // });


        jQuery(".menu-open").on("click", function(event) {
            jQuery(".menu-open").hide()
            jQuery(".menu-close").show()
            jQuery(".mobile-open-menu").show();
        });

        jQuery(".menu-close").on("click", function(event) {
            jQuery(".menu-close").hide()
            jQuery(".menu-open").show()
            jQuery(".mobile-open-menu").hide();
        });
        jQuery(document).ready(function() {
            jQuery(".elementor-kit-1336").removeClass(".elementor-kit-1336")
        })
    </script>
    <div id="content" class="site-content">
        <div class="ast-container" style="display: block">