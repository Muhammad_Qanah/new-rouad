<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/payment-completed.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/payment-completed-min-744px.css" rel="stylesheet"
    media="(min-width:744px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/payment-completed-min-1440px.css" rel="stylesheet"
    media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/payment-completed-min-1920px.css" rel="stylesheet"
    media="screen and (min-width:1920px)">

<section class="payment-completed-page container">
    <article>
        <div class="right-side">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/payment-completed/completed.svg" alt="">
        </div>
        <div class="left-side">
            <div class="title">
                <img src="<?= site_url() ?>/wp-content/uploads/2023/payment-completed/tick-circle.svg" alt="">
                <h4>شكراً لك، تمت عملية الدفع بنجاح.</h4>
            </div>
            <div class="text"
                x-data="{row:[{header:'رقم الطّلب',paragraph:'1799'},{header:'المنتج',paragraph:'متجر إلكترومي متكامل اشتراك شهري لمدة عام *1'},{header:'التّاريخ',paragraph:'13 فبراير، 2023'},{header:'إجمالي المبلغ المستحق',paragraph:'299 درهم إماراتي'},{header:'طريقة الدفع',paragraph:'Credit Card (Stripe)'}] , state:{header:'حالة الطلب',state:'تم دفع المبلغ المستحق'}}">
                <template x-for="item in row">
                    <div class="text-row">
                        <h5 x-text="item.header"></h5>
                        <p x-text="item.paragraph"></p>
                    </div>
                </template>
                <hr>
                <div class="state-row">
                    <h5 x-text="state.header"></h5>
                    <p x-text="state.state"></p>
                </div>
            </div>
            <button class="btn btn-primary">إكمال معلومات الموقع</button>
        </div>

    </article>
</section>