<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/contact-us.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/contact-us-min-744px.css" rel="stylesheet"
    media="(min-width:744px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/contact-us-min-1440px.css" rel="stylesheet"
    media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/contact-us-min-1920px.css" rel="stylesheet"
    media="screen and (min-width:1920px)">

<div style="width:100%">
    <section class="contact-us-page container">
        <h2>تواصل معنا</h2>
        <p>هل لديك استفسار؟ لا تتردد وقم بمراسلتنا</p>
        <article>
            <img src="<?= site_url() ?>/wp-content/uploads/2023/contact-us/contact-us.svg" alt="">
            <div class="contact-us-form">
                <div> <label for="contact-name">اسمك</label>
                    <input id="contact-name" placeholder="اكتب اسمك هنا" type="text" required>
                </div>
                <div> <label for="contact-email">بريدك الإلكتروني</label>
                    <input id="contact-email" placeholder="اكتب بريدك الالكتروني" type="email" required>
                </div>
                <div> <label for="contact-text">نص الرسالة</label>
                    <textarea name="" placeholder="مثال: أرغب بشراء متجر إلكتروني لديكم..." id="contact-text"
                        required></textarea>
                </div>
                <button class="btn btn-primary">إرسال الرسالة</button>
            </div>
        </article>
        <article class="contact-us-map">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/contact-us/map.svg" alt="">
            <button class="btn btn-outline-primary">مشاهدة على الخريطة</button>
        </article>
    </section>
</div>
<style>

</style>
<?php get_footer(); ?>