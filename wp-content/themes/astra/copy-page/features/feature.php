<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<div style="width:100%">
    <!-- 👉 feature Section  -->
    <section class="container margin-top-section">

        <h3 class="section-header">
            مميزات نراهن عليها
        </h3>
        <p class="section-description">
            استفد من تخفيضات باقات الاستضافة في السنوات التالية بدون أي أقساط إضاية
        </p>
        <article class="feature-container" x-data="{section:[
                    {discription:'ملكية 100 %واشتراك ُيدفع مرة وحدة او يمكنك تقسيطه لمدة عام كامل، فيما عدا اشتراكات االستضافة والدومين أو التحديثات.',img:'/wp-content/uploads/2023/landing-page/image 46.svg',btnIcon: '/wp-content/uploads/2023/landing-page/image1.svg' , btnText : 'ملكية 100 %واشتراك ُيدفع مرة وحدة او يمكنك تقسيطه لمدة عام كامل'},
                    {discription:'نوفر حلول لوجستية ومالية لمتجرك من خلال ربطه مع أفضل شركات الشحن وبوابات الدفع الإلكترونية.',img:'/wp-content/uploads/2023/landing-page/image 47.svg' ,btnIcon: '/wp-content/uploads/2023/landing-page/image2.svg' , btnText : 'الربط مع بوابات الدفع و الشحن'},
                    {discription:'فريق عربي محترف ومتعاون يعمل معك عن قرب لضمان حصولك على أفضل تجربة وتقديم الدعم الفني والتقنياللازم لك..',img:'/wp-content/uploads/2023/landing-page/image 48.svg' ,btnIcon: '/wp-content/uploads/2023/landing-page/image3.svg' , btnText : 'الدعم الفّني باللغة العربية'},
                    {discription:'نقدم لك ضمان لاسترداد اموالك خلال ثلاثين يوم من اشتراك ونضمن لك استرداد فوري من دون آي سؤال. في حال رغبت بايقاف التعاقد خلال اول 30 يوم يتم رد كامل المبلغ لك..',img:'/wp-content/uploads/2023/landing-page/image 49.svg' ,btnIcon: '/wp-content/uploads/2023/landing-page/image4.svg' , btnText : 'ضمان ذهبي'}
                    ],active:0}">
            <ul class="feature-cards ">
                <template x-for=" (item , index) in section" :key="index">
                    <li class="feature-card" x-bind:class="active == index ? 'active-feature-card ' : '' "
                        @click="active=index">
                        <div class="icon-for-list-group">
                            <img :src="'<?= site_url() ?>'+item.btnIcon" alt="">
                        </div>
                        <h5 x-text="item.btnText"
                            x-bind:class="active == index ? 'active-feature-card-paragraph' : '' ">
                        </h5>
                    </li>
                </template>
            </ul>
            <div class="feature-second-card">
                <img class=" img-in-second-section-left" :src="'<?= site_url() ?>' + section[active].img" alt="">
                <p class="  p-in-second-section-left" x-text="section[active].discription"></p>
            </div>
        </article>
    </section>

    <!-- 👉 third Section  -->
    <section class="container important-card-section ">
        <div class="">
            <h3 class="section-header">
                دعمنا لك يستمر
            </h3>
            <p class="section-description">هناك العديد من الميزات التي نتفوق بها دائماً</p>
            <div class="important-cards"
                x-data="{card:[{img:'/wp-content/uploads/2023/landing-page/language.svg',header:'متعدد اللغات',paragraph:'ندعم حتى 5 لغات لعرض منتجاتك على جمهورك.'},
                {img:'/wp-content/uploads/2023/landing-page/wallet.svg',header:'بدون عمولة على المبيعات',paragraph:'من بوابة دفع إلى حسابك مباشرة.. بلا اقتطاع.'},
                {img:'/wp-content/uploads/2023/landing-page/update.svg',header:'تحديثات دورية',paragraph:'نواكب أحدث التقنيات العالمية ونوظفها مباشرة في متجرك.'}]}">
                <template x-for="item in card">
                    <div class="important-card">
                        <img :src="'<?= site_url() ?>'+item.img" alt="" class="img-card-in-fo-section">
                        <h4 x-text="item.header"></h4>
                        <p x-text="item.paragraph"></p>
                    </div>
                </template>

            </div>
            <button class="btn btn-primary ">
                إبدأ تجربتك المجانية <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/arrow-right(1).svg"
                    class="arrow-img-btn">
            </button>
        </div>
    </section>
    <!-- 👉 fourth Section  -->
    <section class="container margin-top-section">
        <h3 class="section-header">
            ميزات غير محدودة
        </h3>
        <p class="section-description">ستحصل على العديد من الخدمات التي تفيدك في متجرك الإلكتروني</p>
        <div class="fourth-section-cards" x-data="
                {cards:
                    [
                   {header:'المزايا الإدارية',img:'/wp-content/uploads/2023/landing-page/bag.svg',items: ['إدارة منتجاتك بسهولة مع التحكم بالصور والأسعار.', 'إدارة الطلبات المرسلة من العملاء.', 'إدارة العملاء وتنظيمهم.','إدارة الموظفين والصلاحيات.']}
                  ,{header:'الدفع والشحن',img:'/wp-content/uploads/2023/landing-page/card.svg',items: ['تفعيل الدفع عند الاستلام', 'تفعيل الدفع بالتحويل البنكي', 'تفعيل الدفع الإلكتروني','الربط مع شركات التوصيل']},
                   {header:'عمليات الدفع وتحصيل الأموال',img:'/wp-content/uploads/2023/landing-page/airplane.svg',items: ['إدارة الفواتير والضرائب','تحليل المبيعات','تقارير مخصصة']},
                   {header:'التسويق',img:'/wp-content/uploads/2023/landing-page/happy-face.svg',items: ['تفعيل قسائم الخصم (نسبة مئوية أو خصم ثابت)', 'تفعيل الخصم على الأسعار (نسبة مئوية أو خصم ثابت)','الربط مع وسائل التواصل الإجتماعي المختلفة']}
                ]
                }">
            <template x-for="card in cards">
                <div class="fourth-section-card">
                    <img :src="'<?= site_url() ?>'+card.img" class="" alt="">
                    <div class="">
                        <h5 x-text="card.header"></h5>
                        <ul class=" ">
                            <template x-for="(item, index) in card.items">
                                <li x-text="item">
                                </li>
                            </template>
                        </ul>
                    </div>
                </div>
            </template>
        </div>
    </section>
    <section class="container margin-top-section">
        <div class="">
            <h3 class="section-header">
                تواصل معنا
            </h3>
            <div class="contact-us">
                <div class=""><label for="email" class="contact-us-email-lebel">ايميلك الشخصي</label>
                    <br>
                    <input class="width-100 contact-us-email" id="email" type="text" placeholder="your email">
                </div>
                <div class=""> <label for="text" class="contact-us-text-lebel">نص الرسالة</label>
                    <br>
                    <textarea class="width-100 contact-us-text" rows="4" id="text"
                        placeholder="مثال : ارغب بشراء متجر الكتروني لديكم."></textarea>
                </div>
                <div class=" submit-group">
                    <p class="">
                        سوف نجيب على رسالتك وقت استلامها
                        <br>
                    </p>
                    <button class="btn btn-primary width-100">ارسال الرسالة</button>
                </div>

            </div>
        </div>
    </section>
</div>
<style>

</style>
<?php get_footer(); ?>