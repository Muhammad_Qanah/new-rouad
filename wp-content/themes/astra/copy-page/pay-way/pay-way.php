<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/pay-way.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/pay-way-min-744px.css" rel="stylesheet"
    media="(min-width:744px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/pay-way-min-1440px.css" rel="stylesheet"
    media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/pay-way-min-1920px.css" rel="stylesheet"
    media="screen and (min-width:1920px)">

<div style="width:100%">
    <section class="pay-way-page container" x-data="{overlay:false,pay:false,pill:false}">
        <div class="pay-way-title"> <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/right-arrow.svg" alt="">
            <h1>اشتراك بخطة التاجر</h1>
        </div>
        <h4>
            دفع الفواتير
        </h4>
        <div class="pay-way-price">
            <p>إجمالي المبلغ المستحق <span> 299 درهم إماراتي</span> </p>
            <div class="code-mobile">
                <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/ticket.svg" alt=""
                    @click="overlay=!overlay;">
            </div>
            <div class="code-laptop">
                <button class="btn btn-outline-secondary" @click="overlay=!overlay;"> <img
                        src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/code-laptop.svg" alt="">
                    إدخال كود حسم</button>
            </div>
        </div>
        <article>
            <div class="pay-way-right">
                <div class="pay-way-card">
                    <div class="pay-way-card-title">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/visa.svg" alt="">
                        <div class="pay-way-card-title-text">
                            <h4>ادفع عن طريق آخر بطاقة مستخدمة</h4>
                            <p>4677 45** **** 4546</p>
                        </div>
                    </div>
                    <input type="radio" name="pay" id="">
                </div>
                <div class="pay-way-hr">
                    <hr>
                    <p>أو</p>
                    <hr>
                </div>
                <div class="pay-way-card">
                    <div class="pay-way-card-title">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/ApplePay.svg" alt="">
                        <div class="pay-way-card-title-text">
                            <h4>ادفع عن طريق آخر بطاقة مستخدمة</h4>
                            <p>متوفرة فقط للبطاقات الصادرة في دولة الإمارات</p>
                        </div>
                    </div>
                    <input type="radio" name="pay" id="">
                </div>
                <div class="pay-way-card">
                    <div class="pay-way-card-title">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/Mastercard.svg" alt="">
                        <div class="pay-way-card-title-text">
                            <h4>ادفع عن طريق آخر بطاقة مستخدمة</h4>
                            <p>يرجى إدخال تفاصيل البطاقة</p>
                        </div>
                    </div>
                    <input type="radio" name="pay" id="">
                </div>
                <div class="pay-way-card">
                    <div class="pay-way-card-title">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/Bank.svg" alt="">
                        <div class="pay-way-card-title-text">
                            <h4>ادفع عن طريق آخر بطاقة مستخدمة</h4>
                            <p>يجب اختيار البنك</p>
                        </div>
                    </div>
                    <input type="radio" name="pay" id="color">
                </div>
            </div>
            <div class="pay-way-left">
                <div class="pill-description">
                    <h4 class="pill-header">الفاتورة</h4>
                    <div class="pill-card">
                        <p>المنتج</p>
                        <h5>موقع إلكتروني متكامل اشتراك شهري لمدة عام</h5>

                    </div>
                    <hr>
                    <div class="pill-card">
                        <p>المبلغ المستحق</p>
                        <h5>299 درهم إماراتي</h5>
                    </div>
                    <hr>

                    <div class="pill-card">
                        <p>إجمالي المبلغ المستحق</p>
                        <h5>299 درهم إماراتي</h5>
                    </div>
                    <hr>

                </div>
                <div class="card-number-block">
                    <label for="card-number" class="">رقم البطاقة</label>
                    <input id="card-number" type="text">
                    <img class="card-number1" src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/visa.svg" alt="">
                    <img class="card-number2" src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/Mastercard.svg"
                        alt="">
                    </input>
                </div>

                <div class="end-date-row">
                    <div class="end-date"> <label for="card-number" class="">انتهاء الصلاحية</label>
                        <input id="" type="date">
                    </div>
                    <div class="end-date"> <label for="card-number" class="">انتهاء الصلاحية</label>
                        <input id="" type="text">
                        <img class="card-img" src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/card.svg" alt="">
                    </div>
                </div>
                <div class="select"><label for="">الدولة</label>
                    <select name="" id="">
                        <option value="e">مصر</option>
                        <option value="s">السعودية</option>
                        <option value="s">سوريا</option>
                    </select>
                </div>
                <button class="btn btn-primary"> <img
                        src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/card-send.svg" alt=""> اشتراك</button>
            </div>




        </article>
        <div class="pill-744">
            <h4 class="pill-header">الفاتورة</h4>
            <div class="pill-card">
                <p>المنتج</p>
                <h5>موقع إلكتروني متكامل اشتراك شهري لمدة عام</h5>
            </div>
            <div class="pill-card">
                <p>المبلغ المستحق</p>
                <h5>299 درهم إماراتي</h5>
            </div>
            <div class="pill-card">
                <p>إجمالي المبلغ المستحق</p>
                <h5>299 درهم إماراتي</h5>
            </div>
        </div>
        <h4 class="check">تفقد الفاتورة</h4>
        <button class=" check-btn btn btn-primary" @click="pill=!pill">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/ApplePaybutton.svg" alt="">
        </button>
        <button class=" check-btn-744 btn btn-primary" @click="pay=!pay">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/ApplePaybutton.svg" alt="">
        </button>

        <div class="bottom-sheet-wrapper mobile tab-744" :class="overlay==true?'show-modal':''" x-show="overlay">
            <div class="backdrop" @click.self="overlay=!overlay" :class="overlay==true?'backdrop-open':''">
                <div class="code-backdrop">
                    <h4><span @click="overlay=!overlay">×</span> إدخال كود الحسم</h4>
                    <label for="code" class="">كود الحسم</label>
                    <input id="code" type="text">
                    <div class="code-btns">
                        <button class="btn btn-primary">متابعة</button>
                        <button class="btn exit-code" @click="overlay=!overlay">إغلاق</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="bottom-sheet-wrapper laptop" :class="overlay==true?'show-modal':''" x-show="overlay">
            <div class="backdrop" :class="overlay==true?'backdrop-open':''">
                <div class="laptop-backdrop">
                    <h4><span @click.self="overlay=!overlay">×</span>إدخال كود الحسم</h4>
                    <label for="code" class="">كود الحسم</label>
                    <input id="code" type="text">
                    <div class="code-btns">
                        <button class="btn btn-primary">متابعة</button>
                        <button class="btn exit-code" @click="overlay=!overlay">إغلاق</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-sheet-wrapper mobile tab-744" :class="pay==true?'show-modal':''" x-show="pay">
            <div class="backdrop" @click.self="pay=!pay" :class="pay==true?'backdrop-open':''">
                <div class="content-backdrop">
                    <div class="backdrop-title">
                        <h4 class="right"><span @click="pay=!pay">×</span> طريقة الدفع</h4>
                        <h4 class="left">بطاقة إئتمان</h4>
                    </div>

                    <label for="card-number" class="">رقم البطاقة</label>
                    <input id="card-number" type="text">
                    <img class="card-number1" src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/visa.svg" alt="">
                    <img class="card-number2" src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/Mastercard.svg"
                        alt="">
                    </input>
                    <div class="end-date-row">
                        <div class="end-date"> <label for="card-number" class="">كود الحسم</label>
                            <input id="" type="date">
                        </div>
                        <div class="end-date"> <label for="card-number" class="">كود الحسم</label>
                            <input id="" type="text">
                        </div>
                    </div>
                    <label for="">الدولة</label>
                    <select name="" id="">
                        <option value="e">مصر</option>
                        <option value="s">السعودية</option>
                        <option value="s">سوريا</option>
                    </select>
                    <button class="btn btn-primary"> <img
                            src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/card-send.svg" alt="">
                        اشتراك</button>
                </div>
            </div>
        </div>

        <div class="bottom-sheet-wrapper mobile" :class="pill==true?'show-modal':''" x-show="pill">
            <div class="backdrop" @click.self="pill=!pill" :class="pill==true?'backdrop-open':''">
                <div class="pill-backdrop">
                    <h4 class="pill-header">الفاتورة</h4>
                    <div class="pill-card">
                        <p>المنتج</p>
                        <h5>موقع إلكتروني متكامل اشتراك شهري لمدة عام</h5>
                    </div>
                    <div class="pill-card">
                        <p>المبلغ المستحق</p>
                        <h5>299 درهم إماراتي</h5>
                    </div>
                    <div class="pill-card">
                        <p>إجمالي المبلغ المستحق</p>
                        <h5>299 درهم إماراتي</h5>
                    </div>
                    <button class="btn btn-primary" @click="pill=false;pay=true;">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/pay-way/card-send.svg" alt="">
                        اشتراك
                    </button>
                </div>
            </div>
        </div>

    </section>

</div>