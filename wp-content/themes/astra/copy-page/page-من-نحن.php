<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/about-us.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/about-us-min-744px.css" rel="stylesheet"
    media="(min-width:744px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/about-us-min-1100px.css" rel="stylesheet"
    media="(min-width:1100px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/about-us-min-1440px.css" rel="stylesheet"
    media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/about-us-min-1920px.css" rel="stylesheet"
    media="(min-width:1920px)">
<div style="width:100%" class="about-us-page">
    <section class="about-us-title container">
        <h4>من نحن</h4>
        <p>منصّة روّاد هي شركة إماراتية رائدة في مجال التجارة الإلكترونية</p>
    </section>
    <section class="about-us-building-section ">
        <picture>
            <source srcset="<?= site_url() ?>/wp-content/uploads/2023/about-us/about-us-1440.svg"
                media="(min-width: 1440px)">
            <source srcset="<?= site_url() ?>/wp-content/uploads/2023/about-us/about-us.svg">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/about-us/about us.png" alt="">
        </picture>

        <p class="container">شركة إماراتية رائدة في مجال التجارة الإلكترونية تقدم خدماتها للشركات والأنشطة التجارية في
            الخليج والدول العربية... تخلق مجتمعاً جديداً لرواد الأعمال والناشئين في الأعمال التجارية، لتمنحهم فرصة
            الوصول إلى بوابة التجارة الإلكترونية وتسديد التكاليف المترتبة عليها من أرباحهم.
            وتعدّ بوابة للدخول إلى عالم التجارة الإلكترونية لتسويق المشاريع وعرض المنتجات، بخدمات متكاملة من الألف إلى
            الياء، بأسهل وأيسر طريقة وأقلّ تكلفة.</p>
    </section>
    <section class="about-us-rouad-team container">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/about-us/rouad-team.svg" alt="">
        <h4>
            فريق منصّة روّاد
        </h4>
        <p>لدينا فريق عربي شغوف ومتميز يعمل على إنجاز المهام بطريقة إبداعية منذ تأسيس منصّة روّاد بدأنا بإعداد فريق
            متخصص من المهندسين والتقنيين للوصول إلى جودة عالية في الخدمات المتعلقة بالمتاجر الإلكترونية. ابتداءً
            بالتأسيس والبرمجة ،مروراً بالاختبار وحتى الوصول إلى خدمات مابعد البيع.</p>
    </section>
    <section class="about-us-whay-rouad container">
        <h4>لماذا روّاد؟</h4>
        <article>
            <img src="<?= site_url() ?>/wp-content/uploads/2023/about-us/whay-rouad.svg" alt="">
            <ul>
                <li>
                    <h3 class="why-rouad-title">
                        لأجل توفير جهدك ومالك ووقتك
                    </h3>
                    <p class="why-rouad-text">
                        استخدام التقنية في تنفيذ أعمالك يحتاج لميزانية متكاملة من الوقت والمال، خصوصاً مع محدودية
                        الحلول التقنية في العالم العربي مقارنة بالحلول المتوفرة باللغات الأخرى.
                    </p>
                </li>
                <li>
                    <h3 class="why-rouad-title">
                        لأننا حولك دائماً
                    </h3>
                    <p class="why-rouad-text">
                        لدينا فريق عظيم، التمّيز شغفه، والإبداع طريقه، خبرة ودبرة في الهندسة والتقنية، خدماتنا
                        التقنية يبرمجونها ويأسسونها صح، ثم يختبرونها.. وحتى بعد بيع الخدمة نحن معك دائماً وجاهزون
                        لمساعدتك.
                    </p>
                </li>
                <li>
                    <h3 class="why-rouad-title">
                        لأن التقنية معنا أسهل
                    </h3>
                    <p class="why-rouad-text">
                        سهّلنا، بسطنا، يسّرنا دخول التقنية لقطاع الأعمال، بطريقة سهلة وبسيطة ومفهومة ومتاحة بشكل
                        فوري.
                    </p>
                </li>
            </ul>
        </article>

    </section>
    <section class="about-us-rouad-story container">
        <h4>قصّتنا </h4>

        <article>
            <p style="text-align: justify;">
                قصّتنا بدأت بالخُلاصة!
                <br>
                إنها خلاصة جهد وبحث وتجارب.. لتقدم لكم خدماتنا بجودة عالية وسلاسة ومرونة، مختصرة آلاف الخطوات ومُجيبة عن
                عشرات الأسئلة بثقةٍ وتمكّن.
                <br>
                منذ عام 2008م كانت الشرارة الأولى نتاج الاحتكاك لعشرات الأفكار الرياديّة المتنوعة في مجالاتها؛ التسويق
                بالعمولة، تقديم الخدمات المتنوعة، والمنتجات الرقمية... ومهما تنوّعت الأفكار يظل التحدّي التقني أصعب
                التحديات التي تقف حائلاً بيني وبين الانطلاق لتحقيق أحلامي؛ من عمل متجرٍ بسيط إلى تنفيذ منصّات متطورة،
                لتبدأ رحلة من التعاملات مع شركات تطوير المواقع، ومع المستقلين في أنحاء العالم، لتأتي تباعًا تحديات توفير
                المبالغ المالية الكبيرة نظير تقديم خدمة لستُ متأكدًا من جودتها أو استيفائها على المواعيد المُبرمة..
                <br>
                انتهى ذلك الفصل، وبدأت رحلة التعلّم؛ لأنفذ أفكاري بنفسي توفيرًا للوقت والجهد والمُتابعة، فما حاك ظهرك
                مثل ظفرك! لأخرج من هذه الرحلة بدرسٍ جوهري: نظرة أغلب التقنيين لا تتجاوز التقنية نفسها، بينما نظرتي
                تتجاوز التقنية إلى أثرها على مشروعي ونجاحه.
                <br>
                في عام 2021م أطلقتُ "روّاد" أول منصّة عربية أسعى من خلالها إلى تقديم حلول مُبتكرة لروّاد الأعمال العرب،
                وأختصر عليهم الطريق بتوظيفها بوصلةً أساسية لتقديم حلول مشاكل أصحاب المشاريع المتعلقة بتصميم المواقع
                والتطبيقات، والتسويق الإلكتروني.
                <br>
                قضيّتي في روّاد هي مشروعك وخلق قيمة فريدة تأخذ بيدك إلى نجاحه ومن ثم استمراره، بتكلفةٍ رمزيّة تضمن بقاء
                هذه المنصّة لمساعدة المزيد من روّاد الأعمال.
                <br>
                <br>
                محمود الرنتيسي


            </p>
        </article>
    </section>
    <section class="about-us-arabic-techno container">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/about-us/arabic-techno.svg" alt="">
        <h4>
            لماذا لا يكون العرب هم روّاد التّقنية ، فكانت روّاد لخلق بصمة عربيّة في عالم التقنية
        </h4>
    </section>
    <!-- 👉 FAQs -->

    <section class=" container margin-top-section">

        <h3 class="section-header">
            الأسئلة الشائعة
        </h3>
        <article class="quistion-article" x-data="{section:[
{quistion:'مالفرق بين نظام األقساط في ّ رواد ونظام االشتراك المتعارف عليه؟',answer:'في نظام الاشتراك يتوجب على المشترك دفع مبلغ شهري بشكل دائم وطوال سنوات وجوده على المنصة، أما في نظام التقسيط الذي نقدمه لعملائنا فالدفع يتم فقط لمدة سنة واحدة وبعدها يتمتع العميل بملكية كاملة للبيانات والمتجر' },
{quistion:'هل يمكنني الحصول على بيانات المتجر بعد انتهاء السنة الأولى؟',answer:'نعم، يمكنك امتلاك متجرك بشكل كامل والحصول على بياناته بعد انقضاء السنة الأولى وتملك الحرية في إبقاء متجرك في لدينا للحصول على خدمات الاستضافة والتحديثات أو نقله بكل بساطة.' },
{quistion:'ما هي الالتزامات المادية إذا اخترت الاستمرار في العمل معكم بعد انقضاء السنة الأولى؟',answer:'يمكنك الاستمرار معنا، وتكون قد انتهيت من التزاماتك المادية اتجاه المنصة من حيث أقساط المتجر، ويمكنك الاختيار بين باقة الاستضافة و تجديد الدومين او استمرار اشتراكك كما هو والذي يشمل استضافة وتجديد دومين مجانا بالإضافة الى تحديثات ودعم فني '},
{quistion:'ماهي تكلفة الاستضافة على سيرفرات منصّة روّاد؟',list:['1 .باقة التجار: (موقع واحد، عدد زوار 50 ألف زائر، مساحة االستضافة 10 غيغابايت)السعر (39 درهم شهرياً) تدفع سنوياً','2 .باقة نمو: (موقع واحد، عدد زوار 200 ألف زائر، مساحة االستضافة 50 غيغابايت)السعر (59 درهم شهرياً) تدفع سنوياً','3 .باقة ريادة: (موقع واحد، عدد زوار 500 ألف زائر، مساحة االستضافة 100 غيغابايت)السعر (99 درهم شهرياً) تدفع سنويا']},
{quistion:'أين يقع مقر شركتكم؟',answer:'الإمارات العربية المتحدة، دبي، الخليج التجاري، برج تماني آرت.'},
{quistion:'هل يمكنني بناء متجري الخاص بدون الحاجة إلى معرفة برمجية؟',answer:'الهدف الرئيسي من رواد هو الوصول إلى عالم التجارة الإلكترونية من قبل التجار الذين لا يملكون المعرفة البرمجية وينتظرون الفرصة لتطوير مبيعاتهم بطرق إبداعية وبسيطة وبتكلفة معقولة ، فمن خلال لوحة التحكم الخاصة بك يمكنك إدارة متجرك والتعديل عليه بما يتناسب مع طبيعة النشاط التجاري.'},
{quistion:'كيف أكون واثقاً بأن بياناتي محمية مع ّ رواد؟',answer:'من خلال فريقنا المتخصص في أمن المعلومات يمكنك الوثوق بأن بياناتك محمية بشكل كامل، حيث يتم إنشاء شهادة أمان SSL للمتاجر المجانية أو المدفوعة بشكل آلي. يعمل فريقنا التقني على تحديث المتاجر المشتركة لدينا بشكل دوري وسد أي ثغرات امنية'},
{quistion:'هل يمكنني حجز دومين جديد أو نقل بيانات متجر رواد إلى دوميني الخاص؟',answer:'نعم هذه الخيارات متاحة بشكل آلي، حيث يمكنك حجز الدومين الذي ترغب فيه لمتجرك بشكل مجاني، وإذا كنت تمتلك دومين قديم يمكنك نقل بيانات متجر رواد إليه بطريقة بسيطة وسهلة'},
{quistion:'ما هو الضمان الذهبي من رواد؟',list:['لثقتنا الكبرى بجودة خدماتنا المقدمة نقدم لجميع مشتركينا ضمان استرداد الأموال 100% في حال عدم رضاهم خلال الشهر الأول من الاشتراك وذلك بشكل فوري ومن دون أي سؤال ','ملاحظة : لا يشمل مبلغ حجز النطاقات ويتم تحميل مبلغ 50 درهم للدومين المجاني الذي يتم حجزه من خلال الباقات من حال الرغبة في استراد المبلغ']}
],active:0}">
            <ul class="list-quistion">
                <template x-for=" (item , index) in section" :key="index">
                    <li class="btn-quistion " x-bind:class="active == index ? 'active-quistion' : '' "
                        @click="active=index" style="opacity: 0.5;color: #5C5C5C">
                        <div class="btn-quistion-img">
                            <img class="cercle-list" loading="lazy"
                                src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/Ellipse.svg"
                                :class="active==index?'cercle-list-active':''">
                            <p class="paragraph-quistion " x-text="item.quistion"></p>
                            <img class="img-list arrow" loading="lazy"
                                src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/down.svg" alt="shape"
                                :class="active==index?'rotate-arrow':''">
                            <img class="img-list plus" loading="lazy"
                                :src="active!=index?'<?= site_url() ?>/wp-content/uploads/2023/landing-page/open question.svg':'<?= site_url() ?>/wp-content/uploads/2023/landing-page/close question.svg'">
                        </div>
                        <div class="bottom-quistion">
                            <template x-if="item.answer && index === active">
                                <p class=" answer" x-show="index === active" x-text="item.answer"></p>
                            </template>
                            <template x-if="item.list && index === active">
                                <ul>
                                    <template x-for="item in section[active].list">
                                        <li>
                                            <p x-text="item"></p>
                                        </li>
                                    </template>
                                </ul>
                            </template>

                        </div>
                    </li>
                </template>
            </ul>
            <div class="left-quistion">
                <h3 class="" x-text="section[active].quistion"></h3>
                <p class=" " x-text="section[active].answer"></p>
                <ul>
                    <template x-for="item in section[active].list">
                        <li>
                            <p x-text="item"></p>
                        </li>
                    </template>
                </ul>

            </div>
        </article>
    </section>
    <section class="container margin-top-section">
        <div class="">
            <h3 class="section-header">
                تواصل معنا
            </h3>
            <div class="contact-us">
                <div class=""><label for="email" class="contact-us-email-lebel">ايميلك الشخصي</label>
                    <br>
                    <input class="width-100 contact-us-email" id="email" type="text" placeholder="your email ">
                </div>
                <div class=""> <label for="text" class="contact-us-text-lebel">نص الرسالة</label>
                    <br>
                    <textarea class="width-100 contact-us-text" rows="4" id="text"
                        placeholder="مثال : ارغب بشراء متجر الكتروني لديكم."></textarea>
                </div>
                <div class=" submit-group">
                    <p class="">
                        سوف نجيب على رسالتك وقت استلامها
                        <br>
                    </p>
                    <button class="btn btn-primary width-100">ارسال الرسالة</button>
                </div>

            </div>
        </div>
    </section>
</div>
<style>

</style>
<?php get_footer(); ?>