<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/articles.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/articles-min-1440px.css" rel="stylesheet"
    media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/articles-min-1920px.css" rel="stylesheet"
    media="(min-width:1920px)">
<div style="width:100%" class="about-us-page">
    <div class="articles-category container"
        x-data="{categoryActive:{active:true,id:0},articleActive:{active:false,id:0},category:[
        {categoryHeader:'التسويق',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(2).svg' ,categoryId:1 ,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/second-article-image.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:2}]}
       ,{categoryHeader:'الإعلانات المموّلة',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(3).svg',categoryId:2,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/second-article-image.svg',articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم' ,articleId:2}]}
       ,{categoryHeader:'استراتيجيات التسويق',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(4).svg',categoryId:3,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'Rectangle 3(2).svg',articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم' ,articleId:2}]}
       ,{categoryHeader:'غير مصنف',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(5).svg',categoryId:4,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/second-article-image.svg',articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم' ,articleId:2}]}
       ,{categoryHeader:'العمل عن بعد',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(6).svg',categoryId:5,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/second-article-image.svg',articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم' ,articleId:2}]}
       ,{categoryHeader:'التصميم الجرافيكي',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(7).svg',categoryId:6,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/second-article-image.svg',articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم' ,articleId:2}]}
       ,{categoryHeader:'وسائل التواصل الإجتماعي',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(8).svg',categoryId:7,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/second-article-image.svg',articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم' ,articleId:2}]}
       ,{categoryHeader:'نصائح لأصحاب الأعمال',categoryImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/Rectangle 3(9).svg',categoryId:8,articleInCategory:[{articleHeader:'دور فريق التصميم في الشركات الناشئة وهل يمكن الاستغناء عنه؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg' ,articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم',articleId:1},{articleHeader:'المصمم الأفضل للعلامة التجارية كيف نختاره وفق معايير جودة التصميم؟',articleImg:'<?= site_url() ?>/wp-content/uploads/2023/articles/second-article-image.svg',articleDescription:'أن تختارَ المصمم الأفضل للعلامة التجارية يعني أن تنشئ هويّةً جديدة لعملك، سواءَ كنت في بداية الدرب أو ترغب بإحياء نشاط شركتك. يعدّ اختيار المصمم' ,articleId:2}]}]}">
        <h3 x-show="categoryActive.active==true"> المقالات</h3>
        <section class="cards-section">
            <div class="card-page-1"> <template x-for="(card,index) in category" :key="index">
                    <article :class=" index> 5 ? 'last-cards': '' "
                        @click="categoryActive.id = card.categoryId; categoryActive.active=false ; articleActive.active=true"
                        x-show="categoryActive.active==true" x-transition:enter.duration.300ms
                        x-transition:leave.duration.200ms>
                        <img :src=" card.categoryImg" alt="" />
                        <h4 x-text="card.categoryHeader"></h4>
                    </article>
                </template>
            </div>
            <!-- Second page -->
            <div class="second-page" x-show="categoryActive.active==false && articleActive.active==true > 0 "
                x-transition:enter.duration.300ms x-transition:leave.duration.200ms>
                <div class="title-second-article">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/down.svg" alt=""
                        @click="categoryActive.active=true ; articleActive.active=false">
                    <h1 x-text="categoryActive.id==0?'':category[categoryActive.id-1].categoryHeader">
                    </h1>
                </div>
                <div class="card">
                    <template
                        x-for="(card,index) in categoryActive.id==0?0:category[categoryActive.id-1].articleInCategory"
                        :key="index">
                        <article>
                            <img :src=" card.articleImg" alt="" />
                            <h4 x-text="card.articleHeader"></h4>
                            <p x-text="card.articleDescription"></p>
                            <button class="btn btn-outline-primary"
                                @click="articleActive.id=card.articleId ; articleActive.active=false"> قراءة
                                المزيد</button>
                        </article>
                    </template>
                </div>
            </div>
            <!-- third page -->
            <div class="third-page" x-show="articleActive.active==false && categoryActive.active==false "
                x-transition:enter.duration.300ms x-transition:leave.duration.200ms>
                <div class="title-third-article">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/down.svg" alt=""
                        @click="articleActive.active=true">
                    <h1
                        x-text="articleActive.id==0 && categoryActive.id==0?'':category[categoryActive.id-1].articleInCategory[articleActive.id-1].articleHeader">
                    </h1>

                </div>
                <div class="article-page3-first">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/pres.svg" alt="">
                    <div>
                        <div> <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/man-head.svg" alt="">
                            <h4>Muhammad Alasmar</h4>
                        </div>

                        <p>16 فبراير، 2023</p>
                    </div>
                </div>
                <article class="article-page3-second">
                    <p>يساهم فريق التصميم في خلق فرصٍ رائعة للشركات الناشئة ويجلب لها العديد من الفوائد التجارية.
                    <p>
                        فتوظيف فريق جيّد ومبدع من المصممين يعني خلقَ أفكار جديدة واكتشاف احتياجات العملاء، فضلاً عن
                        تحويل الأفكار
                        إلى خدمات مبتكرة ومناسبة لسوق العمل وبالتالي يكون العمل من بدايته على درجةٍ عالية من الكفاءة.
                    </p>
                    <p>
                        لنتعرف على دور فريق التصميم في الشركات الناشئة وفوائده وكيف يمكن للشركات أن تستخدم هذه الخدمات
                        لصالح عملها.
                    </p>
                    </p>
                </article>
                <article class="article-page3-third">
                    <h4>فوائد فريق التصميم في الشركات الناشئة</h4>
                    <p>إن وجود فريق تصميم في شركتك يحسّن اداء الأعمال، فقد يجلب التصميم مجموعة من الفوائد التجارية في
                        حال كان عمله
                        ممنهجاً، ومن هذه الفوائد:</p>

                    <ul x-data="{lists:['زيادة وعي الناس بالشركة والحصول على قاعدة جماهيرية وعملاء كُثر.',
             'القدرة على إنشاء منتجات وخدمات تناسب العملاء المستهدفين.',
               'تقليل الوقت اللازم لتسويق المنتجات والخدمات الجديدة.',
                'التصميم الفعال يدفع العملاء للشراء منك وليس من منافسيك.',
                'يدفع العملاء أكثر للخدمات المصممة بشكل جيّد والتي تقدم لهم مزايا مميزة.',
             'يقلل التصميم الجيد من تكاليف الإنتاج ويجعل المواد أكثر كفاءة وصديقة للبيئة.'
                   ]}">
                        <template x-for="list in lists">
                            <li x-text="list">
                            </li>
                        </template>
                    </ul>
                </article>
                <article class="article-page3-forth">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/page-3.svg" alt="">
                    <h4>
                        كيف يمكن الاستفادة من فريق التصميم في الشركات؟
                    </h4>
                    <article
                        x-data="{paragraph:['يعتقد بعض أصحاب الشركات أنّ التصميم يعني المظهر الخارجي للمنتجات أو العناصر الرسومية كموقع الويب الخاص والتعبئة والتغليف ومواد التسويق، لكنّ الأمر أعمق بكثير.','لفريق التصميم دور في كلّ جانب من جوانب الشركة تقريباً، لذلك يجب خلق نظرة متسقة للشركة من خلال التصميم، كما أنه عمله يتعلق بإدارة عمليات العمل لتكون فعالة ومنخفضة التكلفة قدر الإمكان.','من الجيّد أن يقوم فريق التصميم في شركتك بإجراء تدقيق للتصميم من خلال النظر إلى المجالات الرئيسية في العمل، كالعلامة التجارية وتطوير المنتج والخدمة، والتواصل مع العملاء.','ولذلك يجب اعتبار دور التصميم مهمّاً في جعل العمل أكثر كفاءة ولإضافة قيمة إلى منتجاتك وخدماتك.']}">
                        <template x-for="item in paragraph">
                            <p x-text="item"></p>
                        </template>
                        <p class="special-paragraph">
                            اقرأ أيضاً: <span class="colored-paragraph">كيف تضمن نحاج الإعلان الممول عبر وسائل التواصل
                                الاجتماعي.</span>
                        </p>
                    </article>
                </article>
                <article class="article-page3-fifth">
                    <h4>
                        اجعل عمل فريق التصميم جزءاً من استراتيجية عمل الشركة
                    </h4>
                    <article
                        x-data="{paragraph:['لا تكن كبقية أصحاب الأعمال والشركات الذين أهملوا عمل قسم التصميم فلم يحصدوا نتائج العمل التي توقعوها.','هذا القسم يستحق مزيداً من الاهتمام، فهو ليس اللمسة النهائية لتطوير المنتج كما أنه ليس خطوة نقوم بها بعد صياغة الاستراتيجية واتخاذ القرارات الرئيسية.','العمل الناجح يعني كون التصميم جزءاً من استراتيجية عمل الشركة منذ البداية، فتضمين فريق عمل التصميم في مرحلة مبكرة قد يوفر المال وينتج عنه عروض أفضل.']}">
                        <template x-for="item in paragraph">
                            <p x-text="item"></p>
                        </template>
                        <p class="header-for-ul">من المهم أن تتضمن خطواتك الأولى لاستخدام فريق التصميم بشكل استراتيجي
                            مايلي:</p>
                        <ul x-data="{lists:['تحديد طرق لتحسين عملية التصميم وتطويرها كزيادة مشاركة الإدارة أو اللجوء إلى استشاري تصميم محترف.',
               'تحديد مكان وكيفية استخدام التصميم في العمل.',
             'تقليل الوقت اللازم لتسويق المنتجات والخدمات الجديدة.',
                'التأكد من إبراز اعتبارات فريق التصميم في جميع الاجتماعات ومستندات تخطيط الأعمال الخاصة بالشركة.',
                  'إجراء أبحاث السوق للتأكد من أن فريقك يعرف ما يحتاجه عملاؤك.'
                         ]}">
                            <template x-for="list in lists">
                                <li x-text="list">
                                </li>
                            </template>
                        </ul>
                    </article>
                </article>
                <article class="article-page3-six">
                    <h4>
                        دور فريق التصميم يكمن في تحسين تطوير المنتج
                    </h4>
                    <article
                        x-data="{paragraph:['إن الأمر الأساسي لانطلاق عملك بقوة يكمن في معرفة واكتشاف ما يريده العملاء وبذلك يمكن دخول سوق العمل بروح تنافسية وبمنتجات وخدمات مناسبة.','لذلك فكلما عمل فريق التصميم على اكتشاف تفضيلات العملاء من خلال أبحاث السوق وأبحاث المستخدم التي يقودها التصميم، زادت احتمالية تصميم المنتجات والخدمات الملائمة للعملاء.','ولذلك يجب اعتبار دور التصميم مهمّاً في جعل العمل أكثر كفاءة ولإضافة قيمة إلى منتجاتك وخدماتك.']}">
                        <template x-for="item in paragraph">
                            <p x-text="item"></p>
                        </template>
                        <p class="header-for-ul">يتضمن عمل فريق التصميم الخطوات التالية لتطوير المنتج :</p>
                        <ul x-data="{lists:['رسم التصميم الأولي.',
                           'إجراء بحث المستخدم.',
                           'تطوير الفكرة لمفهوم أو منتج.',
                           'تحديد الحاجات المطلوبة لإنتاجها من الأشخاص والمواد.',
                           'إنشاء نموذج أولي.',
                           'إجراء تجارب المستخدم لتقييم الخدمة.','إجراء المراجعات بعد تجارب المستخدم واعتماد التصميم النهائي.'
                                         ]}">
                            <template x-for="list in lists">
                                <li x-text="list">
                                </li>
                            </template>
                        </ul>
                        <p class="special-paragraph">
                            اقرأ أيضاً: <span class="colored-paragraph">كيف تضمن نحاج الإعلان الممول عبر وسائل التواصل
                                الاجتماعي.</span>
                        </p>
                    </article>
                </article>
                <article class="article-page3-seven">
                    <h4>شارك المقال على :</h4>
                    <button class="btn btn-outline-primary">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/facebook.svg" alt="">
                    </button>
                    <button class="btn btn-outline-primary">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/tweter.svg" alt="">
                    </button>
                    <button class="btn btn-outline-primary">
                        <img src="<?= site_url() ?>/wp-content/uploads/2023/articles/linkedin.svg" alt="">
                    </button>
                </article>
                <article class="article-page3-eight">
                    <h4>
                        اترك تعليقاً :</h4>
                    <article class="comments"
                        x-data="{comments:[{avatar:'<?= site_url() ?>/wp-content/uploads/2023/articles/Avatar.svg',header:'Maude Hall',date:'14 min',comment:'Thats a fantastic new app feature. You and your team did an excellent job of incorporating user testing feedback.'}]}">
                        <template x-for="(item,index) in comments" :key="index">
                            <div>
                                <div class="comment-title">
                                    <img :src="item.avatar" alt="">
                                    <div class="comment-title-text">
                                        <h4 x-text="item.header"></h4>
                                        <p x-text="item.date"></p>
                                    </div>

                                </div>
                                <p x-text="item.comment" class="comment-text"></p>
                                <hr>
                            </div>

                        </template>
                        <h4 class="more-comment">
                            عرض المزيد من التعليقات ...
                        </h4>
                        <div class=" add-comment">
                            <div class="comment-title">
                                <img :src="<?= site_url() ?>/wp-content/uploads/2023/articles/Avatar.svg" alt="">
                                <div class="comment-title-text">
                                    <h4 x-text="item.header"></h4>
                                </div>
                            </div>
                            <label class="" for="comment">
                                تعليق
                            </label>
                            <textarea placeholder="أحببت هذه المقالة كثيراً، هي مفيدة للغاية !" id="comment"></textarea>
                            <button class="btn btn-primary">تعليق</button>
                        </div>
                    </article>
                </article>
                <article class="article-page3-nine">
                    <h4>مقالات يمكن أن تعجبك</h4>
                    <div class="card">
                        <template
                            x-for="(card,index) in categoryActive.id==0?0:category[categoryActive.id-1].articleInCategory"
                            :key="index">
                            <article>
                                <img :src=" card.articleImg" alt="" />
                                <h4 x-text="card.articleHeader"></h4>
                                <p x-text="card.articleDescription"></p>
                                <button class="btn btn-outline-primary"
                                    @click="articleActive.id=card.articleId ; articleActive.active=false"> قراءة
                                    المزيد</button>
                            </article>
                        </template>
                    </div>
                </article>
            </div>
        </section>
        <section class=" create-free-account" x-show="categoryActive.active==true || articleActive.active==true">
            <h4>أنشئ حساب مجاني الآن</h4>
            <p>مع منصّة روّاد امتلك متجرك الإلكتروني اليوم وقسّط قيمته على دفعات شهرية لمدة عام واحد فقط</p>
            <button class="btn btn-primary">إنشاء حساب مجاني</button>
        </section>
        <!-- 👉 fourth Section  -->
        <section x-show="categoryActive.active==true" class="container margin-top-section">
            <h3 class="section-header">
                ميزات غير محدودة
            </h3>
            <p class="section-description">ستحصل على العديد من الخدمات التي تفيدك في متجرك الإلكتروني</p>
            <div class="fourth-section-cards" x-data="
                {cards:
                    [
                   {header:'المزايا الإدارية',img:'/wp-content/uploads/2023/landing-page/bag.svg',items: ['إدارة منتجاتك بسهولة مع التحكم بالصور والأسعار.', 'إدارة الطلبات المرسلة من العملاء.', 'إدارة العملاء وتنظيمهم.','إدارة الموظفين والصلاحيات.']}
                  ,{header:'الدفع والشحن',img:'/wp-content/uploads/2023/landing-page/card.svg',items: ['تفعيل الدفع عند الاستلام', 'تفعيل الدفع بالتحويل البنكي', 'تفعيل الدفع الإلكتروني','الربط مع شركات التوصيل']},
                   {header:'عمليات الدفع وتحصيل الأموال',img:'/wp-content/uploads/2023/landing-page/airplane.svg',items: ['إدارة الفواتير والضرائب','تحليل المبيعات','تقارير مخصصة']},
                   {header:'التسويق',img:'/wp-content/uploads/2023/landing-page/happy-face.svg',items: ['تفعيل قسائم الخصم (نسبة مئوية أو خصم ثابت)', 'تفعيل الخصم على الأسعار (نسبة مئوية أو خصم ثابت)','الربط مع وسائل التواصل الإجتماعي المختلفة']}
                ]
                }">
                <template x-for="card in cards">
                    <div class="fourth-section-card">
                        <img :src="'<?= site_url() ?>'+card.img" class="" alt="">
                        <div class="">
                            <h5 x-text="card.header"></h5>
                            <ul class=" ">
                                <template x-for="(item, index) in card.items">
                                    <li x-text="item">
                                    </li>
                                </template>
                            </ul>
                        </div>
                    </div>
                </template>
            </div>
        </section>
    </div>

</div>
<?php get_footer(); ?>