<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>
</div> <!-- ast-container -->
</div><!-- #content -->
<?php
//astra_content_after();

//astra_footer_before();

//astra_footer();

//astra_footer_after();
?>
</div><!-- #page -->
<?php
//astra_body_bottom();
wp_footer();
?>

<footer class=" footer   margin-top-section">
    <div class="" style="text-align: center">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/cercle.svg" alt=""
             class="footer-img cercle-top">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/triangle.svg" alt=""
             class="footer-img triangle">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/cercle.svg" alt=""
             class="footer-img cercle-bottom">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/rectangle.svg" alt=""
             class="footer-img rectangle">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/logo-footer.svg" alt="" class="logo">
        <h4 class="fotter-header container">امتلك متجرك الان!</h4>
        <p class="footer-paragraph-font container"> تأسست منصّة روّاد في دبي عام 2021 وتركز على تقديم حلول تقنية لرواد
            الأعمال</p>
        <button class="btn btn-secondary fotter-btn"> انشي متجرك الالكتروني الان <img
                    src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/arrow-right(1).svg" alt=""></button>

        <div class="container footer-group">
            <div class="contact-btns"
                 x-data="{buttons:[
                    {img:'/wp-content/uploads/2023/landing-page/facebook.svg',url:'https://www.facebook.com/Rouad.tech?mibextid=ZbWKwL'},
                    {img:'/wp-content/uploads/2023/landing-page/insta.svg',url:'https://instagram.com/rouad.tech?igshid=ZDdkNTZiNTM='},
                    {img:'/wp-content/uploads/2023/landing-page/youtube.svg',url:'https://youtube.com/@rouadtech'},
                    {img:'/wp-content/uploads/2023/landing-page/tiktok.svg',url:'https://www.tiktok.com/@rouad.tech?_t=8asaToMEDr8&_r=1'},
                    {img:'/wp-content/uploads/2023/landing-page/snapchat.svg',url:'https://www.snapchat.com/add/rouad.tech?share_id=yhz8toO35Us&locale=en-US'}]}">
                <template x-for="btn in buttons">
                    <a :href='btn.url' target="_blank">
                        <button type="button" class="btn btn-outline-white contact-btn">
                            <img :src="'<?= site_url() ?>'+btn.img" alt="shape"></button>
                    </a>
                </template>
            </div>
            <div class="footer-links" x-data="{
                    links:[
        {url:'/من-نحن',title:'من نحن' , slug:'%D9%85%D9%86-%D9%86%D8%AD%D9%86'},{url:'/contact-us',title:'تواصل معنا'},{url:'/privacy-policy',title:'سياسة الخصوصية'}

                    ]
                }">
                <template x-for="(link,index) in links" :key="index">
                    <div>
                        <a :href="'<?= site_url() ?>'+link.url" x-text="link.title">


                        </a>
                        <template x-if="index<2">
                            <span>.</span>

                        </template>
                    </div>
                </template>
            </div>
            <p class="paragraph-font m-auto">المقر الرئيسي: الإمارات العربية المتحدة - دبي - الخليج التجاري - برج تماني
                أرتس - مكتب رقم 543</p>
            <div class="made-by">
                <div class="">صنع بواسطة</div>
                <img class="rouad-logo" src="<?= site_url() ?>/wp-content/uploads/2023/landing-page/logo-w.png"
                     alt="logo">
            </div>
        </div>
    </div>

    <!-- <div class="first-footer-section">
        <div class="flex-section">
            <div class="footer-right">
                <div class="footer-logo">
                    <img class="footer-logo-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/logo/logo.png">
                </div>
                <div class="footer-title">
                    تأسّست منصة روّاد في دبي عام 2021 وتركّز على تقديم حلول تقنية لروّاد الأعمال.
                </div>
                <div class="footer-social-icon">
                    <a href="https://www.facebook.com/rowadplatform">
                        <div class="social-box">
                            <i class="fab fa-facebook social-box1"></i>
                        </div>
                    </a>
                    <a href="https://www.instagram.com/rouad.tech/">
                        <div class="social-box">
                            <i class="fab fa-instagram social-box1"></i>
                        </div>
                    </a>
                    <a href="https://www.snapchat.com/add/rouad.tech?share_id=OBrj_1FlzP0&locale=en-US">
                        <div class="social-box">
                            <i class="fab fa-snapchat social-box1"></i>
                        </div>
                    </a>
                    <a href="https://www.linkedin.com/company/darb-productions/">
                        <div class="social-box">
                            <i class="fab fa-linkedin social-box1"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="footer-mid-right">
                <div class="footer-title-rouad">
                    <?= __("روّاد") ?>
                </div>

                <div class="footer-menu-mid-right">
                    <a href="<?= site_url() ?>/%d9%82%d8%b5%d8%aa%d9%86%d8%a7/">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("قصتنا") ?></div>
                        </div>
                    </a>
                    <a href="<?= site_url() ?>/why-rouad/">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("لماذا روّاد؟") ?></div>
                        </div>
                    </a>

                </div>
            </div>
        </div>
        <div class="flex-section">
            <div class="footer-mid-right">
                <div class="footer-title-rouad">
                    <?= __("روابط مفيدة") ?>
                </div>

                <div class="footer-menu-mid-right">
                    <a href="https://goo.gl/maps/Kz4JQBaMb6Rw5Mf8A">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("العنوان على خرائط جوجل") ?></div>
                        </div>
                    </a>
                    <a href="<?= site_url() ?>/?page_id=3">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __(" سياسة الخصوصية؟") ?></div>
                        </div>
                    </a>

                </div>
            </div>
            <div class="footer-mid-right">
                <div class="footer-title-rouad">
                    <?= __(" الدعم والمساعدة") ?>
                </div>

                <div class="footer-menu-mid-right">
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("المبيعات") ?></div>
                        </div>
                    </a>
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("الدعم الفني") ?></div>
                        </div>
                    </a>
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("الادارة المالية") ?></div>
                        </div>
                    </a>
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("واتس اب") ?></div>
                        </div>
                    </a>
                    <a href="<?= site_url() ?>/%D8%AA%D9%88%D8%A7%D8%B5%D9%84-%D9%85%D8%B9%D9%86%D8%A7/"">
                    <div class=" menu-item">
                        <i aria-hidden="true" class="fas fa-chevron-left"></i>
                        <div class="menu-element"><?= __("تواصل معنا") ?></div>
                </div>
                </a>

            </div>
        </div>
    </div>
    </div>
    <div class="second-footer-section-color">
        <div class="second-footer-section">
            <div class="rouad-adress">
                <?= __("المقر الرئيسي: الإمارات العربية المتحدة -دبي - الخليج التجاري - برج تماني ارتس - مكتب رقم 543") ?>
            </div>
            <div class="rouad-footer-logo">
                <a href="<?= site_url() ?>">
                    <?= __("صُنع بواسطة منصة روّاد ") ?>
                    <img src="<?= site_url() ?>/wp-content/uploads/elementor/thumbs/-خفية-زرقاء-زرقاء-300x300-1-150x150-1-pzrxvvt8yzbtxqgooaqss5nqj503bo45rbcgpz9mq4.png">
                </a>
            </div>
        </div>
    </div> -->
</footer>
<style>
    .footer-links a:hover {
        color: var(--r-secondary);
    }

    .footer-links div {
        display: flex;
        justify-content: center;
        gap: 20px;
        align-items: center;
    }

    .footer-links span {
        font-size: 18px;
        font-weight: 700;
    }

    .footer-links a {
        color: white;
        font-size: 18px;
        font-weight: 600;
        white-space: nowrap;
    }

    .footer-links {
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 20px;
        width: 100%;
        flex-wrap: wrap;

    }

    .contact-btns {
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 16px;
    }

    .made-by {
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 16px
    }

    .made-by p {
        font-weight: 700;
        font-size: 16px;
        line-height: 150%;
        color: #FFFFFF;
    }

    .rouad-logo {
        width: 67.5px;
        height: 21.83px;
    }

    .fotter-header {
        margin-bottom: 8px;
        font-weight: 700;
        font-size: 32px;
        line-height: 190%;
        color: #FFFFFF;
    }

    .fotter-btn {
        margin-top: 48px;
        margin-bottom: 96px;
    }

    .footer-group {
        display: flex;
        justify-content: center;
        gap: 32px !important;
        flex-direction: column;
    }

    @media screen and (min-width: 1920px) {
        .footer-group {
            flex-direction: row;
            justify-content: space-between;
        }
    }

    .footer-img {
        height: 3%
    }

    .logo {
        margin-top: 120px;
        margin-bottom: 48px;
        width: 185px !important;
        height: 185.69px !important;

    }

    .fotter-paragraph-font {
        font-weight: 400;
        font-size: 14px;
        line-height: 190%;
        color: #FFFFFF;
    }


    .paragraph-font {
        font-weight: 700;
        font-size: 16px;
        line-height: 150%;
        color: #FFFFFF;
        margin: auto;
    }


    .rectangle {
        width: 18.12px;
        height: 18.12px;
        inset-inline-end: 27% !important;
        inset-block-end: 88% !important;
    }

    .cercle-top {
        width: 28px;
        height: 28px;
        inset-inline-end: 20% !important;
        inset-block-start: 36% !important;
    }

    .triangle {
        width: 25px;
        height: 25px;
        inset-inline-START: 11% !important;
        inset-block-start: 38% !important;
    }

    .cercle-bottom {
        width: 17px;
        height: 17px;
        inset-inline-start: 24% !important;
        inset-block-end: 84% !important;
    }

    .footer-paragraph-font {
        font-weight: 400;
        font-size: 16px;
        line-height: 150%;
        color: #FFFFFF;
        margin: auto;
    }

    .footer {
        border-top: 16px solid #FF8A00;
        background-color: #1152F2;
        color: white;
        padding: 1rem;
        position: relative;
    }

    @media screen and (min-width: 1440px) {
        .rectangle {
            width: 27.48px;
            height: 27.48px;

        }

        .cercle-top {
            width: 28px !important;
            height: 28px !important;

        }

        .triangle {
            width: 38px;
            height: 38px;

        }

        .cercle-bottom {
            width: 34px;
            height: 34px;
        }

        .footer-paragraph-font {
            font-size: 24px;
            font-weight: 400;
            width: 516px;

        }

        .footer-group {
            font-size: 16px;
            white-space: nowrap;
            font-weight: 400px;
        }

        .footer {
            padding-bottom: 128px;
        }
    }


    .footer-img {
        position: absolute;
    }

    .rectangle {
        inset-inline-end: 29%;
        inset-block-end: 47%;
    }

    .cercle-top {
        inset-inline-end: 13%;
        inset-block-start: 20%;
    }

    .triangle {
        inset-inline-START: 27%;
        inset-block-start: 14%;
    }

    .cercle-bottom {
        inset-inline-start: 14%;
        inset-block-end: 38%;
    }

    .contact-btn {
        width: 48px;
        height: 48px;
        padding: 0px;
        border: 1px solid #FFFFFF;
        border-radius: 100%;
    }

    .contact-btn img {
        width: auto;
        height: auto;
    }

    .rouad-logo {
        height: 1.5rem;
        margin-right: 1rem;
    }


    /* .rouad-footer-logo a {
                color: #ffffff !important;
            }

            .rouad-footer-logo a :hover {
                color: #ffffff !important;
            }


            @media only screen and (max-width: 929px) {
                .first-footer-section {
                    flex-direction: column;
                    padding: 0px 30px !important;

                }

                .flex-section {
                    width: 100% !important;
                }

                .second-footer-section {
                    flex-direction: column;
                    align-items: center;

                }

                .rouad-adress {
                    text-align: center;
                    margin-bottom: 15px;
                }
            }

            .rouad-footer-logo img {
                width: 18px;

            }

            .footer-right {
                display: flex;
                flex-direction: column;
                justify-content: space-around;
                align-items: flex-start;
                width: 50%;

            }


            .second-footer-section {
                padding-top: 20px;
                color: #ffffff;
                font-size: 14px;
            }

            .first-footer-section,
            .second-footer-section {
                display: flex;
                width: 100%;
                max-width: 1190px;
                margin: auto;
                padding-bottom: 20px;
                justify-content: space-around;
            }

            .second-footer-section-color {
                background-color: #1152f2;
            }

            .footer-mid-right {
                width: 50%;
            }

            .flex-section {
                display: flex;
                width: 50%;
            }

            .menu-element {
                margin-top: -7px;
                color: #000000;
            }

            .menu-item {
                display: flex;
                color: #1152f2;
                margin: 17px 5px 5px 5px;
            }

            .menu-item i {
                text-align: center;
                justify-content: center;
                width: 35px;
                display: flex;
                align-items: center;
            }

            .footer-title-rouad {
                font-size: 18px;
                font-weight: 700;
                margin: 10px 20px 25px 20px;
            }

            .footer-section {
                font-size: 14px;
                width: 100%;
                padding: 30px 0px 0px 0px;
            }

            .footer-social-icon {
                display: flex;
                padding: 5px;
                justify-content: flex-start;

            }

            .footer-social-icon a {
                margin: 5px;
            }

            .social-box {
                width: max-content;
            }

            .social-box1 {
                background-color: #ffffff;
                color: #1152f2;
                cursor: pointer;
                padding: 10px 10px 9px 10px;
                border-radius: 5px;
                border: 1px solid #1152f2;
            }

            .social-box :hover {

                background-color: #1152f2 !important;
                color: #ffffff !important;

            }

            .footer-section {
                background-color: #F1F6FF;

            }

            .footer-logo-img {
                width: 131px;
            }

            .footer-title {
                font-weight: 500;
                font-size: 14px;
                color: #000000;
                max-width: 278px;
            } */
</style>

</body>

</html>