<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
if ($_POST["contact-name"]) {
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $email = $user->user_email;
    } else {
        $email = $_POST["email"];
    }

    wp_mail($email, "تواصل معنا ", "نشكر عميلنا العزيز على طلب التواصل سيتم الرد خلال 24 ساعة على استفسارك التالي \n
     " . $_POST["contact-text"]);
    wp_mail("ahlan@rouad.com", "تواصل معنا ", "نشكر عميلنا العزيز على طلب التواصل سيتم الرد خلال 24 ساعة على استفسارك التالي \n
     " . $_POST["contact-text"]);
    $bool = 1;
}
get_header(); ?>
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/contact-us.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/contact-us-min-1440px.css" rel="stylesheet"
    media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/contact-us-min-1920px.css" rel="stylesheet"
    media="screen and (min-width:1920px)">


<div style="width:100%">

    <section class="contact-us-page container">
        <form method="post">
            <?php
            if ($bool)
                echo "<p class='contact-us-form-message'>  تم ارسال الطلب بنجاح سيتم التواصل معك في غضون 24 ساعة</p>";
            ?>
            <h2>تواصل معنا</h2>
            <p>هل لديك استفسار؟ لا تتردد وقم بمراسلتنا</p>
            <article>
                <img src="<?= site_url() ?>/wp-content/uploads/2023/contact-us/contact-us.svg" alt="">
                <div class="contact-us-form">
                    <div><label for="contact-name">اسمك</label>
                        <input name="contact-name" id="contact-name" placeholder="اكتب اسمك هنا" type="text" required>
                    </div>
                    <div><label for="contact-email">بريدك الإلكتروني</label>
                        <input name="email" id="contact-email" placeholder="اكتب بريدك الالكتروني" type="email"
                            required>
                    </div>
                    <div><label for="contact-text">نص الرسالة</label>
                        <textarea name="contact-text" placeholder="مثال: أرغب بشراء متجر إلكتروني لديكم..."
                            id="contact-text" required></textarea>
                    </div>
                    <button class="btn btn-primary">إرسال الرسالة</button>
                </div>
            </article>
        </form>

        <article class="contact-us-map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3610.472299020862!2d55.281012999999994!3d25.1872902!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f69a195816f01%3A0xff8dbe81553a7591!2zUm91YWQgUGxhdGZvcm0g2YXZhti12Kkg2LHZiNmR2KfYrw!5e0!3m2!1sen!2s!4v1680558496500!5m2!1sen!2s"
                width="800" height="600" style="border:0;" allowfullscreen="" loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"></iframe>
            <!-- <img src="<?= site_url() ?>/wp-content/uploads/2023/contact-us/map.svg" alt=""> -->
            <a href="https://maps.app.goo.gl/wAdsPz9xkVvu7Mhp6" target="_blank"><button
                    class="btn btn-outline-primary">مشاهدة على
                    الخريطة</button></a>
        </article>
    </section>
</div>
<style>

</style>
<?php get_footer(); ?>