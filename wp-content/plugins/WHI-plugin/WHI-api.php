<?php
header('Content-Type: application/json');

use Sso\WP;
use Sso\WP\Helpers\SettingHelper;
use Sso\WP\Helpers\AuthenticatHelper;
use Sso\WP\Helpers\UserHelper;
use Sso\WP\Helpers\FunctionApiHelper;
use Sso\WP\View\Renderer;

function reciveDataSso()
{
    register_rest_route('WHI-plugin/v1', '/data', array(
            'methods' => 'GET',
            'callback' => 'getDataMajaraa'
        )
    );
}

function getDataMajaraa()
{
    return rest_ensure_response("hello baby");
}

add_action('rest_api_init', 'reciveDataSso');
