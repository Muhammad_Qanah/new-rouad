<?php

/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_lost_password_form');
?>
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/common.css" rel="stylesheet">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles.css" rel="stylesheet">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1440px.css" rel="stylesheet"
          media="(min-width:1440px)">
    <link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1920px.css" rel="stylesheet"
          media="screen and (min-width:1920px)">


    <div class="created-store-container">
        <div class="creating-store-col loading-store-show">
            <div id="animation-container"></div>

            <div class="store-created loading-site-title">

                <div class="store-created-title loading-title-store ">
                جاري تجهيز موقعك يرجى الإنتظار
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(() => {
            if (window.location.pathname.includes('my-account')) {
                $('footer').remove()
                $('.road-header').remove()
            }
        })

        function s() {
            var animation = bodymovin.loadAnimation({

                container: document.getElementById('animation-container'),

                path: '<?= site_url() ?>/wp-content/uploads/2023/landing-page/logo.json',

                renderer: 'svg',

                loop: true,

                autoplay: true,

                name: "Rouad Animation",

            });
        }
    </script>
    <style>
        .creating-store-container {
            display: none;
        }
    </style>
    <script onload="s()" src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.10.2/lottie.min.js"
            integrity="sha512-fTTVSuY9tLP+l/6c6vWz7uAQqd1rq3Q/GyKBN2jOZvJSLC5RjggSdboIFL1ox09/Ezx/AKwcv/xnDeYN9+iDDA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<?php
do_action('woocommerce_after_lost_password_form');