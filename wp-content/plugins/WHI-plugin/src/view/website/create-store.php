<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_lost_password_form');
$user=wp_get_current_user();

?>

<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles.css" rel="stylesheet">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1440px.css" rel="stylesheet"
      media="(min-width:1440px)">
<link href="<?= site_url() ?>/wp-content/themes/astra/styles/auth-styles-min-1920px.css" rel="stylesheet"
      media="screen and (min-width:1920px)">
<div class="auth-container">


    <!-- TODO: Register Form -->


    <div class="register-form-container">

        <?php include "loading-store.php"; ?>
        <?php include "creating-store.php"; ?>
        <div class="init-wall">

            <div class="email-alert">
                <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/tick-circle.svg">
                <div>
                    تم التحقق من بريدك الإلكتروني
                </div>
            </div>

            <h2 class="welcome-message"><?php esc_html_e('ما هو اسم متجرك ؟', 'woocommerce'); ?></h2>
            <h5 class="welcome-subtitle">سيظهر الاسم لزبائنك كما ستقوم بكتابته، يمكنك تعديله في وقت لاحق</h5>


            <div class="rouad-form">

                <!-- <p><?php echo apply_filters('woocommerce_reset_password_message', esc_html__('Enter a new password below.', 'woocommerce')); ?> -->
                </p><?php // @codingStandardsIgnoreLine ?>

                <p class="auth-form-input">
                    <label for="store-name"><?php esc_html_e('  اسم المتجر باللغة الإنكليزية', 'woocommerce'); ?>
                        &nbsp;<span
                                class="required">*</span></label>
                    <input type="text" required name="store-name" id="store-name" autocomplete="new-password"/>
                </p>
                <p class="auth-form-input">
                    <label for="store-name-en"><?php esc_html_e('اسم المتجر ', 'woocommerce'); ?></label>
                    <input type="text" name="store-name-en" id="store-name-en" autocomplete="new-password"/>
                </p>
                <!--
            <input type="hidden" name="reset_key" value="<?php echo esc_attr($args['key']); ?>" />
            <input type="hidden" name="reset_login" value="<?php echo esc_attr($args['login']); ?>" />

            <div class="clear"></div> -->


                <p class="woocommerce-form-row form-row">
                    <button type="submit" onclick="upload()"
                            class="btn btn-primary auth-btn start-store-btn<?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>"
                            value="<?php esc_attr_e('إبدأ بإعداد متجرك', 'woocommerce'); ?>"><?php esc_html_e('إبدأ بإعداد متجرك', 'woocommerce'); ?></button>
                </p>

            </div>

        </div>
    </div>
    <!-- TODO: Side Image -->

    <div class="auth-side-img">
        <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/Man.svg">
    </div>


</div>
<script>
    $(document).ready(() => {
        if (window.location.pathname.includes('my-account')) {
            $('footer').remove()
            $('.road-header').remove()
        }
    })
</script>
<?php
do_action('woocommerce_after_lost_password_form');

?>
<script>
    jQuery(window).load(function () {
        setTimeout(
            function () {

                jQuery.ajax({
                    type: "post",
                    url: "../wp-admin/admin-ajax.php",
                    data: {
                        action: 'installProjectFile',
                    },
                    success: function (html) {
                        jQuery.ajax({
                            type: "post",
                            url: "../wp-admin/admin-ajax.php",
                            data: {
                                action: 'createProjectData',
                            },
                            success: function (html) {
                                console.log(html)
                            }
                        });
                    }
                });

            }, 1000);

    });

    function save_details() {
        jQuery(".body").hide();

    }

    jQuery(".finish-progres").hide();
    // jQuery(".submit-button-step-1").hide();
    var i = 0;

    function progres() {
        if (i == 0) {
            i = 1;
            var elem = document.getElementById("myBar");
            var width = 1;
            var id = setInterval(frame, 200);

            function frame() {
                if (width >= 98) {
                    clearInterval(id);
                    i = 0;
                } else {

                    width++;
                    elem.style.width = width + "%";
                }
            }
        }
    }


    function upload() {
        // code here
        siteName = jQuery("#store-name-en").val()
        TrialDomain = jQuery("#store-name").val();

        //  description = jQuery("#description").val();
        description = jQuery("#siteName").val();
        TrialDomain = TrialDomain.trim();
        TrialDomain = TrialDomain.replaceAll(" ", "-");
        TrialDomain = TrialDomain.replaceAll("_", "-");
        id = jQuery("#id").val();
        console.log(TrialDomain)
        var letters = /[a-zA-Z][a-zA-Z ]+[a-zA-Z]$/;
        if (jQuery("#store-name-en").val() == "") {
            alert("الرجاء ادخال اسم المتجر في الحقل الثاني ");
            return false;
        }
        // var url = $("#<%= txtUrl.ClientID %>").val();
        // var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
        //
        // args.IsValid = pattern.test(url);

        if (TrialDomain.match(letters)) {

            //  alert("الرجاء ادخال اسم امتجر باللغة العرية");
            TrialDomain = TrialDomain.trim();
            //  alert(TrialDomain);

            // return false;


            jQuery(".loading-store-show").show();

            jQuery(".loading-title-store").show();

            jQuery(".init-wall").hide();

            jQuery.ajax({
                type: "post",
                url: "../wp-admin/admin-ajax.php",
                data: {
                    action: 'installTrial',
                    TrialDomain: TrialDomain,
                    siteName: siteName,
                    description: description,
                    id: id
                },
                success: function (html) {
                    const myTimeout = setTimeout(function () {


                        jQuery(".loading-title-store").hide();
                        jQuery(".finish-setup-site").show();

                        jQuery(".TrialSiteUrl").attr("action", "https://" + html)
                        jQuery(".TrialSiteUrl").submit();
                    }, 60000);


                }
            });


        } else {
            alert("الرجاء ادخال اسم المتجر باللغة لانكليزية");
            return false;
        }

    }
    ;

</script>
<style>
    .loading-title-store, .loading-store-show, .finish-setup-site {
        display: none;
    }


</style>
