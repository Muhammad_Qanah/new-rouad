<div class="created-store-container">
    <div class="creating-store-col  finish-setup-site">


        <div class="store-created">

            <img src="<?= site_url() ?>/wp-content/uploads/2023/auth-pages/tick-circle.svg">
            <div class="store-created-title">
                تم إعداد موقعك بنجاح
            </div>
        </div>
        <form method="post"   -class="TrialSiteUrl" >
            <button class="btn btn-primary auth-btn go-to-site">الذهاب إلى الموقع</button>

        </form>
    </div>
</div>
<script>
    $(document).ready(() => {
        if (window.location.pathname.includes('my-account')) {
            $('footer').remove()
            $('.road-header').remove()
        }
    })

    function s() {
        var animation = bodymovin.loadAnimation({

            container: document.getElementById('animation-container'),

            path: '<?= site_url() ?>/wp-content/uploads/2023/landing-page/logo.json',

            renderer: 'svg',

            loop: true,

            autoplay: true,

            name: "Rouad Animation",

        });
    }
</script>
<style>
    .creating-store-container {
        display: none;
    }
</style>
<?php
do_action('woocommerce_after_lost_password_form');