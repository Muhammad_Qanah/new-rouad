<?php
$widget = json_decode(get_option($widgetName));
?>
<div class="container">
    <form action="<?= esc_url($_SERVER['REQUEST_URI']) ?>" method="Post">
        <h4><?= __('Edit', 'sso') ?> :  <span class="widget_title"><?= $_GET["name"] ?></span>
            <button type="submit" name="edit_widget" class="btn btn-default edit_widget"><?= __('Submit', 'sso') ?> </button>
        </h4>
        <div class="form-group col-md-4">
            <label for=""><?= __('background', 'sso') ?> :</label>
            <input type="color" value="<?= $widget->background ?>" name="background" class="form-control" placeholder=""
                   name="">
        </div>
        <div class="form-group col-md-4">
            <label for=""><?= __('font size', 'sso') ?> :</label>
            <input type="text" value="<?= $widget->fontsize ?>" name="fontsize" class="form-control" placeholder=""
                   name="">
        </div>
        <div class="form-group col-md-4">
            <label for=""><?= __('font color', 'sso') ?> :</label>
            <input type="color" value="<?= $widget->fontcolor ?>" name="fontcolor" class="form-control" placeholder=""
                   name="">
        </div>
        <div class="form-group col-md-6">
            <label for=""><?= __('style', 'sso') ?> :</label>
            <textarea name="style" rows="5" class="form-control"><?= $widget->style ?></textarea>
        </div>
    </form>
</div>
