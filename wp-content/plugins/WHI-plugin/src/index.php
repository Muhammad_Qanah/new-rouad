<?php

namespace Sso\WP;

use Sso\WP\Helpers\AuthenticatHelper;
use Sso\WP\Helpers\CrudApiHelper;
use Sso\WP\Helpers\FunctionApiHelper;
use Sso\WP\Helpers\SettingHelper;
use Sso\WP\Helpers\UserHelper;
use Sso\WP\View\Renderer;

class SsoPlugin
{
    private static $instance;
    private $renderer;

    private function __construct()
    {
        ;
        $this->renderer = Renderer::getInstance();
        $this->settings = SettingHelper::getInstance();
        $this->auth = AuthenticatHelper::getInstance();
        $this->user = UserHelper::getInstance();
        $this->api = FunctionApiHelper::getInstance();
        $this->crud = CrudApiHelper::getInstance();
        $this->addAuthenticatedActions();

        $this->addAlwaysActions();
        // check PurchKey
        if ($this->settings->checkPurchKey()) {
            $this->addAuthenticatedActions();
        } else {
            $this->addUnAuthenticatedActions();
        }
    }

    private function addAlwaysActions()
    {
        add_action('admin_menu', [$this->renderer, 'createAdminMenu']);
        //   add_action('admin_enqueue_scripts', [$this->renderer, 'enqueueAdminScripts']);
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new SsoPlugin();
        }
        return self::$instance;
    }

    public function websiteViews()
    {
        if (!$this->api->checkAuth()) {
            $this->addwebsiteViewsUnAuth();
        } else {
            $this->addwebsiteViewsAuth();
        }
    }

    public function addwebsiteViewsUnAuth()
    {
        add_shortcode('signin-view', [$this->renderer, 'setSigninPage']);

        add_shortcode('add-domain-view', [$this->renderer, 'AddDomainWidget']);

        add_shortcode('add-domain-accept-view', [$this->renderer, 'AddDomainAcceptWidget']);
        add_shortcode('Tria-lHost-Widget-view', [$this->renderer, 'TrialHostWidget']);
    }


    public function addwebsiteViewsAuth()
    {
        add_shortcode('profile-views', [$this->renderer, 'setProfilePage']);
        add_shortcode('article-lock-views', [$this->renderer, 'setArticleLockPage']);
    }

    private function addAuthenticatedActions()
    {
        add_action('widgets_init', [$this, 'websiteViews']);
        add_action('set_user_session', [$this->auth, 'setUserSession'],);
        add_action('check_user_connect', [$this->api, 'checkAuthSso']);
        //if ($this->api->checkAuth()) {
        // filter function
        add_action('check_user_updated', [$this->api, 'checkUserInfoSso'], 10);
        add_action('confirm_user_updated', [$this->api, 'confirmUserInfoSso'], 10);
        add_action('add_offer', [$this->api, 'addOfferSso'], 10, 1);
        add_action('get_product_price', [$this->api, 'getProductPrice'], 10, 1);
        add_action('get_members_info', [$this->api, 'getMembersInfo'], 10);
        add_action('get_members_by_id', [$this->api, 'getMemberById'], 10, 1);
        add_action('check_post_to_show', [$this->api, 'CheckPostToShow'], 10, 1);
        // filter crud
        add_action('get_all_content', [$this->crud, 'getAllContent'], 10);
        add_action('get_content_by_id', [$this->crud, 'getContentById'], 10, 1);
        add_action('add_bulk_content', [$this->crud, 'addBulkContent'], 10, 1);
        add_action('add_content', [$this->crud, 'addContent'], 10, 1);
        add_action('update_content', [$this->crud, 'updateContent'], 10, 1);
        add_action('delete_content', [$this->crud, 'deleteContent'], 10, 1);
        //    }
    }

    private function addUnAuthenticatedActions()
    {
        add_action('rest_api_init', [$this, 'check_domain_api']);
        add_action('rest_api_init', [$this, 'book_domain_api']);
        add_action('rest_api_init', [$this, 'active_rouad_shipping']);
        add_action('rest_api_init', [$this, 'download_plugin']);
        add_action('rest_api_init', [$this, 'get_available_plugin']);

        add_action('rest_api_init', [$this, 'MoveDataApi']);

        add_action('widgets_init', [$this, 'websiteViews']);
        add_action('woocommerce_thankyou', [$this->crud, 'AddSubscriptionToUser'], 10, 1);

        add_action('woocommerce_account_dashboard', [$this, 'widgetdaddDomain'], 10, 1);
        add_action('woocommerce_account_dashboard', [$this, 'notify'], 10, 1);
        add_action('wp_ajax_checkDomain', [$this->api, 'CheckDomain'], 10, 1);
        add_action('wp_ajax_nopriv_checkDomain', [$this->api, 'CheckDomain']);
        add_action('wp_ajax_CreateDomain', [$this->api, 'CreateDomain'], 10, 1);
        add_action('wp_ajax_nopriv_CreateDomain', [$this->api, 'CreateDomain']);
        add_action('user_register', [$this->crud, 'send_notify_register']);


        add_action('wp_ajax_AddDomain', [$this->api, 'AddDomain'], 10, 1);
        add_action('wp_ajax_nopriv_AddDomain', [$this->api, 'AddDomain']);

        add_action('wp_ajax_my_backend_action', 'my_backend_action');
        add_action('admin_init', [$this->settings, 'handleSubmit']);

        add_action('wp_ajax_installProject', [$this->crud, 'installProject'], 10, 1);
        add_action('wp_ajax_nopriv_installProject', [$this->crud, 'installProject']);

        add_action('wp_ajax_installProjectFile', [$this->crud, 'installProjectFile'], 10, 1);
        add_action('wp_ajax_nopriv_installProjectFile', [$this->crud, 'installProjectFile']);
        add_action('wp_ajax_installProjectFrontFile', [$this->crud, 'installProjectFrontFile'], 10, 1);
        add_action('wp_ajax_nopriv_installProjectFrontFile', [$this->crud, 'installProjectFrontFile']);
        add_action('wp_ajax_createProjectData', [$this->crud, 'createProjectData'], 10, 1);
        add_action('wp_ajax_nopriv_createProjectData', [$this->crud, 'createProjectData']);
        add_action('wp_ajax_installTrial', [$this->crud, 'installTrial'], 10, 1);
        add_action('wp_ajax_nopriv_installTrial', [$this->crud, 'installTrial']);
        //  add_action('wp_login', [$this->crud, 'CreeateUserTrial']);
        add_action('wp_ajax_createData', [$this->crud, 'createData'], 10, 1);
        add_action('wp_ajax_createData', [$this->crud, 'createData']);

        add_action('wp_ajax_setup_1', [$this->crud, 'setup_1'], 10, 1);
        add_action('wp_ajax_nopriv_setup_1', [$this->crud, 'setup_1']);
        add_action('wp_ajax_setup_2', [$this->crud, 'setup_2'], 10, 1);
        add_action('wp_ajax_nopriv_setup_2', [$this->crud, 'setup_2']);
        add_action('wp_ajax_setup_3', [$this->crud, 'setup_3'], 10, 1);
        add_action('wp_ajax_nopriv_setup_3', [$this->crud, 'setup_3']);
        add_action('wp_ajax_setup_4', [$this->crud, 'setup_4'], 10, 1);
        add_action('wp_ajax_nopriv_setup_4', [$this->crud, 'setup_4']);
        add_action('wp_ajax_setup_5', [$this->crud, 'setup_5'], 10, 1);
        add_action('wp_ajax_nopriv_setup_5', [$this->crud, 'setup_5']);
        add_action('wp_ajax_setup_6', [$this->crud, 'setup_6'], 10, 1);
        add_action('wp_ajax_nopriv_setup_6', [$this->crud, 'setup_6']);
        add_action('wp_ajax_setup_7', [$this->crud, 'setup_7'], 10, 1);
        add_action('wp_ajax_nopriv_setup_7', [$this->crud, 'setup_7']);
        add_action('wp_ajax_setup_8', [$this->crud, 'setup_8'], 10, 1);
        add_action('wp_ajax_nopriv_setup_8', [$this->crud, 'setup_8']);
    }
    function get_available_plugin($server)
    {
        $server->register_route('wc', '/plugins', array(
            'methods' => 'POST',
            'callback' => function () {
             //   $body = json_decode($body->get_body());
                $result = $this->api->get_available_plugin();
                die($result);
            },
        ));
    }
    function download_plugin($server)
    {
        $server->register_route('wc', '/download', array(
            'methods' => 'POST',
            'callback' => function ($body) {
                   $body = json_decode($body->get_body());
                $result = $this->crud->download_plugin($body);
                   die();
                die($result);
            },
        ));
    }

    function active_rouad_shipping($server)
    {
        $server->register_route('wc', '/active_shipping', array(
            'methods' => 'POST',
            'callback' => function ($body) {
                $body = json_decode($body->get_body());
                $result = $this->api->ActiveShippingApi($body);
                die($result);
            },
        ));
    }

    function check_user_sub($id)
    {
        if (get_user_meta($id, "has_sub")[0] == true || get_user_meta($id, "has_sub")[0] == "1")
            return true;
        else {
            die (json_encode(["message" => "error", "data" => ["status" => 403, "message" => "auth"]]));
        }
    }

    function check_domain_api($server)
    {
        $server->register_route('wc', '/CheckDomain', array(
            'methods' => 'POST',
            'callback' => function ($body) {
                $body = $body->get_body();
                $body = json_decode($body, null, 512, JSON_THROW_ON_ERROR);
                $this->check_user_sub($body->user_id);
                $result = $this->api->CheckDomainApi($body->domains);
                die($result);
            },
        ));
    }

    function book_domain_api($server)
    {
        $server->register_route('wc', '/book', array(
            'methods' => 'POST',
            'callback' => function ($body) {
                $body = json_decode($body->get_body());
                $this->check_user_sub($body->user_id);
                $result = $this->api->BookDomainApi($body);
                die($result);
            },
        ));
    }

    function MoveDataApi($server)
    {
        $server->register_route('wc', '/MoveData', array(
            'methods' => 'POST',
            'callback' => function ($body) {
                $body = json_decode($body->get_body());
                // $this->check_user_sub($body->user_id);

                $result = $this->crud->MoveDataApi($body);
                die($result);
            },
        ));
    }

    function notify()
    {

        $user = wp_get_current_user();
        if ($user->phone) {
            $con = "
         رقم الهاتف : " . $user->phone . "
         ";
        }
        if (!$user->notified) {
            wp_mail("ahlan@rouad.com", "عضو جديد", "
      تهانينا تم تسجيل عميل جديد في منصة روّاد
       \n
        البريد الإلكتروني للمشترك :  " . $user->user_email . "". $con);
            update_user_meta($user->ID, "notified", "true");
            wp_mail("muhammadqanah.1997@gmail.com", "عضو جديد", "
      تهانينا تم تسجيل عميل جديد في منصة روّاد
       \n
        البريد الإلكتروني للمشترك :  
       
        " . $user->user_email . " " . $con);
        }
    }

    function widgetdaddDomain($order_id)

    {
        $user = wp_get_current_user();
        update_user_meta($user->ID, "serv", 0);
        if ($_POST["TrialHost"]) {
            $shortcode = sprintf('[Tria-lHost-Widget-view]');
            echo do_shortcode($shortcode);


        } else if ($_GET['id']) {
            $shortcode = sprintf('[Tria-lHost-Widget-view]');
            echo do_shortcode($shortcode);
        } else {
            $shortcode = sprintf('[Tria-lHost-Widget-view]');
            echo do_shortcode($shortcode);

        }

    }

}

$ssoPlugin = SsoPlugin::getInstance();





