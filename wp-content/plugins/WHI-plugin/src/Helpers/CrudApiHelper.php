<?php


namespace Sso\WP\Helpers;

use WP_User_Query;

class CrudApiHelper extends WhiHelper
{
    protected static $instance;

    private function __construct()
    {
        Parent::__construct();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new CrudApiHelper();
        }
        return self::$instance;
    }

    public function AddSubscriptionToUser()
    {
        $user = wp_get_current_user();
        //update_user_meta($user->ID,"Trialdomain","net");
        $user = wp_get_current_user();
        update_user_meta($user->ID, "has_sub", true);
        if ($user->TrialDomain) {
            return 0;
            echo "<script>
   
const myTimeout = setTimeout(myGreeting, 6000);

function myGreeting() {
           window.location.replace('https://" . $user->TrialDomain . "/wp-admin/admin.php?page=BookDomainPage&&has_sub=1');

}

                </script>";

        }

    }

    function send_notify_register($user_id)
    {
        $user = get_user_by("ID", $user_id,);
        update_user_meta($user->ID, "phone", $_POST["reg_phone"]);

    }

    public function upgrarde($user, $status)
    {
        $server = new InitServer();
        $server->upgrade($user, $status);
    }

    public function AddClientApi($user)
    {
        if (!$user)
            $user = wp_get_current_user();
        $check = array(
            'search' => $user->user_email,
            'clientip' => "00.00.00.00",
            'action' => 'GetClients');
        $result = ($this->callapi($check));
        if ($result['clients']) {
            $clients = $result['clients'];
            foreach ($clients as $client) {
                foreach ($client as $array) {
                    if ($array['email'] == $user->user_email) {
                        update_user_meta($user->ID, "WHI_ID", $array["id"]);
                        return 0;
                    }
                }
            }
        } else {
            $conf = array(
                'firstname' => $user->user_login,
                'lastname' => $user->user_login,
                'email' => $user->user_email,
                'address1' => $user->user_login,
                'city' => $user->user_login,
                'postcode' => "00000",
                'country' => "AE",
                'phonenumber' => "000000000",
                'password2' => "testpassword",
                'clientip' => "00.00.00.00",
                'action' => 'AddClient');
            $user_WHI = $this->callapi($conf);
            update_user_meta($user->ID, "WHI_ID", $user_WHI["clientid"]);
        }
        return $user_WHI;
    }

    public function SyncAll()
    {
        $customer_query = new WP_User_Query(
            array(
                'role' => 'customer',
            )
        );
        $users = $customer_query->get_results();
        foreach ($users as $user) {
            $this->AddClientApi($user);
        }
    }

    public function AddOrderApi($body)
    {
        global $wpdb;
        $user = get_user_by("ID", $body->user_id);
        get_user_meta($user->ID, "WHI_ID");
        $domain = $body->DomainName;
        if (!$user->WHI_ID) {
            ($this->AddClientApi($user));
        }
        $user = get_user_by("ID", $body->user_id);
        $config = array(
            'action' => 'AddOrder',
            'clientid' => 80,
            'pid' => 1,
            'domain' => $domain,
            'addons' => array('1,3,9', ''),
            'customfields' => array(base64_encode(serialize(array("1" => "Google"))), base64_encode(serialize(array("1" => "Google")))),
            'configoptions' => array(base64_encode(serialize(array("1" => 999))), base64_encode(serialize(array("1" => 999)))),
            'nameserver1' => 'ns1.rouad.pro',
            'nameserver2' => 'ns2.rouad.pro',
            'paymentmethod' => 'banktransfer',
            'responsetype' => 'json');
        $result = $this->callApi($config);

        if ($result["result"] == "success") {
            $row_id = $this->AcceptOrderApi($result["orderid"], $body);
        }
        return $row_id;
    }

    function AcceptOrderApi($id, $body)
    {
        $name = explode(".", $body->DomainName);
        $length = 12;
        $keyspace = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz';
        {
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
            }
        }
        $a = $str;
        $config = array(
            'serverid' => 2,
            'orderid' => $id,
            'action' => "AcceptOrder",
            'autosetup' => true,
            'serviceusername' => $str,
            'servicepassword' => "pasPas&UYTIOJH",
            'sendemail' => false,
        );
        global $wpdb;
        $sql = "SELECT id FROM`" . $wpdb->base_prefix . "domains`where `domain_name`='" . $body->DomainName . "' limit 1;";
        $result = $wpdb->get_results($sql);
        $row = $result[0]->id;
        if (!$row) {
            $sql = "INSERT INTO `" . $wpdb->base_prefix . "domains`(`user_id`,`domain_name`,`username`,`password`,`steps` ,`whOrder_id`) VALUES (" . $body->user_id . ",'" . $_POST["domain"] . "','" . $str . "','pasPas&UYTIOJH',1,'" . $id . "');";
            $result = $wpdb->get_results($sql);
        } else {
            $sql = "update `{$wpdb->base_prefix}domains` set `steps`=1 ,`user_id`='" . $body->user_id . "',`username`='" . $str . "',`password`='pasPas&UYTIOJH' ,`whOrder_id`='" . $id . "'   where  `id`= " . $row;
            $info = $wpdb->get_results($sql);;
        }
        $sql = "SELECT id FROM`" . $wpdb->base_prefix . "domains`where `domain_name`='" . $body->DomainName . "' limit 1;";
        $result = $wpdb->get_results($sql);
        $row = $result[0]->id;
        update_user_meta($body->user_id, "serv", 1);
        $result = $this->callApi($config);
        return $row;
    }

    public function AddOrder($id)
    {
        global $wpdb;
        $user = get_user_by("ID", $id);
        get_user_meta($id, "WHI_ID");
        $domain = $_POST["domain"];
        $products = $this->getUserSubsicrption();
        $product = array(6484 => 1, 7572 => 2, 7219 => 3);
        foreach ($products as $key => $value) {
            if ($product[$value])
                $final[] = $product[$value];
        }
        if (!$user->WHI_ID) {
            ($this->AddClientApi($user));
        }
        //  var_dump("dsdsd");
        $config = array(
            'action' => 'AddOrder',
            'clientid' => 80,
            'pid' => 1,
            'domain' => $domain,
            'addons' => array('1,3,9', ''),
            'customfields' => array(base64_encode(serialize(array("1" => "Google"))), base64_encode(serialize(array("1" => "Google")))),
            'configoptions' => array(base64_encode(serialize(array("1" => 999))), base64_encode(serialize(array("1" => 999)))),
            'nameserver1' => 'ns1.rouad.pro',
            'nameserver2' => 'ns2.rouad.pro',
            'paymentmethod' => 'banktransfer',
            'responsetype' => 'json');
        $result = $this->callApi($config);

        if ($result["result"] == "success") {
            $row_id = $this->AcceptOrder($result["orderid"], $id);
        }
        return $row_id;
    }

    function AcceptOrder($id, $userid)
    {
        $name = explode(".", $_POST["domain"]);
        $length = 12;
        $keyspace = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz';
        {
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
            }
        }
        echo $a = $str;
        $config = array(
            'serverid' => 2,
            'orderid' => $id,
            'action' => "AcceptOrder",
            'autosetup' => true,
            'serviceusername' => $str,
            'servicepassword' => "pasPas&UYTIOJH",
            'sendemail' => false,
        );
        global $wpdb;
        $sql = "SELECT id FROM`" . $wpdb->base_prefix . "domains`where `domain_name`='" . $_POST["domain"] . "' limit 1;";
        $result = $wpdb->get_results($sql);
        $row = $result[0]->id;
        if (!$row) {
            $sql = "INSERT INTO `" . $wpdb->base_prefix . "domains`(`user_id`,`domain_name`,`username`,`password`,`steps` ,`whOrder_id`) VALUES (" . $userid . ",'" . $_POST["domain"] . "','" . $str . "','pasPas&UYTIOJH',1,'" . $id . "');";
            $result = $wpdb->get_results($sql);
        } else {
            $sql = "update `{$wpdb->base_prefix}domains` set `steps`=1 ,`user_id`='" . $userid . "',`username`='" . $str . "',`password`='pasPas&UYTIOJH' ,`whOrder_id`='" . $id . "'   where  `id`= " . $row;
            $info = $wpdb->get_results($sql);;
        }
        $sql = "SELECT id FROM`" . $wpdb->base_prefix . "domains`where `domain_name`='" . $_POST["domain"] . "' limit 1;";
        $result = $wpdb->get_results($sql);
        $row = $result[0]->id;
        update_user_meta($userid, "serv", 1);
        $result = $this->callApi($config);
        return $row;
    }

    function GetProducts()
    {

        $config = array(
            'action' => "GetProducts",
        );
    }

    function getUserSubsicrption()
    {
        global $woocommerce, $subscription;
        $user = wp_get_current_user();

        if (is_a($subscription, 'WC_Subscription') && $user->ID === $subscription->get_user_id()) {
            $allcaps['edit_shop_subscription_status'] = true;
        }
        $users_subscriptions = wcs_get_users_subscriptions($user->ID);
        foreach ($users_subscriptions as $subscription) {
            if ($subscription->has_status(array('active'))) {
                $items = ($subscription->order->get_items());
                foreach ($items as $item) {
                    $product_ids[] = $item->get_product_id();
                }
            }
        }
        return $product_ids;
    }

    public function installProject()
    {
        global $wpdb;


        $server = new InitServer();
        //  $server->createDatabase();

        $is_error = empty($wpdb->last_error);
        // var_dump($result);
        die("dfsdfd");
    }

    public function installProjectFile()
    {

        global $wpdb;
        $server = new InitServer();
        $response = $server->createTrialLaravelProjectFile();
        // $is_error = empty($wpdb->last_error);
        // var_dump($result);
        $user = wp_get_current_user();
//echo $user->TrialDomain;
        $response = json_encode($response);
        die($response);
    }
    public function installProjectFrontFile()
    {

        global $wpdb;
        $server = new InitServer();
        $response = $server->createTrialFrontProjectFile();
        // $is_error = empty($wpdb->last_error);
        // var_dump($result);
        $user = wp_get_current_user();
//echo $user->TrialDomain;
        $response = json_encode($response);
        die($response);
    }
    public function createProjectData()
    {
        global $wpdb;
        $server = new InitServer();
        $response = $server->createProjectData();

        $response = json_encode($response);
        die($response);
    }

    public function download_plugin($body)
    {
        $server = new InitServer();
        $server->download_plugin($body);
    }

    public function installTrial()
    {
        global $wpdb;
        $server = new InitServer();
        $server->createTrialDatabase();
        $is_error = empty($wpdb->last_error);
        // var_dump($result);
        $user = wp_get_current_user();
//echo $user->TrialDomain;
        die($user->TrialDomain);
    }

    function CreeateUserTrial()
    {
        $user = wp_get_current_user();
        if (!$user->trial) {
            $server = new InitServer();
            $server->createTrialDatabase();
        }
        die();
    }

    public function createData()
    {
        global $wpdb;
        $server = new InitServer();
        $is_error = empty($wpdb->last_error);
        die("done");
    }

    public function MoveDataApi($body)
    {
        global $wpdb;
        $this->id = $body->user_id;

        $server = new InitServer();
        $server->initDataDomain($body);


        $response = ["message" => "success", "data" => "domain move successfully"];
        $response = json_encode($response);
        die($response);
    }

    function setup_1()
    {
        $server = new InitServer();
        // $server->setup_1();
        die("done");
    }

    function setup_2()
    {
        $server = new InitServer();
        //$server->setup_2();
        die("done");
    }

    function setup_3()
    {
        $server = new InitServer();
        //$server->setup_3();
        die("done");
    }

    function setup_4()
    {
        $server = new InitServer();
        //$server->setup_4();
        die("done");
    }

    function setup_5()
    {
        $server = new InitServer();
        //   $server->setup_5();
        die("done");
    }

    function setup_6()
    {
        $server = new InitServer();
        // $server->setup_6();
        die("done");
    }

    function setup_7()
    {
        $server = new InitServer();
        //$server->setup_7();
        die("done");
    }

    function setup_8()
    {
        $server = new InitServer();
        //$server->setup_8();
        die("done");
    }
}