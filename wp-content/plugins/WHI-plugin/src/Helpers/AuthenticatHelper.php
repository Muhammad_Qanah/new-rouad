<?php


namespace Sso\WP\Helpers;

class AuthenticatHelper
{
    private static $instance;
    private $isAuth;

    private function __construct()
    {
       $this->api = FunctionApiHelper::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new AuthenticatHelper();
        }
        return self::$instance;
    }

    public function CheckAuthenticated()
    {
        return $this->getIsAuth();
    }

    public function getIsAuth()
    {
        return $this->isAuth;
    }

    public function setIsAuth($isAuth)
    {
        $this->isAuth = $isAuth;
    }

    public function setUserSession($arg)
    {
        // Start session on init hook.
        if (!isset($_SESSION['id']) && empty($_SESSION['id'])) {
            $_SESSION['id'] = $arg["id"];
            $_SESSION['email'] = $arg["email"];
            $_SESSION['token'] = $arg["token"];
            return $_SESSION['id'];
        }
        return -1;
    }

}