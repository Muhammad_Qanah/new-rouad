/* zorem_snackbar jquery */
(function( $ ){
	$.fn.zorem_snackbar = function(msg) {
		if ( jQuery('.cev-snackbar-logs').length === 0 ){
			$("body").append("<section class=cev-snackbar-logs></section>");
		}
		var zorem_snackbar = $("<article></article>").addClass('cev-snackbar-log cev-snackbar-log-success cev-snackbar-log-show').text( msg );
		$(".cev-snackbar-logs").append(zorem_snackbar);
		setTimeout(function(){ zorem_snackbar.remove(); }, 3000);
		return this;
	}; 
})( jQuery );

/* zorem_snackbar_warning jquery */
(function( $ ){
	$.fn.zorem_snackbar_warning = function(msg) {
		if ( jQuery('.cev-snackbar-logs').length === 0 ){
			$("body").append("<section class=cev-snackbar-logs></section>");
		}
		var zorem_snackbar_warning = $("<article></article>").addClass( 'cev-snackbar-log cev-snackbar-log-error cev-snackbar-log-show' ).html( msg );
		$(".cev-snackbar-logs").append(zorem_snackbar_warning);
		setTimeout(function(){ zorem_snackbar_warning.remove(); }, 3000);
		return this;
	}; 
})( jQuery );

jQuery(document).on("change", "#cev_enable_email_verification,#cev_enable_email_verification_checkout,#cev_enable_login_authentication", function(){
	
	var accordion = jQuery(this).closest('.accordion');			
	var form = jQuery("#cev_settings_form");	

	jQuery.ajax({
		url: ajaxurl,
		data: form.serialize(),
		type: 'POST',
		dataType:"json",	
		success: function() {	
			jQuery("#cev_settings_form").zorem_snackbar( 'Your Settings have been successfully saved.' );		
		},
		error: function(response) {
			console.log(response);			
		}
	});

	if (jQuery(this).is(':checked')) {
		accordion.find('.accordion-open').removeClass( 'disable_toggle' );		
		toggle_accordion(accordion);
	} else {
		if ( accordion.hasClass( 'active' ) ) {	
			toggle_accordion(accordion);
		}
		accordion.find('.accordion-open').addClass( 'disable_toggle' );		
	}
});

jQuery(document).on("click", ".cev_settings_save", function(){
	
	var form = jQuery("#cev_settings_form");	
	var accordion = jQuery(this).closest('.accordion');
	accordion.find(".spinner").addClass("active");	
	
	jQuery.ajax({
		url: ajaxurl,
		data: form.serialize(),
		type: 'POST',
		dataType:"json",	
		success: function() {	
			form.find(".spinner").removeClass("active");
			jQuery("#cev_settings_form").zorem_snackbar( 'Your Settings have been successfully saved.' );
			jQuery( '.accordion' ).removeClass( 'active' );
			jQuery( '.accordion' ).find( '.cev_settings_save' ).hide();
			jQuery( '.accordion' ).find( 'span.dashicons' ).addClass( 'dashicons-arrow-right-alt2' );
			jQuery( '.panel' ).slideUp( 'slow' );		
		},
		error: function(response) {
			console.log(response);			
		}
	});
	return false;
});

jQuery(document).on("click", ".accordion-label, .accordion-open", function(){
	var accordion = jQuery(this).closest('.accordion');	
	toggle_accordion(accordion);	
});

function toggle_accordion( accordion ){
	if ( accordion.hasClass( 'active' ) ) {				
		accordion.removeClass( 'active' );
		accordion.siblings( '.panel' ).removeClass( 'active' );
		accordion.siblings( '.panel' ).slideUp( 'slow' );
		jQuery( '.accordion' ).find('span.dashicons').addClass('dashicons-arrow-right-alt2');
		jQuery( '.accordion' ).find('.cev_settings_save').hide();		
	} else {		
		jQuery( '.accordion' ).removeClass( 'active' );
		jQuery(".accordion").find('.cev_settings_save').hide();
		jQuery(".accordion").find('span.dashicons').addClass('dashicons-arrow-right-alt2');	
		jQuery( '.panel' ).slideUp('slow');
		accordion.addClass( 'active' );
		accordion.siblings( '.panel' ).addClass( 'active' );
		accordion.find('span.dashicons').removeClass('dashicons-arrow-right-alt2');
		accordion.find('.cev_settings_save').show();				
		accordion.siblings( '.panel' ).slideDown( 'slow', function() {
			var visible = accordion.isInViewport();
			if ( !visible ) {
				jQuery('html, body').animate({
					scrollTop: accordion.prev().offset().top - 35
				}, 1000);	
			}
		} );		
		/**/		
	}
}

(function( $ ){
	$.fn.isInViewport = function( element ) {
		var win = $(window);
		var viewport = {
			top : win.scrollTop()			
		};
		viewport.bottom = viewport.top + win.height();
		
		var bounds = this.offset();		
		bounds.bottom = bounds.top + this.outerHeight();

		if( bounds.top >= 0 && bounds.bottom <= window.innerHeight) {
			return true;
		} else {
			return false;	
		}		
	};
})( jQuery );

/*( function( $, data, wp, ajaxurl ) {
		
	var $cev_settings_form = $("#cev_settings_form");	
			
	var cev_settings_init = {
		
		init: function() {									
			$cev_settings_form.on( 'click', '.cev_settings_save', this.save_wc_cev_settings_form );			
		},

		save_wc_cev_settings_form: function( event ) {
			
			event.preventDefault();
			
			$cev_settings_form.find(".spinner").addClass("active");				
			var ajax_data = $cev_settings_form.serialize();
			
			$.post( ajaxurl, ajax_data, function(response) {
				$cev_settings_form.find(".spinner").removeClass("active");
				jQuery("#cev_settings_form").zorem_snackbar( 'Your Settings have been successfully saved.' );
				jQuery( '.accordion' ).removeClass( 'active' );
				jQuery( '.accordion' ).find( '.cev_settings_save' ).hide();
				jQuery( '.accordion' ).find( 'span.dashicons' ).addClass( 'dashicons-arrow-right-alt2' );
				jQuery( '.panel' ).slideUp( 'slow' );
			});
			return false;
		}, 		
	};
	
	$(window).load(function(e) {
        cev_settings_init.init();
    });	
	
})( jQuery, cev_pro_admin_js, wp, ajaxurl );*/


jQuery( document ).ready(function() {	
	jQuery(".woocommerce-help-tip").tipTip();	
	var cev_verification_checkout_dropdown_option = jQuery('#cev_verification_checkout_dropdown_option').val();
	if ( cev_verification_checkout_dropdown_option  == '2' ) {
		jQuery('#cev_enable_email_verification_cart_page').closest('li').hide();
	}
	console.log(cev_verification_checkout_dropdown_option);
});

jQuery( document ).on( "change", "#cev_verification_checkout_dropdown_option", function() {
	var cev_verification_checkout_dropdown_option = jQuery(this).val();
	if ( cev_verification_checkout_dropdown_option  == '2' ) {
		jQuery('#cev_enable_email_verification_cart_page').closest('li').hide();
	} else{
		jQuery('#cev_enable_email_verification_cart_page').closest('li').show();
	}
});


jQuery(document).on("click", ".cev_tab_input", function(){
	"use strict";
	var tab = jQuery(this).data('tab');
	var url = window.location.protocol + "//" + window.location.host + window.location.pathname+"?page=customer-email-verification-for-woocommerce&tab="+tab;
	window.history.pushState({path:url},'',url);	
});

jQuery( document ).on( "click", "#activity-panel-tab-help", function() {
	jQuery(this).addClass( 'is-active' );
	jQuery( '.woocommerce-layout__activity-panel-wrapper' ).addClass( 'is-open is-switching' );
});

jQuery(document).click(function(event){
	var $trigger = jQuery(".woocommerce-layout__activity-panel");
    if($trigger !== event.target && !$trigger.has(event.target).length){
		jQuery('#activity-panel-tab-help').removeClass( 'is-active' );
		jQuery( '.woocommerce-layout__activity-panel-wrapper' ).removeClass( 'is-open is-switching' );
    }   
});

jQuery(document).on('click', '.cev-email-verify-button-tools', function(e) {
	"use strict";	

	jQuery('#cev_content_tools .cev-email-verify-tools-td .spinner').addClass("active");
	jQuery( '#cev_content_tools .cev-email-verify-tools-td .tools_save_msg').hide();
	
	var ajax_data = {
		action: 'bulk_email_verify_from_tools',			
	};
	
	jQuery.ajax({           
		url : ajaxurl,
		type : 'post',
		data : ajax_data,
		success : function( response ) {
			jQuery('#cev_content_tools .cev-email-verify-tools-td .spinner').removeClass("active");
			jQuery("#cev_content_tools .cev-email-verify-tools-td .tools_save_msg").zorem_snackbar( 'Successfully verified '+response+' emails.' );					
		}
	}); 
	return false;
});	

jQuery(document).on('click', '.cev-email-resend-bulk-button-tools', function(e) {
	"use strict";		
	jQuery('#cev_content_tools .cev-email-resend-tools-td .spinner').addClass("active");
	jQuery( '#cev_content_tools .cev-email-resend-tools-td .tools_save_msg').hide();
	var ajax_data = {
		action: 'bulk_email_resend_verify_from_tools',			
	};
	
	jQuery.ajax({           
		url : ajaxurl,
		type : 'post',
		data : ajax_data,
		success : function( response ) {
				jQuery('#cev_content_tools .cev-email-resend-tools-td .spinner').removeClass("active");
				jQuery("#cev_content_tools .cev-email-resend-tools-td .tools_save_msg").zorem_snackbar( 'Successfully sent a verification email to all unverified users.' );
		}
	}); 
	return false;
});	

jQuery(document).on('click', '#cev_enable_tools,#cev_change_texbox_value', function(e) {	
	"use strict";
	var toggle_val = jQuery('#cev_enable_tools').parent().find('#cev_enable_tools').is(":checked");
	var taxbox_val = jQuery('#cev_change_texbox_value').parent().find('#cev_change_texbox_value').val();
	var ajax_data = {
		action: 'toggle_email_delete_from_tools',	
		toggle: toggle_val,
		wp_nonce : jQuery(this).attr('wp_nonce'),
		textbox: taxbox_val,
	};
	
	jQuery.ajax({           
		url : ajaxurl,
		type : 'post',
		data : ajax_data,
		success : function( response ) {
							
		}
	}); 	
});

jQuery(document).on("click", ".delay_account_check", function(){
	if(jQuery(this).prop("checked") == true){
		jQuery('.delay_account_check').prop('checked', false);
		jQuery(this).prop('checked', true);		
	}
});

jQuery(document).on("change", "#cev_verification_checkout_dropdown_option", function(){	
	if(jQuery(this).val() == '2'){
		jQuery('.cev_cart_values_hide').hide();
	} else{
		jQuery('.cev_cart_values_hide').show();	
	}
});	