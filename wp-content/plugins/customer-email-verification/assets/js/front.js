jQuery(document).on("submit", ".cev_pin_verification_form", function(){
	var form = jQuery(this);
	var error;	
	var cev_pin1 = form.find("#cev_pin1");
	
	if( cev_pin1.val() === '' ){
		jQuery('.required-filed').html('<span class="cev-error-display">4-digits code*</span>');		
		showerror( cev_pin1 );error = true;
	} else{
		hideerror(cev_pin1);
	}
	
	if(error == true){
		return false;
	}
	jQuery(".cev_pin_verification_form ").block({
		message: null,
		overlayCSS: {
			background: "#fff",
			opacity: .6
		}	
    });

	
	jQuery.ajax({
		url: cev_ajax_object.ajax_url,		
		data: form.serialize(),
		type: 'POST',
		dataType: "json",
		success: function(response) {
			if(response.success == 'true'){
				window.location.href = response.url;
			} else{
				jQuery('.cev-pin-verification__failure').show();
			}
			jQuery(".cev_pin_verification_form ").unblock();				
		},
		error: function(jqXHR, exception) {
			console.log(jqXHR.status);						
		}
	});
	return false;
});

jQuery(document).on("submit", ".cev_login_authentication_form", function(){
	var form = jQuery(this);
	var error;	
	var cev_pin1 = form.find("#cev_pin1");
	
	if( cev_pin1.val() === '' ){
		jQuery('.required-filed').html('<span class="cev-error-display">4-digits code*</span>');		
		showerror( cev_pin1 );error = true;
	} else{
		hideerror(cev_pin1);
	}
	
	if(error == true){
		return false;
	}
	jQuery(".cev_login_authentication_form ").block({
		message: null,
		overlayCSS: {
			background: "#fff",
			opacity: .6
		}	
    });
	
	jQuery.ajax({
		url: cev_ajax_object.ajax_url,		
		data: form.serialize(),
		type: 'POST',
		dataType: "json",
		success: function(response) {
			if(response.success == 'true'){
				window.location.href = response.url;
			} else{
				jQuery('.cev-pin-verification__failure').show();
			}
			jQuery(".cev_login_authentication_form ").unblock();				
		},
		error: function(jqXHR, exception) {
			console.log(jqXHR.status);						
		}
	});
	return false;
});

function showerror(element){
	element.css("border-color","red");
}
function hideerror(element){
	element.css("border-color","");
}
function getCodeBoxElement(index) {
  return document.getElementById('cev_pin' + index);
}
function onKeyUpEvent(index, event) {
  const eventCode = event.which || event.keyCode;
  if (getCodeBoxElement(index).value.length === 1) {
	 if (index !== 4) {
		getCodeBoxElement(index+ 1).focus();
	 } else {
		getCodeBoxElement(index).blur();
		// Submit code		
	 }
  }
  if (eventCode === 8 && index !== 1) {
	 getCodeBoxElement(index - 1).focus();
  }
}
function onFocusEvent(index) {
  for (item = 1; item < index; item++) {
	 const currentElement = getCodeBoxElement(item);
	 if (!currentElement.value) {
		  currentElement.focus();
		  break;
	 }
  }
}

/* cev pro */
jQuery(document).on('click', '.cev-send-verification-code', function(e) {
	"use strict";	
	
	var error;	
	var cev_pin_email = jQuery("#cev_pin_email");
	
	if( cev_pin_email.val() === '' ){
		showerror( cev_pin_email );
		error = true;
	} else{
		hideerror(cev_pin_email);
	}
	
	if( !validate_email(cev_pin_email.val()) ){
		showerror( cev_pin_email );
		error = true;
	} else{
		hideerror(cev_pin_email);
	}
	
	if(error == true){
		return false;
	}		
	
	jQuery(".cev-authorization").block({
		message: null,
		overlayCSS: {
			background: "#fff",
			opacity: .6
		}	
    });
	
	var ajax_data = {
		action: 'checkout_page_send_verification_code',
		email: cev_pin_email.val(),	
		wp_nonce : jQuery(this).attr('wp_nonce')
	};
	
	jQuery.ajax({           
		url : cev_ajax_object.ajax_url,
		type : 'post',
		dataType: "json",
		data : ajax_data,
		success : function( response ) {
			if(response.success == 'true'){				
				jQuery('.cev-show-reg-content .cev-authorization__description span').html(cev_pin_email.val());
				jQuery('.cev-pin-verification').show();
				jQuery('.cev-authorization__heading').hide();
				jQuery('.cev-show-reg-content').show();
				jQuery('.cev-authorization__footer').hide();
				jQuery('.cev-rge-content').show();
				jQuery('.cev-hide-success-message').hide();										
			}
			if(response.success == 'false'){
				if( response.cev_resend_limit_reached == 'true'){
					jQuery('.resend_verification_code_guest_user').css("pointer-events", "none");
					jQuery('.cev-send-verification-code').css({"pointer-events":"none",'cursor': 'not-allowed','background-color': '#bdbdbd'});
					jQuery('.cev_limit_message').show();
					
				}
			}
			jQuery(".cev-authorization").unblock();	
		}
	}); 
});

function validate_email(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}


jQuery(document).on('click', '#cev_submit_pin_button', function(e) {
	"use strict";		
	var error;	
	var cev_pin_box_code = jQuery("#cev_pin_box_code");
	var cev_pin_email = jQuery("#cev_pin_email");
	
	if( cev_pin_box_code.val() === '' ){
		showerror( cev_pin_box_code );
		error = true;
	} else{
		hideerror(cev_pin_box_code);
	}
	
	if(error == true){
		return false;
	}
	
	jQuery(".cev_pin_verification_form_pro").block({
		message: null,
		overlayCSS: {
			background: "#fff",
			opacity: .6
		}	
    });
	
	var ajax_data = {
		action: 'checkout_page_verify_code',
		email: cev_pin_email.val(),
		pin: cev_pin_box_code.val(),
		security: cev_ajax_object.checkout_verify_code,	
	};
	
	jQuery.ajax({           
		url : cev_ajax_object.ajax_url,
		type : 'post',
		dataType: "json",
		data : ajax_data,
		success : function( response ) {
			if(response.success == 'true'){
				location.reload();	
			} else{
				jQuery('.cev-pin-verification__failure').show();	
			}
			jQuery(".cev_pin_verification_form_pro").unblock();				
		},
		error: function(jqXHR, exception) {
			console.log(jqXHR.status);							
		}
	}); 
	return false;	
});

jQuery(document).on('click', '.verify_account_email_my_account', function(e) {
	"use strict";	
	
	var error;	
	var my_account_email_verification = jQuery("#my_account_email_verification");	
	
	if( my_account_email_verification.val() === '' ){
		showerror( my_account_email_verification );
		error = true;
		jQuery('.cev-pin-verification__failure_code').hide();		 
	} else{
		hideerror(my_account_email_verification);
		
		jQuery(".woocommerce-MyAccount-content").block({
			message: null,
			overlayCSS: {
				background: "#fff",
				opacity: .6
			}	
		});
		
		var ajax_data = {                                                                                                                
			action: 'verify_email_on_edit_account',		
			pin: my_account_email_verification.val(),
			wp_nonce : jQuery(this).attr('wp_nonce')
		};
		
		jQuery.ajax({           
			url : cev_ajax_object.ajax_url,
			type : 'post',
			dataType: "json",
			data : ajax_data,
			success : function( response ) {
				if(response.success == 'true'){
					location.reload();
				} else{
					jQuery('.cev-pin-verification__failure_code').show().css("color","red");
				}
				jQuery(".woocommerce-MyAccount-content").unblock();				
			},
			error: function(jqXHR, exception) {
				console.log(jqXHR.status);							
			}
		}); 
	}
	
	
	return false;	
});

jQuery(document).on('click', '.resend_verification_code_my_account', function(e) {
	"use strict";	
	
	var error;
	var my_account_email_verification = jQuery("#my_account_email_verification");	
	
	
	jQuery(".woocommerce-MyAccount-content").block({
		message: null,
		overlayCSS: {
			background: "#fff",
			opacity: .6
		}	
    });
	
	var ajax_data = {
		action: 'resend_verification_code_on_edit_account',	
				
	};
		
	jQuery.ajax({           
		url : cev_ajax_object.ajax_url,
		type : 'post',
		dataType: "json",
		data : ajax_data,
		success : function( response ) {
			if(response.success == 'true'){
				if( response.cev_resend_limit_reached == 'true'){
					jQuery('.a_cev_resend').css({"pointer-events":"none",'cursor': 'not-allowed','opacity': '0.5'});
				}
			} else{
				jQuery('.show_cev_resend_yes').show();
			}
			jQuery(".woocommerce-MyAccount-content").unblock();			
		},
		
	}); 
	return false;	
});
jQuery(document).on("click", ".cev_already_verify", function(){	
	"use strict";
	jQuery('.cev-pin-verification').show();
	jQuery('.cev-authorization__heading').hide();
	jQuery('.cev-show-reg-content').show();
	jQuery('.cev-authorization__footer').hide();
	jQuery('.cev-rge-content').show();
	jQuery('.cev-hide-success-message').hide();	
});

function showerror(element){
	element.css("border-color","red");
	
}
function hideerror(element){
	element.css("border-color","");
}


jQuery(document).on('click', '.resend_verification_code_guest_user', function(e) {
	"use strict";	
	
	var cev_pin_email = jQuery("#cev_pin_email");	
	
	jQuery(".resend_verification_code_guest_user").block({
		message: null,
		overlayCSS: {
			background: "#fff",
			opacity: .6
		}	
    });
	
	var ajax_data = {
		action: 'resend_verification_code_guest_user',
		email: cev_pin_email.val(),
		wp_nonce : jQuery(this).data('nonce'),		
	};
	
	
	jQuery.ajax({           
		url : cev_ajax_object.ajax_url,
		type : 'post',
		dataType: "json",
		data : ajax_data,
		success : function( response ) {
			if(response.success === 'true'){
				if( response.cev_resend_limit_reached ===  'true'){
					jQuery('.resend_verification_code_guest_user').css("pointer-events", "none");
				}
			} else{
				jQuery('.show_cev_resend_yes').show();
			}
			jQuery(".resend_verification_code_guest_user").unblock();				
		},
		
	}); 
	return false;	
});