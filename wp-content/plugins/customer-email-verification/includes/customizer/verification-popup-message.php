<?php
/**
 * Customizer Setup and Custom Controls
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
class Cev_Verification_Popup_Message {
	// Get our default values	
	private static $order_ids  = null;
	
	public function __construct() {
		// Get our Customizer defaults
		$this->defaults = $this->cev_generate_defaults();		
		
		// Register our sample default controls
		add_action( 'customize_register', array( $this, 'cev_my_verification_popup_message' ) );
		
		// Only proceed if this is own request.				
		if ( ! self::is_own_customizer_request() && ! self::is_own_preview_request()) {
			return;
		}			
		
		// Register our sections
		add_action( 'customize_register', array( wc_cev_customizer(), 'cev_add_customizer_sections' ) );	
		
		// Remove unrelated components.
		add_filter( 'customize_loaded_components', array( wc_cev_customizer(), 'remove_unrelated_components' ), 99, 2 );
		
		// Remove unrelated sections.
		add_filter( 'customize_section_active', array( wc_cev_customizer(), 'remove_unrelated_sections' ), 10, 2 );	
		
		// Unhook divi front end.
		add_action( 'woomail_footer', array( wc_cev_customizer(), 'unhook_divi' ), 10 );
		
		// Unhook Flatsome js
		add_action( 'customize_preview_init', array( wc_cev_customizer(), 'unhook_flatsome' ), 50  );	
		

		add_filter( 'customize_controls_enqueue_scripts', array( wc_cev_customizer(), 'enqueue_customizer_scripts' ) );	
		
		//add_action( 'parse_request', array( $this, 'set_up_preview' ) );	

		add_action( 'customize_preview_init', array( $this, 'enqueue_preview_scripts' ) );					
	}			
		
	/**
	* Add css and js for preview
	*/	
	public function enqueue_preview_scripts() {		 
		 wp_enqueue_style('cev-pro-preview-styles', cev_pro()->plugin_dir_url() . 'assets/css/preview-styles.css', array(), cev_pro()->version  );		 
	}	
	
	/**
	 * Checks to see if we are opening our custom customizer preview
	 *
	 * @return bool
	 */
	public static function is_own_preview_request() {
		return isset( $_REQUEST['action'] ) && ( 'preview_cev_verification_lightbox' === $_REQUEST['action'] || 'guest_user_preview_cev_verification_lightbox' === $_REQUEST['action'] );
	}
	
	/**
	 * Checks to see if we are opening our custom customizer controls
	 *
	 * @return bool
	 */
	public static function is_own_customizer_request() {
		return isset( $_REQUEST['section'] ) && 'cev_verificaion_popup_message' === $_REQUEST['section'];
	}
	
	/**
	 * Get Customizer URL
	 *
	 */
	public static function get_customizer_url( $section ) {	
		$customizer_url = add_query_arg( array(
				'cev-customizer' => '1',
				'section' => $section,						
				'autofocus[section]' => 'cev_verificaion_popup_message',
				'url'                  => urlencode( add_query_arg( array( 'action' => 'preview_cev_verification_lightbox' ), home_url( '/' ) ) ),
				'return'               => urlencode( self::get_cev_widget_message_page_url( $return_tab ) ),								
			), admin_url( 'customize.php' ) );		

		return $customizer_url;
	}

	/**
	 * Get WooCommerce email settings page URL
	 *
	 * @return string
	 */
	public static function get_cev_widget_message_page_url( $return_tab ) {
		return admin_url( 'admin.php?page=customer-email-verification-for-woocommerce&tab=' . $return_tab );
	}
	/**
	* Code for initialize default value for customizer
	*/	
	public function cev_generate_defaults() {
		$customizer_defaults = array(
			
			
			'cev_widget_type' => 'registration',
			'cev_verification_header' => __( 'Verify its you.', 'customer-email-verification' ),
			'cev_verification_message'	=> __('We sent verification code to  {customer_email}. To verify your email address, please check your inbox and enter the code below.', 'customer-email-verification'),
			'cev_verification_widget_footer'  =>__("Didn't receive an email? <strong>{cev_resend_verification}</strong>", 'customer-email-verification'),
			'cev_header_text_checkout'	=> __( 'Verify its you.', 'customer-email-verification' ),
			'cev_verification_widget_message_checkout'	=> __( 'Please verify your email address to proceed to checkout.', 'customer-email-verification' ),
			'cev_verification_widget_message_footer_checkout_pro'	=> __( 'Already have an account?', 'customer-email-verification') . '<a href="/my-account">' . __(' Login now', 'customer-email-verification') . '</a>',
			'cev_verification_header_button_text' => __('Verify Code', 'customer-email-verification'),
			'cev_verification_header_send_verify_button_text' => __('Verify Your Email', 'customer-email-verification'),
			'cev_verification_header_verify_button_text' => __('Verify Code', 'customer-email-verification')
			
		);
		return $customizer_defaults;
	}						
	
	/**
	 * Register our sample default controls
	 */
	public function cev_my_verification_popup_message( $wp_customize ) {	
	
		/**
		* Load all our Customizer Custom Controls
		*/
		require_once trailingslashit( dirname(__FILE__) ) . 'custom-controls.php';		
	
		
		
		$wp_customize->add_setting( 'cev_widget_type',
			array(
				'default' => $this->defaults['cev_widget_type'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_widget_type',
			array(
				'label' => __( 'Popup Type' ),
				'section' => 'cev_verificaion_popup_message',
				'type' => 'select',
				'choices' => array(
					'registration' => __( 'Registration', 'customer-email-verification' ),
					'checkout' => __( 'Checkout', 'customer-email-verification' ),
				),
			)
		);	
		
		// Header text	
		$wp_customize->add_setting( 'cev_verification_header',
			array(
				'default' => $this->defaults['cev_verification_header'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_header',
			array(
				'label' => __( 'Header text', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verificaion_popup_message',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_header'], 'customer-email-verification' ),			
				),
				'active_callback' => array( $this, 'active_callback_for_registration' ),
			)
			
		);	
		
		// message content	
		$wp_customize->add_setting( 'cev_verification_message',
			array(
				'default' => $this->defaults['cev_verification_message'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_message',
			array(
				'label' => __( 'Message', 'customer-email-verification' ),
				'section' => 'cev_verificaion_popup_message',
				'type' => 'textarea',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_message'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_registration' ),	
			)
		);
		
		// Footer content	
		$wp_customize->add_setting( 'cev_verification_widget_footer',
			array(
				'default' => $this->defaults['cev_verification_widget_footer'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_widget_footer',
			array(
				'label' => __( 'Footer content', 'customer-email-verification' ),
				'section' => 'cev_verificaion_popup_message',
				'type' => 'textarea',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_widget_footer'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_registration' ),
			)
		);
		
		$wp_customize->add_setting( 'cev_widzet_code_block',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( new WP_Customize_Cev_Codeinfoblock_Control( $wp_customize, 'cev_widzet_code_block',
			array(
				'label' => __( 'Available variables', 'customer-email-verification' ),
				'description' => '<code>You can use HTML tags : &lt;a&gt;, &lt;strong&gt;, &lt;i&gt; and placeholders:<br>{customer_email}<br>{cev_resend_verification}<br></code>',
				'section' => 'cev_verificaion_popup_message',
				'active_callback' => array( $this, 'active_callback_for_registration' ),				
			)		
		)
		 );
		 
		// Header text	Checkout
		$wp_customize->add_setting( 'cev_header_text_checkout',
			array(
				'default' => $this->defaults['cev_header_text_checkout'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		
		$wp_customize->add_control( 'cev_header_text_checkout',
			array(
				'label' => __( 'Header text', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verificaion_popup_message',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_header_text_checkout'], 'customer-email-verification' ),
					
				),
				'active_callback' => array( $this, 'active_callback_for_checkout' ),
			)
		);
		
		// message content	Chekout
		$wp_customize->add_setting( 'cev_verification_widget_message_checkout',
			array(
				'default' => $this->defaults['cev_verification_widget_message_checkout'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_widget_message_checkout',
			array(
				'label' => __( 'Widget Content', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verificaion_popup_message',
				'type' => 'textarea',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_widget_message_checkout'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_checkout' ),
			)
		);
		
		// Footer content Checkout
		$wp_customize->add_setting( 'cev_verification_widget_message_footer_checkout_pro',
			array(
				'default' => $this->defaults['cev_verification_widget_message_footer_checkout_pro'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_widget_message_footer_checkout_pro',
			array(
				'label' => __( 'Footer content', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verificaion_popup_message',
				'type' => 'textarea',
				'active_callback' => array( $this, 'active_callback_for_checkout' ),
				
			)
			
		);
		
		$wp_customize->add_setting( 'cev_widzet_code_block_che',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( new WP_Customize_Cev_Codeinfoblock_Control( $wp_customize, 'cev_widzet_code_block_che',
			array(
				'label' => __( 'Available variables', 'customer-email-verification' ),
				'description' => '<code> You can use HTML tags : &lt;a&gt;, &lt;strong&gt;, &lt;i&gt;	 and placeholders:<br> {customer_email}<br>{cev_resend_verification}<br></code>',
				'section' => 'cev_verificaion_popup_message',
				'active_callback' => array( $this, 'active_callback_for_checkout' ),				
			)		
		)
		 );
		
		// Header Button text register
		$wp_customize->add_setting( 'cev_verification_header_button_text',
			array(
				'default' => $this->defaults['cev_verification_header_button_text'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_header_button_text',
			array(
				'label' => __( 'Verification Button Text', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verificaion_popup_message',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_header_button_text'], 'customer-email-verification' ),			
					
				),
				'active_callback' => array( $this, 'active_callback_for_registration' ),
			)
			
		);	
		
		
		// Header Button text checkout_send_verify_code
		$wp_customize->add_setting( 'cev_verification_header_send_verify_button_text',
			array(
				'default' => $this->defaults['cev_verification_header_send_verify_button_text'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_header_send_verify_button_text',
			array(
				'label' => __( 'Verification Your Email Button Text', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verificaion_popup_message',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_header_send_verify_button_text'], 'customer-email-verification' ),			
					
				),
				'active_callback' => array( $this, 'active_callback_for_checkout' ),
			)
			
		);	
		
		// Header Button text checkout_verify_code
		$wp_customize->add_setting( 'cev_verification_header_verify_button_text',
			array(
				'default' => $this->defaults['cev_verification_header_button_text'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_header_verify_button_text',
			array(
				'label' => __( 'Verification Button Text', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verificaion_popup_message',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_header_verify_button_text'], 'customer-email-verification' ),			
					
				),
				'active_callback' => array( $this, 'active_callback_for_checkout' ),
			)
			
		);		
		// Header text	Checkout
		$wp_customize->add_setting( 'cev_header_text_checkout',
			array(
				'default' => $this->defaults['cev_header_text_checkout'],
				'transport' => 'refresh',
				'type' => 'option',

				'sanitize_callback' => ''
			)
		);
									
	}	
	
	public function active_callback_for_registration() {	
		$cev_widget_type = get_option('cev_widget_type');		
		if ( 'registration' == $cev_widget_type ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function active_callback_for_checkout() {	
		$cev_widget_type = get_option('cev_widget_type');		
		if ( 'checkout' == $cev_widget_type ) {
			return true;
		} else {
			return false;
		}
	}
}
/**
 * Initialise our Customizer settings
 */

$cev_verification_popup_message = new cev_verification_popup_message();
