<?php
/**
 * Customizer Setup and Custom Controls
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
class Cev_Verification_Widget_Message {
	// Get our default values	
	private static $order_ids  = null;
	
	public function __construct() {
		// Get our Customizer defaults
		$this->defaults = $this->cev_generate_defaults();		
		
		// Register our sample default controls
		add_action( 'customize_register', array( $this, 'cev_my_verification_widget_message' ) );
		
		// Only proceed if this is own request.				
		if ( ! self::is_own_customizer_request() && ! self::is_own_preview_request()) {
			return;
		}			
		
		// Register our sections
		add_action( 'customize_register', array( wc_cev_customizer(), 'cev_add_customizer_sections' ) );	
		
		// Remove unrelated components.
		add_filter( 'customize_loaded_components', array( wc_cev_customizer(), 'remove_unrelated_components' ), 99, 2 );
		
		// Remove unrelated sections.
		add_filter( 'customize_section_active', array( wc_cev_customizer(), 'remove_unrelated_sections' ), 10, 2 );	
		
		// Unhook divi front end.
		add_action( 'woomail_footer', array( wc_cev_customizer(), 'unhook_divi' ), 10 );
		
		// Unhook Flatsome js
		add_action( 'customize_preview_init', array( wc_cev_customizer(), 'unhook_flatsome' ), 50  );	
		

		add_filter( 'customize_controls_enqueue_scripts', array( wc_cev_customizer(), 'enqueue_customizer_scripts' ) );	
		
		//add_action( 'parse_request', array( $this, 'set_up_preview' ) );	

		add_action( 'customize_preview_init', array( $this, 'enqueue_preview_scripts' ) );					
	}			
		
	/**
	* Add css and js for preview
	*/	
	public function enqueue_preview_scripts() {		 
		 wp_enqueue_style('cev-pro-preview-styles', cev_pro()->plugin_dir_url() . 'assets/css/preview-styles.css', array(), cev_pro()->version  );		 
	}	
	
	/**
	 * Checks to see if we are opening our custom customizer preview
	 *
	 * @return bool
	 */
	public static function is_own_preview_request() {
		return isset( $_REQUEST['action'] ) && ( 'preview_cev_verification_lightbox' === $_REQUEST['action'] || 'guest_user_preview_cev_verification_lightbox' === $_REQUEST['action'] );
	}
	
	/**
	 * Checks to see if we are opening our custom customizer controls
	 *
	 * @return bool
	 */
	public static function is_own_customizer_request() {
		return isset( $_REQUEST['section'] ) && 'cev_verification_popup_style' === $_REQUEST['section'];
	}
	
	/**
	 * Get Customizer URL
	 *
	 */
	public static function get_customizer_url( $section ) {	
		$customizer_url = add_query_arg( array(
				'cev-customizer' => '1',
				'section' => $section,						
				'autofocus[section]' => 'cev_verification_popup_style',
				'url'                  => urlencode( add_query_arg( array( 'action' => 'preview_cev_verification_lightbox' ), home_url( '/' ) ) ),
				'return'               => urlencode( self::get_cev_widget_message_page_url( $return_tab ) ),								
			), admin_url( 'customize.php' ) );		

		return $customizer_url;
	}

	/**
	 * Get WooCommerce email settings page URL
	 *
	 * @return string
	 */
	public static function get_cev_widget_message_page_url( $return_tab ) {
		return admin_url( 'admin.php?page=customer-email-verification-for-woocommerce&tab=' . $return_tab );
	}
	/**
	* Code for initialize default value for customizer
	*/	
	public function cev_generate_defaults() {
		$customizer_defaults = array(
			
			'cev_verification_image'	=> cev_pro()->plugin_dir_url() . 'assets/css/images/email-verification.svg',
			'cev_widget_header_image_width'   => '80',
			'cev_button_text_header_font_size' => '18',
			'cev_button_color_widget_header' => '#2296f3',
			'cev_button_text_color_widget_header'	=> '#ffffff',
			'cev_button_text_size_widget_header' => '14',
			'cev_verification_popup_background_color'	=> '#f5f5f5',
			'cev_verification_popup_overlay_background_color' => '#ffffff',
			'cev_widget_content_width' => '440',
			'cev_content_align' => 'Center',
			'cev_button_padding_size_widget_header' => '15',
			'cev_widget_content_padding' => '30',
			'sample_toggle_switch_cev' => '0',
			'cev_popup_button_size' => 'normal',
		);
		return $customizer_defaults;
	}						
	
	/**
	 * Register our sample default controls
	 */
	public function cev_my_verification_widget_message( $wp_customize ) {	
	
		/**
		* Load all our Customizer Custom Controls
		*/
		require_once trailingslashit( dirname(__FILE__) ) . 'custom-controls.php';		
													
		$font_size_array_cev[ '' ] = __( 'Select', 'customer-email-verification' );
		for ( $i = 10; $i <= 30; $i +=2 ) {
			$font_size_array_cev[ $i ] = $i . 'px';
		}
		
		
		// Table Background color
		$wp_customize->add_setting( 'cev_verification_popup_overlay_background_color',
			array(
				'default' => $this->defaults['cev_verification_popup_overlay_background_color'],
				'transport' => 'refresh',
				'sanitize_callback' => 'sanitize_hex_color',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_popup_overlay_background_color',
			array(
				'label' => __( 'Overlay background Color', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
				'priority' => 1, // Optional. Order priority to load the control. Default: 10
				'type' => 'color',
			)
		);
		
		// email button color overly
		$wp_customize->add_setting( 'cev_verification_popup_background_color',
			array(
				'default' => $this->defaults['cev_verification_popup_background_color'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_popup_background_color',
			array(
				'label' => __( 'Widget background Color', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_verification_popup_style',
				'priority' => 2, // Optional. Order priority to load the control. Default: 10
				'type' => 'color',
			)

		);
		
		// Content align
		$wp_customize->add_setting( 'cev_content_align',
			array(
				'default' => $this->defaults['cev_content_align'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_content_align',
			array(
				'label' => __( 'Content align' ),
				'section' => 'cev_verification_popup_style',
				'type' => 'select',
				'choices' => array(
					'center' => __( 'Center', 'customer-email-verification' ),
					'left' => __( 'Left', 'customer-email-verification' ),
				),
			)
		);
		
		// Content width
		$wp_customize->add_setting( 'cev_widget_content_width',
			array(
				'default' => $this->defaults['cev_widget_content_width'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new Skyrocket_Slider_Custom_Control_Cev( $wp_customize, 'cev_widget_content_width',
			array(
				'label' => __( 'Content width', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
				'input_attrs' => array(
						'default' => $this->defaults['cev_widget_content_width'],
						'step'  => 10,
						'min'   => 300,
						'max'   => 600,
					),
			)
		));	
		
		// Content padding
		$wp_customize->add_setting( 'cev_widget_content_padding',
			array(
				'default' => $this->defaults['cev_widget_content_padding'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new Skyrocket_Slider_Custom_Control_Cev( $wp_customize, 'cev_widget_content_padding',
			array(
				'label' => __( 'Content Padding', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
				'input_attrs' => array(
						'default' => $this->defaults['cev_widget_content_padding'],
						'step'  => 10,
						'min'   => 10,
						'max'   => 100,
					),
			)
		));
		
		 // Popup Header
		$wp_customize->add_setting( 'cev_verification_widget_style_Popup_Header',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new WP_Customize_Heading_Control_Cev( $wp_customize, 'cev_verification_widget_style_Popup_Header',
			array(
				'label' => __( 'Popup Header', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
		
			)
		) );
		
		// Display email image/thumbnail
		$wp_customize->add_setting( 'cev_verification_image',
			array(
				'default' => $this->defaults['cev_verification_image'],
				'transport' => 'refresh',
				'type'      => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cev_verification_image',
			array(
				'label'      => __( 'Header image', 'customer-email-verification' ),
				'section'    => 'cev_verification_popup_style',
			)
			) 
		);
		
		// email image  width
		$wp_customize->add_setting( 'cev_widget_header_image_width',
			array(
				'default' => $this->defaults['cev_widget_header_image_width'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new Skyrocket_Slider_Custom_Control_Cev( $wp_customize, 'cev_widget_header_image_width',
			array(
				'label' => __( 'Image Width', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
				'input_attrs' => array(
						'default' => $this->defaults['cev_widget_header_image_width'],
						'step'  => 10,
						'min'   => 50,
						'max'   => 250,
					),
			)
		));
		
		// Header font size 
		$wp_customize->add_setting( 'cev_button_text_header_font_size',
			array(
				'default' => $this->defaults['cev_button_text_header_font_size'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_button_text_header_font_size',
			array(
				'label' => __( 'Header font size', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
				'type' => 'select',
				'choices' => $font_size_array_cev,
			)
		);
		
		 // Verification Button Custom Control
		$wp_customize->add_setting( 'cev_verification_widget_style_custom_control',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new WP_Customize_Heading_Control_Cev( $wp_customize, 'cev_verification_widget_style_custom_control',
			array(
				'label' => __( 'Verification Button', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
		
			)
		) );
		
		// cev button color
		$wp_customize->add_setting( 'cev_button_color_widget_header',
			array(
				'default' => $this->defaults['cev_button_color_widget_header'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_button_color_widget_header',
			array(
				'label' => __( 'Button color', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
				'type' => 'color'
			)
		);	
		// Cev Button fornt color
		$wp_customize->add_setting( 'cev_button_text_color_widget_header',
			array(
				'default' => $this->defaults['cev_button_text_color_widget_header'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_button_text_color_widget_header',
			array(
				'label' => __( 'Button Font Color', 'customer-email-verification' ),
				'section' => 'cev_verification_popup_style',
				'type' => 'color'
			)
		);
	
		// Add our Text Radio Button setting and Custom Control for controlling alignment of icons
		$wp_customize->add_setting( 'cev_popup_button_size',
			array(
				'default' => $this->defaults['cev_popup_button_size'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => 'cev_radio_sanitization'
			)
		);
		$wp_customize->add_control( new CEV_Text_Radio_Button_Custom_Control( $wp_customize, 'cev_popup_button_size',
			array(
				'label' => __( 'Button size', 'customer-email-verification' ),				
				'section' => 'cev_verification_popup_style',
				'choices' => array(
					'normal' => __( 'Normal', 'customer-email-verification' ),
					'large' => __( 'Large', 'customer-email-verification'  )
				)
			)
		) );
		
		$wp_customize->add_setting( 'sample_toggle_switch_cev',
			array(
				'default' =>  $this->defaults['sample_toggle_switch_cev'],
				'transport' => 'refresh',				
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new CEV_Customizer_Toggle_Control( $wp_customize, 'sample_toggle_switch_cev', array(
			'label'	      => esc_html__( 'Expand Button', 'customer-email-verification' ),
			'section'     => 'cev_verification_popup_style',
			'settings'    => 'sample_toggle_switch_cev',
			'type'        => 'ios',// light, ios, flat
		) ) );
										
	}	
	
}
/**
 * Initialise our Customizer settings
 */

$cev_verification_widget_message = new cev_verification_widget_message();
