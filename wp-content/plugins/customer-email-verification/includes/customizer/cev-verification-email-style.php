<?php
/**
 * Customizer Setup and Custom Controls
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
class Cev_Initialise_Customizer_Settings {
	// Get our default values	
	private static $order_ids  = null;
	
	public function __construct() {
		// Get our Customizer defaults
		$this->defaults = $this->cev_generate_defaults();		
		
		// Register our sample default controls
		add_action( 'customize_register', array( $this, 'cev_register_sample_default_controls_style' ) );
		
		// Only proceed if this is own request.				
		if ( ! self::is_own_customizer_request() && ! self::is_own_preview_request()) {
			return;
		}						
		
		// Register our sections
		add_action( 'customize_register', array( wc_cev_customizer(), 'cev_add_customizer_sections' ) );	
		
		// Remove unrelated components.
		add_filter( 'customize_loaded_components', array( wc_cev_customizer(), 'remove_unrelated_components' ), 99, 2 );

		// Remove unrelated sections.
		add_filter( 'customize_section_active', array( wc_cev_customizer(), 'remove_unrelated_sections' ), 10, 2 );	
		
		// Unhook divi front end.
		add_action( 'woomail_footer', array( wc_cev_customizer(), 'unhook_divi' ), 10 );

		// Unhook Flatsome js
		add_action( 'customize_preview_init', array( wc_cev_customizer(), 'unhook_flatsome' ), 50  );	

		add_filter( 'customize_controls_enqueue_scripts', array( wc_cev_customizer(), 'enqueue_customizer_scripts' ) );		
		
		add_action( 'parse_request', array( $this, 'set_up_preview' ) );

		add_action( 'customize_preview_init', array( $this, 'enqueue_preview_scripts' ) );	
	}

	/**
	 * Add css and js for preview
	*/	
	public function enqueue_preview_scripts() {		 
		 wp_enqueue_style('cev-preview-styles', cev_pro()->plugin_dir_url() . 'assets/css/preview-styles.css', array(), cev_pro()->version  );		 
	}	
	/**
	 * Checks to see if we are opening our custom customizer preview
	 *
	 * @return bool
	 */
	public static function is_own_preview_request() {
		return isset( $_REQUEST['cev-email-style-preview'] ) && '1' === $_REQUEST['cev-email-style-preview'];
	}
	
	/**
	 * Checks to see if we are opening our custom customizer controls
	 *
	 * @return bool
	 */
	public static function is_own_customizer_request() {
		return isset( $_REQUEST['section'] ) && 'cev_main_controls_section' === $_REQUEST['section'];
	}
	
	/**
	 * Get Customizer URL
	 *
	 */
	public static function get_customizer_url( $section ) {	
			//echo $return_tab;exit;
			$customizer_url = add_query_arg( array(
				'cev-customizer' => '1',
				'section' => $section,
				'url'     => urlencode( add_query_arg( array( 'cev-email-preview' => '1' ), home_url( '/' ) ) ),
			), admin_url( 'customize.php' ) );		

		return $customizer_url;
	}
	
	/**
	 * Code for initialize default value for customizer
	*/	
	public function cev_generate_defaults() {
		$customizer_defaults = array(
			
			'cev_widget_content_width_style' => '650',
			'cev_content_align_style' => 'left',
			'cev_widget_content_padding_style' => '30',
			'cev_verification_content_background_color'	=> '#fafafa',
			'cev_verification_content_border_color' => '#e0e0e0',
			'cev_verification_content_font_color' => '#333333',
			'cev_verification_image_content'	=> cev_pro()->plugin_dir_url() . 'assets/css/images/email-verification.png',
			'cev_email_content_widget_header_image_width'   => '80',
			'cev_header_content_font_size' => '18',
			'cev_verification_selection' => 'button',
			'cev_new_acoount_button_text'	=> __( 'Verify Your Email', 'customer-email-verification' ),
			'cev_button_text_font_size' => '14',
			'cev_button_padding_size' => '15',
			'cev_new_email_button_color' => '#2296f3',
			'cev_new_email_link_color' => '#0052ff',
			'cev_new_email_button_color_text' => '#ffffff',
			'cev_email_verification_button_size' => 'normal'
		);

		return $customizer_defaults;
	}		

	/**
	 * Register our sample default controls
	 */
	public function cev_register_sample_default_controls_style( $wp_customize ) {		
		/**
		* Load all our Customizer Custom Controls
		*/
		require_once trailingslashit( dirname(__FILE__) ) . 'custom-controls.php';		
		
		$font_size_array_cev[ '' ] = __( 'Select', 'customer-email-verification' );
		for ( $i = 10; $i <= 30; $i++ ) {
			$font_size_array_cev[ $i ] = $i . 'px';
		}	

		
		// Content width
		$wp_customize->add_setting( 'cev_widget_content_width_style',
			array(
				'default' => $this->defaults['cev_widget_content_width_style'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new Skyrocket_Slider_Custom_Control_Cev( $wp_customize, 'cev_widget_content_width_style',
			array(
				'label' => __( 'Content width', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'input_attrs' => array(
						'default' => $this->defaults['cev_widget_content_width_style'],
						'step'  => 1,
						'min'   => 400,
						'max'   => 1000,
					),
			)
		));
		 
		
		// Content align
		$wp_customize->add_setting( 'cev_content_align_style',
			array(
				'default' => $this->defaults['cev_content_align_style'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_content_align_style',
			array(
				'label' => __( 'Content align' ),
				'section' => 'cev_email_style_section',
				'type' => 'select',
				'choices' => array(
					'center' => __( 'Center', 'customer-email-verification' ),
					'left' => __( 'Left', 'customer-email-verification' ),
				),
			)
		);
		
		// Content padding
		$wp_customize->add_setting( 'cev_widget_content_padding_style',
			array(
				'default' => $this->defaults['cev_widget_content_padding_style'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new Skyrocket_Slider_Custom_Control_Cev( $wp_customize, 'cev_widget_content_padding_style',
			array(
				'label' => __( 'Content Padding', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'input_attrs' => array(
						'default' => $this->defaults['cev_widget_content_padding_style'],
						'step'  => 1,
						'min'   => 10,
						'max'   => 100,
					),
			)
		));
		
		// email content background color
		$wp_customize->add_setting( 'cev_verification_content_background_color',
			array(
				'default' => $this->defaults['cev_verification_content_background_color'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_content_background_color',
			array(
				'label' => __( 'background Color', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_email_style_section',
				'type' => 'color',
			)

		);
		
		// email content Border Color 
		$wp_customize->add_setting( 'cev_verification_content_border_color',
			array(
				'default' => $this->defaults['cev_verification_content_border_color'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_content_border_color',
			array(
				'label' => __( 'Border Color', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_email_style_section',
				'type' => 'color',
			)

		);
		
		// email content Font Color 
		$wp_customize->add_setting( 'cev_verification_content_font_color',
			array(
				'default' => $this->defaults['cev_verification_content_font_color'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_content_font_color',
			array(
				'label' => __( 'Font Color', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_email_style_section',
				'type' => 'color',
			)

		);
		
		 // content Header
		$wp_customize->add_setting( 'cev_verification_widget_style_content_Header',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new WP_Customize_Heading_Control_Cev( $wp_customize, 'cev_verification_widget_style_content_Header',
			array(
				'label' => __( 'Widget Header', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
		
			)
		) );
		
		// Display email image/thumbnail
		$wp_customize->add_setting( 'cev_verification_image_content',
			array(
				'default' => $this->defaults['cev_verification_image_content'],
				'transport' => 'refresh',
				'type'      => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cev_verification_image_content',
			array(
				'label'      => __( 'Header image', 'customer-email-verification' ),
				'section'    => 'cev_email_style_section',
			)
			) 
		);
		
		// email content image  width
		$wp_customize->add_setting( 'cev_email_content_widget_header_image_width',
			array(
				'default' => $this->defaults['cev_email_content_widget_header_image_width'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new Skyrocket_Slider_Custom_Control_Cev( $wp_customize, 'cev_email_content_widget_header_image_width',
			array(
				'label' => __( 'image Width', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'input_attrs' => array(
						'default' => $this->defaults['cev_email_content_widget_header_image_width'],
						'step'  => 10,
						'min'   => 50,
						'max'   => 300,
					),
			)
		));
		
		// Header content font size 
		$wp_customize->add_setting( 'cev_header_content_font_size',
			array(
				'default' => $this->defaults['cev_header_content_font_size'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_header_content_font_size',
			array(
				'label' => __( 'Header font size', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'type' => 'select',
				'choices' => $font_size_array_cev,
			)
		);
		
		// Verification  button text
		$wp_customize->add_setting( 'cev_verification_button',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new WP_Customize_Heading_Control_Cev( $wp_customize, 'cev_verification_button',
			array(
				'label' => __( 'Verification Button', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
			)
		) );
		
		// verification selection  
		$wp_customize->add_setting( 'cev_verification_selection',
			array(
				'default' => $this->defaults['cev_verification_selection'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_selection',
			array(
				'label' => __( 'Type', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'type' => 'select',
				'choices' => array(
			'button' => __( 'Button', 'customer-email-verification' ),
			'link' => __( 'Simple Link', 'customer-email-verification' ),
			
				),
				
			)
		);
		
		// Button/link Text	
		$wp_customize->add_setting( 'cev_new_acoount_button_text',
			array(
				'default' => $this->defaults['cev_new_acoount_button_text'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_new_acoount_button_text',
			array(
				'label' => __( 'Text', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_new_acoount_button_text'], 'customer-email-verification' ),
				),
			)
		);		
		
		// Add our Text Radio Button setting and Custom Control for controlling alignment of icons
		$wp_customize->add_setting( 'cev_email_verification_button_size',
			array(
				'default' => $this->defaults['cev_email_verification_button_size'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => 'cev_radio_sanitization'
			)
		);
		$wp_customize->add_control( new CEV_Text_Radio_Button_Custom_Control( $wp_customize, 'cev_email_verification_button_size',
			array(
				'label' => __( 'Button size', 'customer-email-verification' ),				
				'section' => 'cev_email_style_section',
				'choices' => array(
					'normal' => __( 'Normal', 'customer-email-verification' ),
					'large' => __( 'Large', 'customer-email-verification'  )
				)
			)
		) );
		
		// email button color
		$wp_customize->add_setting( 'cev_new_email_button_color',
			array(
				'default' => $this->defaults['cev_new_email_button_color'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_new_email_button_color',
			array(
				'label' => __( 'Button color', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'type' => 'color',
				'active_callback' => array( $this, 'active_callback_for_button' ),
			)
			
		);
		// email link color
		$wp_customize->add_setting( 'cev_new_email_link_color',
			array(
				'default' => $this->defaults['cev_new_email_link_color'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_new_email_link_color',
			array(
				'label' => __( 'Font Color', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'type' => 'color',
				'active_callback' => array( $this, 'active_callback_for_link' ),
			)
			
		);		
		
		// email button font color text
		$wp_customize->add_setting( 'cev_new_email_button_color_text',
			array(
				'default' => $this->defaults['cev_new_email_button_color_text'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_new_email_button_color_text',
			array(
				'label' => __( 'Button Font Color', 'customer-email-verification' ),
				'section' => 'cev_email_style_section',
				'type' => 'color',
				'active_callback' => array( $this, 'active_callback_for_button' ),
				
			)
			
		);
		
	}
	
	public function active_callback_for_button() {	
		$cev_verification_selection = get_option('cev_verification_selection');		
		if ( 'button' == $cev_verification_selection ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function active_callback_for_link() {	
		$cev_verification_selection = get_option('cev_verification_selection');		
		if ( 'link' == $cev_verification_selection ) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Set up preview
	 *
	 * @return void
	 */
	public function set_up_preview() {
		
		// Make sure this is own preview request.
		if ( ! self::is_own_preview_request() ) {
			return;
		}
		include cev_pro()->get_plugin_path() . '/includes/customizer/preview/preview.php';		
		exit;			
	}	

	/**
	 * Code for preview of tracking info in email
	*/	
	public function preview_account_email() {				
		
		// Load WooCommerce emails.
		$cev_verification_email_heading_reg = new Cev_Initialise_Customizer_Settings_Content();
		$wc_emails      = WC_Emails::instance();
		$emails         = $wc_emails->get_emails();				
		cev_pro()->WC_customer_email_verification_email_Common->wuev_user_id  = 1;		
		$email_heading     = get_option('cev_verification_email_heading', $cev_verification_email_heading_reg->defaults['cev_verification_email_heading']);
		$email_heading 	   = cev_pro()->WC_customer_email_verification_email_Common->maybe_parse_merge_tags( $email_heading );		
		
		$email_content = get_option('cev_verification_email_body', $cev_verification_email_heading_reg->defaults['cev_verification_email_body']);
					
		$email = '';
				
		$mailer = WC()->mailer();			

		// create a new email
		$email = new WC_Email();
		$email->id = 'CEV_Registration_Verification';
			
		$email_content = cev_pro()->WC_customer_email_verification_email_Common->maybe_parse_merge_tags( $email_content );
		$footer_content = get_option('cev_new_verification_Footer_content');
		
		$content = ob_start();					
		$local_template	= get_stylesheet_directory() . '/woocommerce/emails/cev-email-verification.php';				
		
		if ( file_exists( $local_template ) && is_writable( $local_template )) {
			wc_get_template( 'emails/cev-email-verification.php', array( 
				'email_heading' => $email_heading,
				'content' => $email_content,
				'footer_content' => $footer_content,	
			), 'customer-email-verification/', get_stylesheet_directory() . '/woocommerce/' );
		} else {
			wc_get_template( 'emails/cev-email-verification.php', array( 
				'email_heading' => $email_heading,
				'content' => $email_content,					
				'footer_content' => $footer_content,	
			), 'customer-email-verification/', cev_pro()->get_plugin_path() . '/templates/' );
		}
		
		$content = ob_get_clean();				
		
		add_filter( 'wp_kses_allowed_html', array( cev_pro()->WC_customer_email_verification_email_Common, 'my_allowed_tags' ) );
		add_filter( 'safe_style_css', array( cev_pro()->WC_customer_email_verification_email_Common, 'safe_style_css_callback' ), 10, 1 );
		
		// wrap the content with the email template and then add styles
		$message = apply_filters( 'woocommerce_mail_content', $email->style_inline( $mailer->wrap_message( $email_heading, $content ) ) );
		$message = apply_filters( 'wc_cev_decode_html_content', $message );		
		echo wp_kses_post( $message );
	}	
}

/**
 * Initialise our Customizer settings
 */
$cev_initialise_customizer_settings = new cev_initialise_customizer_settings();
