<?php
/**
 * CEV Customizer Custom Controls
 *
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
 
if ( class_exists( 'WP_Customize_Control' ) ) {
	
	class WP_Customize_Cev_Codeinfoblock_Control extends WP_Customize_Control {

		public function render_content() {
			?>
			<label>
				<h3 class="customize-control-title"><?php esc_html_e( $this->label, 'customer-email-verification' ); ?></h3>
				<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo wp_kses_post( $this->description ); ?></span>
				<?php endif; ?>
			</label>
			<?php
		}
	}	
	
	class WP_Customize_Heading_Control_Cev extends WP_Customize_Control {		

		public function render_content() {
			?>
			<label>
				<h3 class="control_heading"><?php esc_html_e( $this->label, 'customer-email-verification' ); ?></h3>
				<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php endif; ?>
			</label>
			<?php
		}
	}
	/**
	* Custom Control Base Class
	*/
	class Skyrocket_Custom_Control_Cev extends WP_Customize_Control {
		protected function get_skyrocket_resource_url() {
			if ( strpos( wp_normalize_path( __DIR__ ), wp_normalize_path( WP_PLUGIN_DIR ) ) === 0 ) {
				// We're in a plugin directory and need to determine the url accordingly.
				return plugin_dir_url( __DIR__ );
			}

			return trailingslashit( get_template_directory_uri() );
		}
	}
	
	class CEV_Customizer_Toggle_Control extends WP_Customize_Control {
		public $type = 'ios';
		
		/**
		* Enqueue scripts/styles.
		*
		* @since 3.4.0
		*/
		public function enqueue() {
			wp_enqueue_script( 'cev-custom-new-controls-js', cev_pro()->plugin_dir_url() . 'assets/js/customizer.js', array( 'jquery', 'jquery-ui-core' ), cev_pro()->version, true );
			wp_enqueue_style( 'cev-custom-new-controls-css', cev_pro()->plugin_dir_url() . 'assets/css/customizer.css', array(), cev_pro()->version, 'all' );
		}
	
		/**
		* Render the control's content.
		*		
		* @version 1.0.0
		*/
		public function render_content() {
			?>
			<label class="customize-toogle-label">
				<div style="padding: 10px 0;">
					<input id="cb<?php esc_html_e( $this->instance_number ); ?>" type="checkbox" class="tgl tgl-flat-cev" value="<?php echo esc_attr( $this->value() ); ?>"
											<?php
											$this->link();
											checked( $this->value() );
											?>
					/>
					<label for="cb<?php esc_html_e( $this->instance_number ); ?>" class="tgl-btn"></label>
					<span class="customize-control-title" style="flex: 2 0 0; vertical-align: middle; display: inline-block;margin:0 0 0 7px;"><?php esc_html_e( $this->label, 'customer-email-verification' ); ?></span>
				</div>
				<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php esc_html_e( $this->description ); ?></span>
				<?php endif; ?>
			</label>
			<?php
		}
	}
	
	/**
	 * Slider Custom Control
	 */
	class Skyrocket_Slider_Custom_Control_Cev extends Skyrocket_Custom_Control_Cev {
		/**
		 * The type of control being rendered
		 */
		public $type = 'slider_control';
		/**
		 * Enqueue our scripts and styles
		 */
		public function enqueue() {
			wp_enqueue_script( 'cev-custom-new-controls-js', cev_pro()->plugin_dir_url() . 'assets/js/customizer.js', array( 'jquery', 'jquery-ui-core' ), cev_pro()->version, true );
			wp_enqueue_style( 'cev-custom-new-controls-css', cev_pro()->plugin_dir_url() . 'assets/css/customizer.css', array(), cev_pro()->version, 'all' );
		}
		/**
		 * Render the control in the customizer
		 */
		public function render_content() {
			?>
			<div class="slider-custom-control">
				<span class="customize-control-title"><?php esc_html_e( $this->label, 'customer-email-verification' ); ?></span>				
				<div class="slider" slider-min-value="<?php echo esc_attr( $this->input_attrs['min'] ); ?>" slider-max-value="<?php echo esc_attr( $this->input_attrs['max'] ); ?>" slider-step-value="<?php echo esc_attr( $this->input_attrs['step'] ); ?>">
				</div>				
				<span class="slider-reset dashicons dashicons-image-rotate" slider-reset-value="<?php echo esc_attr( $this->input_attrs['default'] ); ?>"></span>
				<input type="number" id="<?php echo esc_attr( $this->id ); ?>" name="<?php echo esc_attr( $this->id ); ?>" value="<?php echo esc_attr( $this->value() ); ?>" class="customize-control-slider-value" <?php $this->link(); ?> />
			</div>
		<?php
		}
	}

	/**
	 * Text Radio Button Custom Control	
	 */
	class CEV_Text_Radio_Button_Custom_Control extends Skyrocket_Custom_Control_Cev {
		/**
		 * The type of control being rendered
		 */
		public $type = 'text_radio_button';
		/**
		 * Enqueue our scripts and styles
		 */
		public function enqueue() {
			wp_enqueue_style( 'cev-custom-new-controls-css', cev_pro()->plugin_dir_url() . 'assets/css/customizer.css', array(), cev_pro()->version, 'all' );
		}
		/**
		 * Render the control in the customizer
		 */
		public function render_content() {
			?>
			<div class="text_radio_button_control">
				<?php if ( !empty( $this->label ) ) { ?>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php } ?>
				<?php if ( !empty( $this->description ) ) { ?>
					<span class="customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php } ?>

				<div class="radio-buttons">
					<?php foreach ( $this->choices as $key => $value ) { ?>
						<label class="radio-button-label">
							<input type="radio" name="<?php echo esc_attr( $this->id ); ?>" value="<?php echo esc_attr( $key ); ?>" <?php $this->link(); ?> <?php checked( esc_attr( $key ), $this->value() ); ?>/>
							<span><?php echo esc_html( $value ); ?></span>
						</label>
					<?php	} ?>
				</div>
			</div>
		<?php
		}
	}	
}

/**
 * Radio Button and Select sanitization
 *
 * @param  string		Radio Button value
 * @return integer	Sanitized value
 */
if ( ! function_exists( 'cev_radio_sanitization' ) ) {
	function cev_radio_sanitization( $input, $setting ) {
		//get the list of possible radio box or select options
		$choices = $setting->manager->get_control( $setting->id )->choices;
	
		if ( array_key_exists( $input, $choices ) ) {
			return $input;
		} else {
			return $setting->default;
		}
	}
}
