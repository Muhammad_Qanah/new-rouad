<?php
/**
 * Customizer Setup and Custom Controls
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
class Cev_Initialise_Customizer_Settings_Content {
	// Get our default values	
	private static $order_ids  = null;
	
	public function __construct() {
		// Get our Customizer defaults
		$this->defaults = $this->cev_generate_defaults();		
		
		// Register our sample default controls
		add_action( 'customize_register', array( $this, 'cev_register_sample_default_controls' ) );
		
		// Only proceed if this is own request.				
		if ( ! self::is_own_customizer_request() && ! self::is_own_preview_request()) {
			return;
		}						
		
		// Register our sections
		add_action( 'customize_register', array( wc_cev_customizer(), 'cev_add_customizer_sections' ) );	
		
		// Remove unrelated components.
		add_filter( 'customize_loaded_components', array( wc_cev_customizer(), 'remove_unrelated_components' ), 99, 2 );

		// Remove unrelated sections.
		add_filter( 'customize_section_active', array( wc_cev_customizer(), 'remove_unrelated_sections' ), 10, 2 );	
		
		// Unhook divi front end.
		add_action( 'woomail_footer', array( wc_cev_customizer(), 'unhook_divi' ), 10 );

		// Unhook Flatsome js
		add_action( 'customize_preview_init', array( wc_cev_customizer(), 'unhook_flatsome' ), 50  );	

		add_filter( 'customize_controls_enqueue_scripts', array( wc_cev_customizer(), 'enqueue_customizer_scripts' ) );		
		
		add_action( 'parse_request', array( $this, 'set_up_preview' ) );

		add_action( 'customize_preview_init', array( $this, 'enqueue_preview_scripts' ) );				
	}

	/**
	 * Add css and js for preview
	*/	
	public function enqueue_preview_scripts() {		 
		 wp_enqueue_style('cev-preview-styles', cev_pro()->plugin_dir_url() . 'assets/css/preview-styles.css', array(), cev_pro()->version  );		 
	}

	/**
	 * Checks to see if we are opening our custom customizer preview
	 *
	 * @return bool
	 */
	public static function is_own_preview_request() {
		return isset( $_REQUEST['cev-email-preview'] ) && '1' === $_REQUEST['cev-email-preview'];
	}
	
	/**
	 * Checks to see if we are opening our custom customizer controls
	 *
	 * @return bool
	 */
	public static function is_own_customizer_request() {
		return isset( $_REQUEST['section'] ) && 'cev_main_controls_section' === $_REQUEST['section'];
	}
	
	/**
	 * Get Customizer URL
	 *
	 */
	public static function get_customizer_url( $section ) {	
			//echo $return_tab;exit;
			$customizer_url = add_query_arg( array(
				'cev-customizer' => '1',
				'section' => $section,
				'url'     => urlencode( add_query_arg( array( 'cev-email-preview' => '1' ), home_url( '/' ) ) ),
			), admin_url( 'customize.php' ) );		

		return $customizer_url;
	}
	
	/**
	 * Code for initialize default value for customizer
	*/	
	public function cev_generate_defaults() {
		$customizer_defaults = array(
			'cev_content_widget_type' => 'registration',
			'cev_verification_email_heading' => __( 'Verify Your Email Address', 'customer-email-verification' ),
			'cev_verification_email_heading_che' => __( 'Verify Your Email Address', 'customer-email-verification' ),			
			'cev_verification_email_subject' =>  __( 'Please Verify Your Email Address on {site_title}', 'customer-email-verification' ),
			'cev_verification_email_subject_che' =>  __( 'Please Verify Your Email Address on {site_title}', 'customer-email-verification' ),		
			'cev_verification_email_body' => __( 'Thank you for signing up for {site_title}, to activate your account, we need to verify your email address.<p>Your verification code: <strong>{cev_user_verification_pin}</strong></p><p>Or, verify your account clicking on the button below:{cev_user_verification_link}', 'customer-email-verification' ),
			'cev_verification_email_body_che' => __( 'To complete your order on {site_title}, please confirm your email address. This ensures we have the right email in case we need to contact you.<p>Your verification code: {cev_user_verification_pin}</p><p>Or, verify your account clicking on the button below:</p>{cev_user_verification_link}', 'customer-email-verification' ),	
			'cev_new_email_button_color' => '#03a9f4',
			'cev_new_email_button_color_text' => '#ffffff',
			'cev_verification_selection' => 'button',
			'cev_header_button_size_pro'	=> '14px',
			'cev_new_verification_Footer_content' => '',
			'cev_new_verification_Footer_content_che' => '',		
		);

		return $customizer_defaults;
	}		

	/**
	 * Register our sample default controls
	 */
	public function cev_register_sample_default_controls( $wp_customize ) {		
		/**
		* Load all our Customizer Custom Controls
		*/
		require_once trailingslashit( dirname(__FILE__) ) . 'custom-controls.php';		
		
		$font_size_array_cev[ '' ] = __( 'Select', 'customer-email-verification' );
		for ( $i = 10; $i <= 30; $i++ ) {
			$font_size_array_cev[ $i ] = $i . 'px';
		}	
		
		/**
		* Load all our Customizer Custom Controls
		*/
		require_once trailingslashit( dirname(__FILE__) ) . 'custom-controls.php';		
	
		
		
		$wp_customize->add_setting( 'cev_content_widget_type',
			array(
				'default' => $this->defaults['cev_content_widget_type'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_content_widget_type',
			array(
				'label' => __( 'Email Type' ),
				'section' => 'cev_email_content',
				'type' => 'select',
				'choices' => array(
					'registration' => __( 'Registration', 'customer-email-verification' ),
					'checkout' => __( 'Checkout', 'customer-email-verification' ),
				),
			)
		);	
		
		// Email Subject reg	
		$wp_customize->add_setting( 'cev_verification_email_subject',
			array(
				'default' => $this->defaults['cev_verification_email_subject'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_email_subject',
			array(
				'label' => __( 'Email Subject', 'woocommerce' ),
				'description' => '',
				'section' => 'cev_email_content',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_email_subject'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_registration_style' ),
			)
		);

		// Email Heading reg
		$wp_customize->add_setting( 'cev_verification_email_heading',
			array(
				'default' => $this->defaults['cev_verification_email_heading'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_email_heading',
			array(
				'label' => __( 'Email Heading', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_email_content',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_email_heading'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_registration_style' ),
			)
		);

		// Email content reg	
		$wp_customize->add_setting( 'cev_verification_email_body',
			array(
				'default' => $this->defaults['cev_verification_email_body'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_email_body',
			array(
				'label' => __( 'Verification Message', 'customer-email-verification' ),
				'section' => 'cev_email_content',
				'type' => 'textarea',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_email_body'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_registration_style' ),
			)
		);
		
			// Email Subject che	
		$wp_customize->add_setting( 'cev_verification_email_subject_che',
			array(
				'default' => $this->defaults['cev_verification_email_subject_che'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_email_subject_che',
			array(
				'label' => __( 'Email Subject', 'woocommerce' ),
				'description' => '',
				'section' => 'cev_email_content',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_email_subject_che'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_checkout_style' ),
			)
		);

		// Email Heading che
		$wp_customize->add_setting( 'cev_verification_email_heading_che',
			array(
				'default' => $this->defaults['cev_verification_email_heading_che'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_email_heading_che',
			array(
				'label' => __( 'Email Heading', 'customer-email-verification' ),
				'description' => '',
				'section' => 'cev_email_content',
				'type' => 'text',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_email_heading_che'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_checkout_style' ),
			)
		);

		// Email content che	
		$wp_customize->add_setting( 'cev_verification_email_body_che',
			array(
				'default' => $this->defaults['cev_verification_email_body_che'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_verification_email_body_che',
			array(
				'label' => __( 'Verification Message', 'customer-email-verification' ),
				'section' => 'cev_email_content',
				'type' => 'textarea',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_verification_email_body_che'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_checkout_style' ),
			)
		);
		
		$wp_customize->add_setting( 'cev_email_code_block',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( new WP_Customize_Cev_Codeinfoblock_Control( $wp_customize, 'cev_email_code_block',
			array(
				'label' => __( 'Available variables', 'customer-email-verification' ),
				'description' => '<code>You can use HTML tags : &lt;a&gt;, &lt;strong&gt;, &lt;i&gt;	 and placeholders:{site_title}<br>{cev_user_verification_pin}<br>{cev_user_verification_link}</code>','You can use HTML tag : <strong>, <i>',
				'section' => 'cev_email_content',				
			)
		) );

		// Verification Footer content 
		$wp_customize->add_setting( 'cev_verification_footer_content',
			array(
				'default' => '',
				'transport' => 'postMessage',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( new WP_Customize_Heading_Control_Cev( $wp_customize, 'cev_verification_footer_content',
			array(
				'label' => __( 'Footer content', 'customer-email-verification' ),
				'section' => 'cev_email_content',
		
			)
		) );
		
		// Footer content	reg
		$wp_customize->add_setting( 'cev_new_verification_Footer_content',
			array(
				'default' => $this->defaults['cev_new_verification_Footer_content'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_new_verification_Footer_content',
			array(
				'label' => __( 'Addition footer content', 'customer-email-verification' ),
				'section' => 'cev_email_content',
				'type' => 'textarea',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
				),
				'active_callback' => array( $this, 'active_callback_for_registration_style' ),
			)
		);	
		
		// Footer content che
		$wp_customize->add_setting( 'cev_new_verification_Footer_content_che',
			array(
				'default' => $this->defaults['cev_new_verification_Footer_content_che'],
				'transport' => 'refresh',
				'type' => 'option',
				'sanitize_callback' => ''
			)
		);
		$wp_customize->add_control( 'cev_new_verification_Footer_content_che',
			array(
				'label' => __( 'Addition footer content', 'customer-email-verification' ),
				'section' => 'cev_email_content',
				'type' => 'textarea',
				'input_attrs' => array(
					'class' => '',
					'style' => '',
					'placeholder' => __( $this->defaults['cev_new_verification_Footer_content_che'], 'customer-email-verification' ),
				),
				'active_callback' => array( $this, 'active_callback_for_checkout_style' ),
			)
		);
	}

	public function active_callback_for_registration_style() {	
		$cev_content_widget_type = get_option('cev_content_widget_type');		
		if ( 'registration' == $cev_content_widget_type ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function active_callback_for_checkout_style() {	
		$cev_content_widget_type = get_option('cev_content_widget_type');
		if ('checkout' == $cev_content_widget_type ) {
			return true;
		} else {
			return false;
		}
	}	
	
	/**
	 * Set up preview
	 *
	 * @return void
	 */
	public function set_up_preview() {
		
		// Make sure this is own preview request.
		if ( ! self::is_own_preview_request() ) {
			return;
		}
		include cev_pro()->get_plugin_path() . '/includes/customizer/preview/email_content_preview.php';		
		exit;			
	}	

	/**
	 * Code for preview of tracking info in email
	*/	
	public function preview_account_email() {				
		
		// Load WooCommerce emails.
		$wc_emails      = WC_Emails::instance();
		$emails         = $wc_emails->get_emails();				
		cev_pro()->WC_customer_email_verification_email_Common->wuev_user_id  = 1;				
		
		$email_heading     = get_option('cev_verification_email_heading', $this->defaults['cev_verification_email_heading']);
		$email_heading 	   = cev_pro()->WC_customer_email_verification_email_Common->maybe_parse_merge_tags( $email_heading );		
		
		$email_content = get_option('cev_verification_email_body', $this->defaults['cev_verification_email_body']);
					
		$email = '';
				
		$mailer = WC()->mailer();			

		// create a new email
		$email = new WC_Email();
		$email->id = 'CEV_Registration_Verification';
			
		$email_content = cev_pro()->WC_customer_email_verification_email_Common->maybe_parse_merge_tags( $email_content );
		$footer_content = get_option('cev_new_verification_Footer_content');
		
		$content = ob_start();					
		$local_template	= get_stylesheet_directory() . '/woocommerce/emails/cev-email-verification.php';				
		if ( file_exists( $local_template ) && is_writable( $local_template )) {
			wc_get_template( 'emails/cev-email-verification.php', array( 
				'email_heading' => $email_heading,
				'content' => $email_content,				
				'footer_content' => $footer_content,
			), 'customer-email-verification/', get_stylesheet_directory() . '/woocommerce/' );
		} else {
			wc_get_template( 'emails/cev-email-verification.php', array( 
				'email_heading' => $email_heading,
				'content' => $email_content,	
				'footer_content' => $footer_content,	
			), 'customer-email-verification/', cev_pro()->get_plugin_path() . '/templates/' );
		}
		$content = ob_get_clean();				
		
		add_filter( 'wp_kses_allowed_html', array( cev_pro()->WC_customer_email_verification_email_Common, 'my_allowed_tags' ) );
		add_filter( 'safe_style_css', array( cev_pro()->WC_customer_email_verification_email_Common, 'safe_style_css_callback' ), 10, 1 );
		
		// wrap the content with the email template and then add styles
		$message = apply_filters( 'woocommerce_mail_content', $email->style_inline( $mailer->wrap_message( $email_heading, $content ) ) );
		$message = apply_filters( 'wc_cev_decode_html_content', $message );		
		echo wp_kses_post( $message );
	}

	/**
	 * Code for preview of tracking info in email
	*/	
	public function preview_checkout_email() {				
		
		// Load WooCommerce emails.
		$wc_emails      = WC_Emails::instance();
		$emails         = $wc_emails->get_emails();				
		cev_pro()->WC_customer_email_verification_email_Common->wuev_user_id  = 1;				
		
		$email_heading     = get_option('cev_verification_email_heading_che', $this->defaults['cev_verification_email_heading_che']);
		$email_heading 	   = cev_pro()->WC_customer_email_verification_email_Common->maybe_parse_merge_tags( $email_heading );		
		
		$email_content = get_option('cev_verification_email_body_che', $this->defaults['cev_verification_email_body_che']);
					
		$email = '';
				
		$mailer = WC()->mailer();			

		// create a new email
		$email = new WC_Email();
		$email->id = 'CEV_Guset_User_Verification';
			
		$email_content = cev_pro()->WC_customer_email_verification_email_Common->maybe_parse_merge_tags( $email_content );
		$footer_content = get_option('cev_new_verification_Footer_content_che');
		
		$content = ob_start();					
		$local_template	= get_stylesheet_directory() . '/woocommerce/emails/cev-email-verification.php';				
		if ( file_exists( $local_template ) && is_writable( $local_template )) {
			wc_get_template( 'emails/cev-email-verification.php', array( 
				'email_heading' => $email_heading,
				'content' => $email_content,
				'footer_content' => $footer_content,
			), 'customer-email-verification/', get_stylesheet_directory() . '/woocommerce/' );
		} else {
			wc_get_template( 'emails/cev-email-verification.php', array( 
				'email_heading' => $email_heading,
				'content' => $email_content,
				'footer_content' => $footer_content,				
			), 'customer-email-verification/', cev_pro()->get_plugin_path() . '/templates/' );
		}
		$content = ob_get_clean();				
		
		add_filter( 'wp_kses_allowed_html', array( cev_pro()->WC_customer_email_verification_email_Common, 'my_allowed_tags' ) );
		add_filter( 'safe_style_css', array( cev_pro()->WC_customer_email_verification_email_Common, 'safe_style_css_callback' ), 10, 1 );
		
		// wrap the content with the email template and then add styles
		$message = apply_filters( 'woocommerce_mail_content', $email->style_inline( $mailer->wrap_message( $email_heading, $content ) ) );
		$message = apply_filters( 'wc_cev_decode_html_content', $message );		
		echo wp_kses_post( $message );
	}	
	
	
}

/**
 * Initialise our Customizer settings
 */
$Cev_Initialise_Customizer_Settings_Content = new Cev_Initialise_Customizer_Settings_Content();
